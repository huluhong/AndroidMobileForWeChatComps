package com.pansoft.resmanager;

import com.core.xml.StubObject;
import com.efounder.service.Registry;
import com.efounder.util.EnvSupportManager;
import com.efounder.utils.ResStringUtil;
import com.pansoft.android.espbase.R;

import java.util.List;

/**
 * @author : zzj
 * @e-mail : zhangzhijun@pansoft.com
 * @date : 2018/12/19 9:49
 * @desc :  xml资源加载工具类  根据语言返回不同的路径
 * @version: 1.0
 */
public class ResLoadManager {
    //主工程 R文件
//    private static Object rFile;

//    public static Object getrFile() {
//        return rFile;
//    }

    /**
     * 设置主工程的R文件 ，一定要在主工程初始化一下
     * @param object
     */
    @Deprecated
    public static void setMainProjectR(Object object) {
//        rFile = object;
    }

    /**
     * 加载TabMenu路径
     *
     * @return
     */
    public static String getTabMenuPath() {
//        if(MultiLanguageUtil.getInstance().getLanguageType() == LanguageType.LANGUAGE_EN){
//            return ResFileManager.PACKAGE_EN_DIR;
//        }else {
        return ResFileManager.PACKAGE_DIR;
//        }
    }

    /**
     * 加载多语言tabMenu
     *
     * @param
     * @return
     */
    public static void loadMultiLanguageMenu() {
        if (EnvSupportManager.isSupportMultiLanguage()) {
            if (Registry.registryStore.size() > 0) {

                for (List<StubObject> stubObjects : Registry.registryStore.values()) {
                    for (StubObject stubObject : stubObjects) {
                        String multiLanguageKey = stubObject.getString(ResStringUtil.MULTI_LANGUAGE_KEY, "");
                        if (!multiLanguageKey.equals("") && multiLanguageKey != null) {
                            setMutlLanguage(stubObject, multiLanguageKey);
                        }
                    }
                }
            }
        }
    }

    private static void setMutlLanguage(StubObject stubObject, String multiLanguageKey) {
        try {
//            if (rFile != null) {
//                stubObject.setString(ResStringUtil.CAPTION, ResStringUtil.getString(ResStringUtil.getResIdByString((Class) rFile, multiLanguageKey)));
//            } else {
                stubObject.setString(ResStringUtil.CAPTION, ResStringUtil.getString(ResStringUtil.getResIdByString(R.string.class, multiLanguageKey)));
          //  }
        } catch (Exception e) {
            com.utilcode.util.LogUtils.e("未配置此LanguageKey的多语言："+multiLanguageKey);
        }
    }
}

