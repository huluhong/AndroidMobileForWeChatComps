package com.pansoft.resmanager;

import com.pansoft.resmanager.ResFileManager.DownloadType;

public interface ResCallback {
	
	/**
	 * 通知进度
	 * @param progress
	 */
	//void onProgress(int progress);
	
	/**
	 * 通知成功
	 */
	void onSuccess(DownloadType type);
	
	
	/**
	 * 通知失败
	 */
	void onFail();
}
