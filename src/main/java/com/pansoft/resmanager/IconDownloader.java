package com.pansoft.resmanager;

import android.os.Looper;

import com.efounder.constant.EnvironmentVariable;

import java.io.File;
import java.util.ArrayList;
import java.util.concurrent.CompletionService;
import java.util.concurrent.ExecutorCompletionService;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import static com.efounder.frame.utils.Constants.KEY_SETTING_APPID;

/**
 * 用于从服务器下载图片(图标等)
 * 同时对下载下来的图标进行本地缓存
 * @author zhangjg
 *
 */
public class IconDownloader {
	
	
	//图片缓存的位置
//	private static final String cache = "/sdcard/ESPMobile/image_cache/"; 
	
	private static final String cache = "/sdcard/"+ EnvironmentVariable.getProperty(KEY_SETTING_APPID)+"/res/unzip_res/Image/";
	
	
	static {   //检查并创建缓存目录
		
		File cacheDir = new File(cache);
		
		if(!cacheDir.exists()){
			cacheDir.mkdirs();
		}
	}
	
	
	
	/**
	 * 该方法会阻塞线程, 放在子线程中
	 * 批量下载多张图片, 并且缓存
	 * 默认url最后的部分为文件名
	 * @param urls
	 * @return 下载成功的图片的数量
	 */
	public static int download(ArrayList<String> urls){
		
		//判断线程
		if(Looper.myLooper() == Looper.getMainLooper()){
			throw new UnsupportedOperationException("该方法必须在子线程中调用");
		}
		
		int successCount = 0;  //有几个图片下载成功
		
		ExecutorService pool = Executors.newCachedThreadPool();
		CompletionService<Boolean> completionService = new ExecutorCompletionService<Boolean>(pool);
		
		
		for(final String url : urls){
			
		    /*completionService.submit(new Callable<Boolean>() {

				@Override
				public Boolean call() throws Exception {
					return download(url);
				}
			});*/
			
			
		}
		
		for(int i = 0; i < urls.size(); i ++){
			
			try {
				Future<Boolean> fu = completionService.take();
				Boolean success = fu.get();
				
				if(success){  //某个图片下载成功
					successCount ++;
				}
				
				
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		return successCount;
	}
	
	/**
	 * 下载单张图片, 并且缓存
	 * 默认url最后的部分为文件名
	 * @param url
	 *//*
	public static boolean download(String url){
		
		boolean isCached = isCached(url);
		if(isCached){    //判断是否已经缓存
			return true;
		}
		
		HttpUtils http = new HttpUtils();
		http.configCurrentHttpCacheExpiry(1000*10);
		ResponseStream responseStream;
		
		
		try {
			responseStream = http.sendSync(HttpRequest.HttpMethod.GET, 
					url);
			
			String savePath = cache + getIconFileName(url);
			responseStream.readFile(savePath);
			
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		
	}
	

	*/

	/**
	 * 根据url获取文件名
	 * @param url
	 * @return
	 */
	public static String getIconFileName(String url) {
		
		return url.substring(url.lastIndexOf("/") + 1);

	}


	

	/**
	 * 是否在本地缓存
	 * @param fileName
	 * @return
	 */
	public static boolean isCached(String url) {
		
		String fileName = getIconFileName(url);
		File file = new File(cache + fileName);
		
		
		return file.exists();

	}
	
	/**
	 * 根据url判断一些文件是否已经缓存
	 * @param urls
	 * @return
	 */
	public static boolean isCached(ArrayList<String> urls){
		
		for(String url : urls){
			
			if(!isCached(url)){   //如果有一个资源没有缓存, 就返回false.
				return false;
			}
		}
		
		return true;
	}
	
	/**
	 * 根据url得到图片的绝对路径
	 * @param urls
	 * @return
	 */
	public static ArrayList<String> getAbsolutePathByUrl(ArrayList<String> urls){
		ArrayList<String> paths = new ArrayList<String>();
		
		for(String url : urls){
			
			paths.add(getAbsolutePathByUrl(url));
		}
		
		return paths;
		
	}
	
	/**
	 * 根据url得到图片的绝对路径
	 * @param urls
	 * @return
	 */
	public static String getAbsolutePathByUrl(String url){
		
		return cache + getIconFileName(url);
		
	}
}
