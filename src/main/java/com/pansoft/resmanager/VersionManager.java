package com.pansoft.resmanager;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

/**
 * 管理web资源（菜单，表单，图片）的版本
 * 初始版本为0.0.0
 * 版本必须持久化存储
 * 
 * @author zhangjg
 * @date Jan 6, 2014 4:41:57 PM
 */
public class VersionManager {
	
	//存储资源版本信息的sp的文件名
	private static final String VERSION_SP_NAME = "res_version";
	
	private static final String VERSION_KEY = "res_version";
	
	//资源初始版本号
	private static final String INIT_VERSION = "0.0.0";
	
	//是否为第一次运行
//	private static final String IS_FIRST_KEY = "is_first_run";
	
	/**
	 * 设置当前资源的版本
	 * 每次从后台更新资源时调用
	 * 资源的版本从后台获取
	 */
	public static void setVersion(Context context, String version){
		
		SharedPreferences sp = context.getSharedPreferences(VERSION_SP_NAME, Context.MODE_PRIVATE);
		
		sp.edit().putString(VERSION_KEY, version).commit();
	}
	
	/**
	 * 设置初始版本, 默认0.0.0
	 */
	public static void setInitVersion(Context context){
		setVersion(context, INIT_VERSION);
	}
	
	
	/**
	 * 获取当前的资源版本
	 * 该数据从本地持久存储中获取
	 * 每次向服务器请求更新资源时调用，以判断是否需要更新
	 * @return
	 */
	public static String getVersion(Context context){
		SharedPreferences sp = context.getSharedPreferences(VERSION_SP_NAME, Context.MODE_PRIVATE);
		
		return sp.getString(VERSION_KEY, "");
		
	}
	
	/**
	 * 判断是否是第一次请求
	 * 如果是第一次的话, 加载init资源
	 * @param context
	 * @return
	 */
	public static boolean isFirstToRequest(Context context){
		Log.i("versionfromServer","----"+getVersion(context));
		return "".equals(getVersion(context));
	}
	/**
	 * 判断是否是最新版本
	 * 如果是第一次的话, 加载init资源
	 * @param context
	 * @return
	 */
	public static boolean isLastVersion(Context context , String versionfromServer){
		Log.i("versionfromServer",versionfromServer+"----"+getVersion(context));
		return getVersion(context).equals(versionfromServer);
	}
}
