package com.pansoft.espcomp.utils;
import java.lang.reflect.Field;

import android.content.Context;
import android.util.Log;

public class ResourceChange {
	private static final String TAG = ResourceChange.class.getName();
	private static ResourceChange self;

	private Context mContext;

	private static Class<?> CDrawable = null;

	private static Class<?> CLayout = null;

	private static Class<?> CId = null;

	private static Class<?> CAnim = null;

	private static Class<?> CStyle = null;

	private static Class<?> CString = null;

	private static Class<?> CArray = null;
	private static Class<?> CDemin = null;

	public static ResourceChange getInstance(Context context){
		if(self == null){
			self = new ResourceChange(context);
		}
		return self;
	}

	private ResourceChange(Context context){
		this.mContext = context.getApplicationContext();
		try{
			CDrawable = Class.forName(this.mContext.getPackageName() + ".R$drawable");
			CLayout = Class.forName(this.mContext.getPackageName() + ".R$layout");
			CId = Class.forName(this.mContext.getPackageName() + ".R$id");
			CAnim = Class.forName(this.mContext.getPackageName() + ".R$anim");
			CStyle = Class.forName(this.mContext.getPackageName() + ".R$style");
			CString = Class.forName(this.mContext.getPackageName() + ".R$string");
			CArray = Class.forName(this.mContext.getPackageName() + ".R$array");
			CDemin = Class.forName(this.mContext.getPackageName() + ".R$demin");

		}catch(ClassNotFoundException e){
			Log.i(TAG,e.getMessage());
		}
	}

	public int getDrawableId(String resName){
		return getResId(CDrawable,resName);
	}
	
	public int getLayoutId(String resName){
		return getResId(CLayout,resName);
	}
	
	public int getIdId(String resName){
		return getResId(CId,resName);
	}
	
	public int getAnimId(String resName){
		return getResId(CAnim,resName);
	}
	
	public int getStyleId(String resName){
		return getResId(CStyle,resName);
	}
	
	public int getStringId(String resName){
		return getResId(CString,resName);
	}
	
	public int getArrayId(String resName){
		return getResId(CArray,resName);
	}
	public int getDemin(String resName){
		return getResId(CDemin,resName);
	}
	
	private int getResId(Class<?> resClass,String resName){
		if(resClass == null){
			Log.i(TAG,"getRes(null," + resName + ")");
			throw new IllegalArgumentException("ResClass is not initialized. Please make sure you have added neccessary resources. Also make sure you have " + this.mContext.getPackageName() + ".R$* configured in obfuscation. field=" + resName);
		}

		try {
			Field field = resClass.getField(resName);
			return field.getInt(resName);
		} catch (Exception e) {
			Log.i(TAG, "getRes(" + resClass.getName() + ", " + resName + ")");
			Log.i(TAG, "Error getting resource. Make sure you have copied all resources (res/) from SDK to your project. ");

			Log.i(TAG, e.getMessage());
		} 

		return -1;
	}
}
