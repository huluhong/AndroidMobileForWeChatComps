package com.pansoft.espcomp.utils;

import android.app.Activity;
import android.graphics.Rect;
import android.util.Log;
import android.view.Display;


/**
 * 工具类， 获取和屏幕相关的尺寸信息
 * @author zhangjg
 *
 */
public class ScreenUtils {
	
	private static final String TAG = "ScreenUtils";
	
	/**
	 * 获取状态栏高度
	 * @param activity
	 * @return
	 */
	public static int getStatusHeight(Activity activity){
        int statusHeight = 0;
        Rect localRect = new Rect();
        activity.getWindow().getDecorView().getWindowVisibleDisplayFrame(localRect);
        statusHeight = localRect.top;
        if (0 == statusHeight){
            Class<?> localClass;
            try {
                localClass = Class.forName("com.android.internal.R$dimen");
                Object localObject = localClass.newInstance();
                int i5 = Integer.parseInt(localClass.getField("status_bar_height").get(localObject).toString());
                statusHeight = activity.getResources().getDimensionPixelSize(i5);

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        
        Log.i(TAG, "状态栏高度：" + statusHeight);
        return statusHeight;
    }
	
	/**
	 * 获取整个屏幕宽度
	 * @param a
	 * @return
	 */
	public static int getScreenWidth(Activity a){
		Display d = a.getWindowManager().getDefaultDisplay();
//		Log.i(TAG, "屏幕宽度：" + d.getWidth());
		return d.getWidth();
	}
	
	/**
	 * 获取整个屏幕高度
	 * @param a
	 * @return
	 */
	public static int getScreenHeight(Activity a){
		Display d = a.getWindowManager().getDefaultDisplay();
//		Log.i(TAG, "屏幕高度：" + d.getHeight());
		return d.getHeight();
	}
}
