package com.pansoft.espcomp.utils;

import android.app.Activity;
import android.content.res.Configuration;
import android.util.Log;

/**
 * 用于存储屏幕的尺寸, 和屏幕方向有关
 * 屏幕的尺寸不包括状态栏
 * 根据屏幕方向的不同, 创建两个实例,并且只能有这两个实例.
 * @author zhangjg
 *
 */
public class ESPRootSize {
	
	private static final String TAG = "ScreenSize";

	private int width;
	private int heigth;
	
	private static  ESPRootSize  verticalRootSize = null;  //竖屏时的尺寸
	private static  ESPRootSize  horizontalRootSize = null;  //横屏时的尺寸
	
	
	private ESPRootSize(){}
	
	public static ESPRootSize getInstance(Activity a){
		
		//获取配置信息
		Configuration conf = a.getResources().getConfiguration();
		
		//判断屏幕方向
		if(conf.orientation == Configuration.ORIENTATION_PORTRAIT){ //竖屏
			
			if(verticalRootSize == null){
				synchronized(ESPRootSize.class){
					
					if(verticalRootSize == null){
						verticalRootSize = new ESPRootSize();
						verticalRootSize.width = ScreenUtils.getScreenWidth(a);
						verticalRootSize.heigth = ScreenUtils.getScreenHeight(a) - ScreenUtils.getStatusHeight(a);
						
						Log.i(TAG, " size.width=" + verticalRootSize.width + ", size.height=" + verticalRootSize.heigth );

					}
				}
			}
			
			return verticalRootSize;
			
		}else if(conf.orientation == Configuration.ORIENTATION_LANDSCAPE){ //横屏
			
			if(horizontalRootSize == null){
				synchronized(ESPRootSize.class){
					
					if(horizontalRootSize == null){
						horizontalRootSize = new ESPRootSize();
						horizontalRootSize.width = ScreenUtils.getScreenWidth(a);
						horizontalRootSize.heigth = ScreenUtils.getScreenHeight(a) - ScreenUtils.getStatusHeight(a);
						
						Log.i(TAG, " size.width=" + horizontalRootSize.width + ", size.height=" + horizontalRootSize.heigth );

					}
				}
			}
			
			return horizontalRootSize;
		}
		
		return null;
		
	}
	
	public int getWidth() {
		return width;
	}

	public void setWidth(int width) {
		this.width = width;
	}

	public int getHeigth() {
		return heigth;
	}

	public void setHeigth(int heigth) {
		this.heigth = heigth;
	}
	

}
