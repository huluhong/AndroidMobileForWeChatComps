package com.pansoft.espcomp.utils;

import android.graphics.Typeface;
import android.view.Gravity;
import android.widget.TextView;

/**
 * 定义一些组件中的常量，如字体类型
 * 定义一些对组件的通用操作,如设置字体样式和字体颜色等基本操作
 * @author zhangjg
 *
 */
public class ESPCompUtils {
	
	public static final String FONT_NORMAL = "N";   //字体样式 - 普通
	public static final String FONT_BOLD = "B";		//字体样式 - 加粗
	public static final String FONT_ITALIC = "I";	//button字体样式 - 斜体
	
	public static final int TEXT_ALIGN_LEFT = 0;   //字体在控件中显示的位置 - 靠左
	public static final int TEXT_ALIGN_CENTER = 1; //字体在控件中显示的位置 - 居中
	public static final int TEXT_ALIGN_RIGHT = 2;	//字体在控件中显示的位置 - 靠右
	
	/**
	 * 设置组件的字体样式
	 * @param tv
	 * @param fontStyle
	 */
	public static void setTextStyle(TextView tv, String fontStyle){
		
		
		if (FONT_NORMAL.equals(fontStyle)) { // 普通字体
			tv.setTypeface(Typeface.DEFAULT);

		} else if (FONT_BOLD.equals(fontStyle)) { // 粗体
			tv.setTypeface(Typeface.DEFAULT_BOLD);

		} else if (FONT_ITALIC.equals(fontStyle)) { // 斜体
			tv.setTypeface(Typeface.create(Typeface.DEFAULT,
					Typeface.ITALIC));
		}
	}
	
	/**
	 * 设置字体在控件中的位置
	 * @param tv
	 * @param align
	 */
	public static void setTextAlign(TextView tv, int align){
		
		switch (align) {
		case TEXT_ALIGN_LEFT:
			tv.setGravity(Gravity.LEFT);
			break;
			
		case TEXT_ALIGN_CENTER:
			tv.setGravity(Gravity.CENTER_HORIZONTAL);
			break;

		case TEXT_ALIGN_RIGHT:
			tv.setGravity(Gravity.RIGHT);
			break;
		default:
			break;
		}
		
	}
}
