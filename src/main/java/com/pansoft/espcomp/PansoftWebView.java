package com.pansoft.espcomp;

/**
 * lch
 * 7-15
 * webview
 */
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URLDecoder;



import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.webkit.DownloadListener;
import android.webkit.WebView;
import android.widget.PopupMenu;
import android.widget.Toast;

import com.efounder.broadcast.ESPWebNotification;
import com.efounder.util.LoadingDataUtilBlack;

public class PansoftWebView extends WebView {
	JsToAndroidInterface jsToAndroidInterface ;
	LoadInterface loadInterface;
	ProgressDialog progressBar;
	Context mContext;
	ESPWebNotification espWebNotification ;
	
	public PansoftWebView(Context context, AttributeSet attrs) {
		super(context, attrs);
		mContext = context;
		this.getSettings().setJavaScriptEnabled(true);
		//添加javascriptinterface，这个方法重要，this，和后面的string为js中调用android程序提供标志。
		this.addJavascriptInterface(this, "pansoft");
		//progressBar = ProgressDialog.show(context, "MaxPowerSoft Example", "Loading...");  
		
		/*** 打开本地缓存提供JS调用 **/
		getSettings().setDomStorageEnabled(true);
		// Set cache size to 8 mb by default. should be more than enough
		getSettings().setAppCacheMaxSize(1024 * 1024 * 8);
		// This next one is crazy. It's the DEFAULT location for your app's
		// cache
		// But it didn't work for me without this line.
		// UPDATE: no hardcoded path. Thanks to Kevin Hawkins
		String appCachePath = context.getApplicationContext().getCacheDir()
				.getAbsolutePath();
		getSettings().setAppCachePath(appCachePath);
		getSettings().setAllowFileAccess(true);
		getSettings().setAppCacheEnabled(true);
		
				
		this.setOnKeyListener(new View.OnKeyListener() {    
            @Override    
            public boolean onKey(View v, int keyCode, KeyEvent event) {    
                if (event.getAction() == KeyEvent.ACTION_DOWN) {    
                    if (keyCode == KeyEvent.KEYCODE_BACK && PansoftWebView.this.canGoBack()) {  //表示按返回键 时的操作  
                    	PansoftWebView.this.goBack();   //后退     
                        //webview.goForward();//前进   
                        return true;    //已处理     
                    }    
                }    
                return false;    
            }    
        });
		
	}
	//js android 交互标示的方法（无参数）
	public void jsToAndroid(){
		jsToAndroidInterface.jsToAndroid();
	}
	// js android 交互标示的方法（有参数）
	public void jsToAndroid(String xx) {
		System.out.println("...................jsToAndroid" + xx);
		// FIXME 有时候不显示“正在加载数据”
		if (xx.equals("start")) {
		/*	Log.i("test-form-time", "startLoadForm -----> formCreated 耗时: " + (System.currentTimeMillis() - Constant.currentTimeMillis) + " 毫秒");
			new Handler().postDelayed(new Runnable() {

				@Override
				public void run() {
					LoadingDataUtilBlack.show(espContext, "正在加载数据");
				}
			}, 50);*/
			
		} else if (xx.equals("stop")) {
			LoadingDataUtilBlack.dismiss();

			// LoadingDataUtilBlack.show("正在加载数据");
		} else if (xx.endsWith("OpenDialogForJH")){
			jsToAndroidInterface.jsToAndroid("OpenDialogForJH");
		}else if (xx.endsWith("CloseDialogForJH")){
			jsToAndroidInterface.jsToAndroid("CloseDialogForJH");
		}
	}
	
    //load回调
	public interface LoadInterface
	{
		public void load(String url);
	    public void Reload(String url);

	}
	
	
	//js android 交互回调
	public interface JsToAndroidInterface
	{
	    public void jsToAndroid();
	    public void jsToAndroid(String xx);
	    public String getIfNeedRedis();
	}
	public void setJsToAndroidInterface(JsToAndroidInterface jsToandroidInterface){
		this.jsToAndroidInterface = jsToandroidInterface;
	}
	public void setLoadInterface(LoadInterface loadinterface){
		this.loadInterface = loadinterface;
	}
	public String getFromAssets(String fileName){ 
        try {
            InputStreamReader inputReader = new InputStreamReader(getResources().getAssets().open(fileName)); 
            BufferedReader bufReader = new BufferedReader(inputReader);
            String line="";
            String Result="";
            while((line = bufReader.readLine()) != null)
                Result += line;
            return Result;
        } catch (Exception e) { 
            e.printStackTrace(); 
        }
        return null;
} 
	
	

}
