package com.pansoft.espcomp.interfaces;

import com.efounder.builder.base.data.DataSetEvent;
import com.efounder.builder.base.data.EFDataSet;
import com.efounder.builder.base.data.EFRowSet;
import com.pansoft.espmodel.interfaces.DataSetComponent;

public interface DMComponent {
	// 与组件相关的操作
	public DataSetComponent getDataSetComponent() ;
	public void setDataSetComponent(DataSetComponent dsc);
	public void setDataSetID(String dataSetID) ;
	public String getDataSetID();
	public void setDataSet(EFDataSet dataSet);
	public EFDataSet getDataSet();
	public EFRowSet getMainRowSet();
	//dataSet改变处理
	public void dataSetChanged(DataSetEvent event);
	// 事件监听器
	//public void dataSetComponentListener(DataSetComponentEvent dataSetComponentEvent) :void;	
}
