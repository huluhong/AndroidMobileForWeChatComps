package com.pansoft.espcomp;

import java.util.ArrayList;
import java.util.List;

import com.pansoft.android.espbase.R;
import com.pansoft.espcomp.KCalendar.OnCalendarClickListener;
import com.pansoft.espcomp.KCalendar.OnCalendarDateChangedListener;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.provider.ContactsContract.CommonDataKinds.Organization;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;


public class DateWindows extends PopupWindow {

	String date = null;// 设置默认选中的日期 格式为 “2014-04-05” 标准DATE格式
	Context context;
	private DateOnclickListener dateOnclickListener; // 日历翻页回调
	public DateWindows(Context mContext, View parent) {
        context = mContext;
        LinearLayout ll = new LinearLayout(mContext);
        LinearLayout.LayoutParams p = new LinearLayout.LayoutParams(     
                LinearLayout.LayoutParams.FILL_PARENT,     
                LinearLayout.LayoutParams.WRAP_CONTENT     
        );  
        
        ll.setLayoutParams(p);
        ll.setOrientation(LinearLayout.VERTICAL);
        ImageView arrowUp = new ImageView(mContext);
        arrowUp.setLayoutParams(new LayoutParams(50,50));  //TODO
        arrowUp.setImageResource(R.drawable.calendar_uarrow); //TODO
        ll.addView(arrowUp);
        int w = View.MeasureSpec.makeMeasureSpec(0,  
                View.MeasureSpec.UNSPECIFIED);  
        int h = View.MeasureSpec.makeMeasureSpec(0,  
                View.MeasureSpec.UNSPECIFIED); 
        arrowUp.measure(w, h);
		View view = View.inflate(mContext, R.layout.popupwindow_calendar,
				null);//TODO
		view.startAnimation(AnimationUtils.loadAnimation(mContext,
				R.anim.fade_in));
		LinearLayout ll_popup = (LinearLayout) view
				.findViewById(R.id.ll_popup);
		ll_popup.startAnimation(AnimationUtils.loadAnimation(mContext,
				R.anim.push_bottom_in_1));
		
		ll.addView(view);

		setWidth(LayoutParams.FILL_PARENT);
		setHeight(LayoutParams.WRAP_CONTENT);
		setBackgroundDrawable(new BitmapDrawable());
		setFocusable(true);
		setOutsideTouchable(true);
		setContentView(ll);
    	int[] location = new int[2];  
	    parent.getLocationInWindow(location);
	    arrowUp.setPadding(location[0]+parent.getMeasuredWidth()/2-arrowUp.getMeasuredWidth()/2, 0, 0, 0);
		//showAtLocation(parent, Gravity.TOP, 0, 0);
	        showAsDropDown(parent);
		update();

		final TextView popupwindow_calendar_month = (TextView) view
				.findViewById(R.id.popupwindow_calendar_month);
		final KCalendar calendar = (KCalendar) view
				.findViewById(R.id.popupwindow_calendar);
		/*Button popupwindow_calendar_bt_enter = (Button) view
				.findViewById(R.id.popupwindow_calendar_bt_enter);*/

		popupwindow_calendar_month.setText(calendar.getCalendarYear() + "年"
				+ calendar.getCalendarMonth() + "月");

		if (null != date) {

			int years = Integer.parseInt(date.substring(0,
					date.indexOf("-")));
			int month = Integer.parseInt(date.substring(
					date.indexOf("-") + 1, date.lastIndexOf("-")));
			popupwindow_calendar_month.setText(years + "年" + month + "月");

			calendar.showCalendar(years, month);
			calendar.setCalendarDayBgColor(date,
					R.drawable.calendar_date_focused);
		}

		List<String> list = new ArrayList<String>(); // 设置标记列表
		list.add("2014-04-01");
		list.add("2014-04-02");
		calendar.addMarks(list, 0);

		// 监听所选中的日期
		calendar.setOnCalendarClickListener(new OnCalendarClickListener() {

			public void onCalendarClick(int row, int col, String dateFormat) {
				if (dateFormat != null && !"".equals(dateFormat)) {
					int month = Integer.parseInt(dateFormat.substring(
							dateFormat.indexOf("-") + 1,
							dateFormat.lastIndexOf("-")));

					if (calendar.getCalendarMonth() - month == 1// 跨年跳转
							|| calendar.getCalendarMonth() - month == -11) {
						calendar.lastMonth();

					} else if (month - calendar.getCalendarMonth() == 1 // 跨年跳转
							|| month - calendar.getCalendarMonth() == -11) {
						calendar.nextMonth();

					} 
					calendar.removeAllBgColor();
					calendar.setCalendarDayBgColor(dateFormat,
							R.drawable.calendar_date_focused);
					date = dateFormat;// 最后返回给全局 date
					dateOnclickListener.ondateCalendarClick(date);
//					calendar.setCalendarDayBgColor(date,Color.WHITE );
				}
			}
		});

		// 监听当前月份
		calendar.setOnCalendarDateChangedListener(new OnCalendarDateChangedListener() {
			public void onCalendarDateChanged(int year, int month) {
				popupwindow_calendar_month
						.setText(year + "年" + month + "月");
			}
		});

		// 上月监听按钮
		RelativeLayout popupwindow_calendar_last_month = (RelativeLayout) view
				.findViewById(R.id.popupwindow_calendar_last_month);
		popupwindow_calendar_last_month
				.setOnClickListener(new OnClickListener() {

					public void onClick(View v) {
						calendar.lastMonth();
					}

				});

		// 下月监听按钮
		RelativeLayout popupwindow_calendar_next_month = (RelativeLayout) view
				.findViewById(R.id.popupwindow_calendar_next_month);
		popupwindow_calendar_next_month
				.setOnClickListener(new OnClickListener() {

					public void onClick(View v) {
						calendar.nextMonth();
					}
				});

		// 关闭窗口
	/*	popupwindow_calendar_bt_enter
				.setOnClickListener(new OnClickListener() {

					public void onClick(View v) {
						dismiss();
					}
				});*/
	}
	/**
	 * onClick接口回调
	 */
	public interface DateOnclickListener {
		void ondateCalendarClick(String date);
	}

	public DateOnclickListener getDateOnclickListener() {
		return dateOnclickListener;
	}
	public void setDateOnclickListener(DateOnclickListener dateOnclickListener) {
		this.dateOnclickListener = dateOnclickListener;
	}
}