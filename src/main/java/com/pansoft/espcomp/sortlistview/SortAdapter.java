package com.pansoft.espcomp.sortlistview;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.SectionIndexer;
import android.widget.TextView;

import com.efounder.thirdpartycomps.stickylistheaders.StickyListHeadersAdapter;
import com.pansoft.android.espbase.R;

import java.util.List;
import java.util.Locale;

@Deprecated
/**
 * 代码已弃用，会闪退 请勿使用
 */
public class SortAdapter extends BaseAdapter implements StickyListHeadersAdapter,SectionIndexer {
	private List<SortModel> list = null;
	private Context mContext;

	public SortAdapter(Context mContext, List<SortModel> list) {
		this.mContext = mContext;
		this.list = list;
	}

	/**
	 * 当ListView数据发生变化时,调用此方法来更新ListView
	 * 
	 * @param list
	 */
	public void updateListView(List<SortModel> list) {
		this.list = list;
		notifyDataSetChanged();
	}

	public int getCount() {
		return this.list.size();
	}

	public Object getItem(int position) {
		return list.get(position);
	}

	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(final int position, View view, ViewGroup arg2) {
		ViewHolder viewHolder = null;
		final SortModel mContent = list.get(position);
		if (view == null) {
			viewHolder = new ViewHolder();
//			view = LayoutInflater.from(mContext).inflate(R.layout.contacts_item, null);
//			viewHolder.text = (TextView) view.findViewById(R.id.title);
//			view.setTag(viewHolder);
		} else {
			viewHolder = (ViewHolder) view.getTag();
		}

		// 根据position获取分类的首字母的Char ascii值
		int section = getSectionForPosition(position);

//		// 如果当前位置等于该分类首字母的Char的位置 ，则认为是第一次出现
//		if (position == getPositionForSection(section)) {
//			viewHolder.tvLetter.setVisibility(View.VISIBLE);
//			viewHolder.tvLetter.setText(mContent.getSortLetters());
//		} else {
//			viewHolder.tvLetter.setVisibility(View.GONE);
//		}

		viewHolder.text.setText(this.list.get(position).getName());

		return view;

	}
	@Override
	public View getHeaderView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		HeaderViewHolder holder;

        if (convertView == null) {
            holder = new HeaderViewHolder();
            //convertView = LayoutInflater.from(mContext).inflate(R.layout.contacts_header, parent, false);
            holder.text = (TextView) convertView.findViewById(R.id.header);
            convertView.setTag(holder);
        } else {
            holder = (HeaderViewHolder) convertView.getTag();
        }

        // set header text as first char in name
        CharSequence headerChar = list.get(position).getSortLetters();
        holder.text.setText(headerChar);

		return convertView;
	}


	/**
	 * 根据ListView的当前位置获取分类的首字母的Char ascii值
	 */
	@Override
	public int getSectionForPosition(int position) {
		return list.get(position).getSortLetters().charAt(0);
	}

	/**
	 * 根据分类的首字母的Char ascii值获取其第一次出现该首字母的位置
	 */
	@Override
	public int getPositionForSection(int section) {
		for (int i = 0; i < getCount(); i++) {
			String sortStr = list.get(i).getSortLetters();
			char firstChar = sortStr.toUpperCase(Locale.getDefault()).charAt(0);
			if (firstChar == section) {
				return i;
			}
		}

		return -1;
	}

	@Override
	public Object[] getSections() {
		return list.toArray();
	}

	
	@Override
	public long getHeaderId(int position) {
		// TODO Auto-generated method stub
		return list.get(position).getSortLetters().charAt(0);
	}
	
	class HeaderViewHolder {
        TextView text;
    }
	class ViewHolder {
        TextView text;
    }

}