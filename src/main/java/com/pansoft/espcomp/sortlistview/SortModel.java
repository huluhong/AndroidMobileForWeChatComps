package com.pansoft.espcomp.sortlistview;

import java.io.Serializable;

public class SortModel implements Serializable {

	private String name; // 显示的数据
	private String sortLetters; // 显示数据拼音的首字母
	private String number;//联系人电话
	private Long contactsID;//联系人ID

	public Long getContactsID() {
		return contactsID;
	}

	public void setContactsID(Long contactsID) {
		this.contactsID = contactsID;
	}

	public SortModel() {
		super();
	}

	public SortModel(String name, String sortLetters, String number, Long contactsID) {
		super();
		this.name = name;
		this.sortLetters = sortLetters;
		this.number = number;
		this.contactsID = contactsID;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSortLetters() {
		return sortLetters;
	}

	public void setSortLetters(String sortLetters) {
		this.sortLetters = sortLetters;
	}
}