package com.pansoft.espmodel;

import java.util.ArrayList;
import java.util.List;

/*
 * author lch
 * 表格组件，实现嵌套所以设计的
 * 
 */
public class DMGridModel {
	public DMGridModel models;
	public List<DMGridModel> children;
	/* make为此model的标记位兼标题tilte*/
	public String make;
	public DMGridModel(){
	/*	this.make = make;
		this.models = models;*/
	}
	public DMGridModel getModels() {
		return models;
	}
	public void setModels(DMGridModel models) {
		this.models = models;
	}
	public List<DMGridModel> getChildren() {
		return children;
	}
	public void setChildren(List<DMGridModel> list) {
		this.children = list;
	}
	public String getMake() {
		return make;
	}
	public void setMake(String make) {
		this.make = make;
	}
}
