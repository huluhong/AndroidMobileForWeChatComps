package com.pansoft.espmodel;

import java.io.Serializable;
import java.util.HashMap;

public class EFWebNoticationObject implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 20151015L;
	HashMap<String, String> map;
	String id ;
	String caption;
	String type;
	String[] param;
	public String[] getParam() {
		return param;
	}

	public void setParam(String[] param) {
		this.param = param;
	}

	public EFWebNoticationObject(){
		map = new HashMap<String, String>();
	}
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
		map.put("id", id);
	}
	public String getCaption() {
		return caption;
	}
	public void setCaption(String caption) {
		this.caption = caption;
		map.put("caption", caption);
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
		map.put("type", type);
	}
	
	public HashMap<String, String> getMap() {
		return map;
	}
}