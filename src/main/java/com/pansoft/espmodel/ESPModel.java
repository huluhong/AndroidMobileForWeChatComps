package com.pansoft.espmodel;

import android.os.Handler;
import android.util.Log;

/**
 * 所有model的父类,用来处理view需要的数据
 * 基本成员变量为handler、id、lable、scriptObject
 * @author yanxianhai
 *
 */
public class ESPModel {
	
	/**主线程的句柄，用来通知controler来更新引用当前model的view*/
	private Handler handler;
	
	/**模型id*/
	private String id;
	
	/***/
	private String label;
	
	
	/**当前模型对应的脚本*/
	private String scriptObject;
	
	/**
	 * 构造函数
	 * @param handler 主线程的句柄
	 */
	public ESPModel(Handler handler){
		this.handler=handler;
	}
	/**
	 * 
	 */
	public void addChildModel(ESPModel espModel){
		Log.v("test","espmodel:add");
	}
	/**
	 * Model加载数据,子类需要实现该方法
	 */
	public void loadData(){}
	
	/**
	 * 修改model里面的数据，子类需要实现该方法
	 */
	public void setData(){}
	
	/**
	 * 取消正在加载数据的当前model，用来取消loadData比较耗时的操作，子类需要实现该方法
	 */
	public void cancelData(){}
	
	public Handler getHandler() {
		return handler;
	}

	public void setHandler(Handler handler) {
		this.handler = handler;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}
	
	public String getScriptObject() {
		return scriptObject;
	}

	public void setScriptObject(String scriptObject) {
		this.scriptObject = scriptObject;
	}
    
}
