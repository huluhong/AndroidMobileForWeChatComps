package com.pansoft.espmodel;

import com.efounder.builder.base.data.EFDataSet;
import com.efounder.bz.flow.drive.NodeTaskDataSet;
import com.efounder.eai.data.JParamObject;
import com.efounder.eai.data.JResponseObject;
import com.efounder.form.EFFormDataModel;
import com.pansoft.espcomp.interfaces.DMComponent;
import com.pansoft.espflow.IFlowDriveDataObject;
import com.pansoft.espform.base.DataComponent;
import com.pansoft.espmodel.interfaces.DataSetComponent;
import com.pansoft.espservice.ESPService;
import com.pansoft.utils.CommonPo;

import java.util.ArrayList;
import java.util.List;

public class FormModel extends DataComponent implements DataSetComponent, IFlowDriveDataObject{
	
	protected JParamObject paramObject= null;
	//private DataContainer dataContainer;
	public static int RES_OK=0;
	private EFFormDataModel formDataModel;
	private NodeTaskDataSet nodeTaskDataSet;
	private String bizMDLID;
	private List<DMComponent> dmCompList;
	//private ESPService providerService;

	public void loadByDJBHWithoutMetaData(String ZZJG , String DATE, String DJBH) throws Exception{
	   // JParamObject po = this.getParamObject(null);
		CommonPo.setPoToNull();
		JParamObject po = CommonPo.getPo();
	    po.SetValueByParamName("BIZ_UNIT", ZZJG);
	    po.SetValueByParamName("BIZ_DATE", DATE);
	    po.SetValueByParamName("BIZ_DJBH", DJBH);
	    
	    po.SetValueByParamName("BILL_TJLX", "COMM");
	    
	   /* po.SetValueByParamName("QXBZW", "2");
	    po.SetValueByParamName("SELF_SUBMIT_TASK", "1");
    	//PO.SetValueByParamName("MDL_UNIT_DCTIDS", "BZZZJGZD");
	    po.SetValueByParamName("MDL_UNIT_DCTIDS", "YHZZJG");
	    po.SetValueByParamName("_CHILD_FLOW_DISP_TYPE_", "1");
	    po.SetValueByParamName("taskDataSetID", "TASKPendingDataSet");*/
    	
    	
    	
	    this.loadFormDataModel(po);
	    //[self loadFormDataModelWithPO:po];
	}

	public void loadFormDataModel(JParamObject po) throws Exception{
		po.SetValueByParamName("MDL_ID", this.bizMDLID);
	    //[po setParamWithObject:self.bizMDLID forKey:@"MDL_ID"];
	    if(this.dataContainer != null){
	    	po.setValue("OP_ID", this.dataContainer.getValue("OP_ID", ""));
	    	po.setValue("POP_ID", this.dataContainer.getValue("POP_ID", ""));
	    	po.setValue("TASK_UNIT", this.dataContainer.getValue("TASK_UNIT", ""));
	    	po.setValue("FLOW_ID", this.dataContainer.getValue("FLOW_ID", ""));
	    	po.setValue("BIZ_DJBH", this.dataContainer.getValue("BIZ_DJBH", ""));
	    	po.setValue("NODE_ID", this.dataContainer.getValue("NODE_ID", ""));
	    	po.setValue("OBJ_GUID", this.dataContainer.getValue("OBJ_GUID", ""));
	    	
	    	/*po.SetValueByParamName("OP_ID", this.dataContainer.getValue("OP_ID", "").toString());
	    	po.SetValueByParamName("POP_ID", this.dataContainer.getValue("POP_ID", "").toString());
	    	po.SetValueByParamName("TASK_UNIT", this.dataContainer.getValue("TASK_UNIT", "").toString());
	    	po.SetValueByParamName("FLOW_ID", this.dataContainer.getValue("FLOW_ID", "").toString());
	    	po.SetValueByParamName("BIZ_DJBH", this.dataContainer.getValue("BIZ_DJBH", "").toString());
	    	po.SetValueByParamName("NODE_ID", this.dataContainer.getValue("NODE_ID", "").toString());
	    	po.SetValueByParamName("OBJ_GUID", this.dataContainer.getValue("OBJ_GUID", "").toString());*/
	    	
	    }
	    if(this.providerService == null) return;
	    JResponseObject ro = providerService.executeService(po, null, null, null, null, null, null);
	    if(ro != null && ro.ErrorCode == RES_OK){
	        this.formDataModel = (EFFormDataModel)ro.ResponseObject;
	        if(ro.getResponseMap() != null){
	            this.nodeTaskDataSet = (NodeTaskDataSet)ro.getResponseMap().get("FLOW_TASK_LIST");//ResponseMap objectForKey:@"FLOW_TASK_LIST"];
	        }
	        
	        if(this.formDataModel == null || this.formDataModel.getBillDataSet() == null) return;
	        //this.nc = [NSNotificationCenter defaultCenter];
	        //[self.nc addObserver:self selector:@selector(dataSetChanged:) name:@"dataSetChanged" object:nil];
	        
	        this.setActiveDataSet(this.formDataModel.getBillDataSet());
	    }
	}
	/**
	 * 获取参数对象
	 */
	public JParamObject getParamObject(JParamObject paramObject)
	{
		//没有定义业务模型
		if ( bizMDLID == null || (bizMDLID.replace(" ", "")).length() == 0 ) return null;
		// 创建参数对象
		if ( this.paramObject == null )
		{
			
			JParamObject PO = CommonPo.getPo();
			//JParamObject PO = new JParamObject();
		/*	PO.setEnvValue("DBNO", "ZYHYGL01");
			PO.setEnvValue("ServerURL","http://192.168.248.201:8080/EnterpriseServer" );
			PO.setEnvValue("licenseID", "753B7C4C94467F1C31BC20CB3E913E6D08F81F676704C4F7");// 胜利
			PO.setEnvValue("Product", "IOS");
			PO.setEnvValue("Password","pghy123456");
			PO.setEnvValue("productPath","http://192.168.248.201:8080/EnterpriseServer/Android");
		
			PO.setEnvValue("UserName", "P0034"); // 数据服务
			PO.setEnvValue("DataBaseName", "ZYYT_DB01");// 数据标示
			PO.setEnvValue("UserPass","pghy123456");
			//PO.setString("ServiceKey", "FBWPProvider");
			//PO.setEnvValue("ServiceKey", "FBWPProvider");
*/		    this.paramObject = PO;
		    /*	PO.setEnvValue("ServerURL","http://10.75.131.128:8080/EnterpriseServer");
			PO.setEnvValue("productPath","http://10.75.131.128:8080/EnterpriseServer/Android");*/
			//PO.setEnvValue("ServerURL","http://zyesp.zyof.com.cn:8085/EnterpriseServer");
			//PO.setEnvValue("productPath","http://zyesp.zyof.com.cn:8085/EnterpriseServer/Android");
		    /*PO.setEnvValue("UserName","NM029");
	        PO.setEnvValue("UserPass", "");*/
			//this.paramObject = CommonPo.getPo();
		}else
		{
			this.paramObject = paramObject;
		}
		/*
		// 如果没有设置DataBase，则先创建DataBase
		if ( dataBase == null )
		{
			dataBase = new DataBase();
		}
		
		// 如果参数转换器为空，则先创建
		if ( paramConverter == null )
		{
			paramConverter = new ParamConverter();
		}
		
		// 通过参数转换器及DataBase组件设置应用服务及自定义参数
		paramConverter.convertParam(this, _paramObject);
		*/
		return this.paramObject;
	}
	/**
	 * 设置数据集
	 * @param dataSet EFDataSet
	 */
	void setActiveDataSet(EFDataSet dataSet)
	{
	    if (dataSet == null) return;
	    if (dmCompList == null || dmCompList.size() == 0) return;
	    for (int i=0; i<dmCompList.size(); i++)
	    {
	    	DMComponent dmComponent=dmCompList.get(i);
	        if (dmComponent.getDataSetID().endsWith(getDataSet().getTableName()))
	        {
	            dmComponent.setDataSet(dataSet);
	        }
	    }
	}

//	void dataSetChanged(NSNotification *)notification{
//	    NSDictionary *dic = [notification userInfo];
//	    DataSetEvent *dataSetEvent = [dic objectForKey:@"dataSetEvent"];
//	    
//	    for (id<DMComponent> dmComponent in _dmCompList) {
//	        [dmComponent dataSetChanged:dataSetEvent];
//	    }
//	}
	/**
	 * 根据guid加载数据，不需要加载元数据的单据加载
	 * @param 
	 * @param
	 * @throws Exception 
	 */
	public void loadByGuidWithoutMetaData(String F_GUID, String guidColID) throws Exception
	{
		JParamObject po= getParamObject(null);
		if (po == null) return;
		//设置参数
		po.setValue(guidColID, F_GUID);
		//加载业务表单数据
		this.loadFormDataModel(po, null);
	}
	/**
	 * 加载业务表单数据模型
	 * @param paramObject JParamObject
	 * @param operateType String 加载完单据的操作类型（复制，冲销）
	 * @throws Exception 
	 */
	private void loadFormDataModel(JParamObject paramObject, String operateType) throws Exception
	{
		// 设置业务模型标志
		paramObject.setValue("MDL_ID", this.bizMDLID);
		// 在待办任务中查看单据状态时，设置此参数
		paramObject.setValue("OP_ID", this.dataContainer.getValue("OP_ID", ""));
		paramObject.setValue("POP_ID", this.dataContainer.getValue("POP_ID", ""));
		paramObject.setValue("TASK_UNIT", this.dataContainer.getValue("TASK_UNIT", ""));
		
		// 没有定义数据加载服务
		if (providerService == null) return; 
//		// 等待窗口
//		//WaitingWindow.begin("正在加载业务数据，请稍候...");
//		StubObject userObject= new StubObject();
//		userObject.setString("operateType", operateType);
		// 执行数据加载服务
		JResponseObject ro=providerService.executeService(paramObject, null, null, null, null, null, null);
		
		if(ro != null && ro.ErrorCode == RES_OK){
			this.formDataModel = (EFFormDataModel)ro.ResponseObject;
		    if(ro.getResponseMap() != null){
		    	this.nodeTaskDataSet = (NodeTaskDataSet)ro.getResponseMap().get("FLOW_TASK_LIST");
		    }    
		    if(this.formDataModel == null || this.formDataModel.getBillDataSet() == null) return;
//		    	self.nc = [NSNotificationCenter defaultCenter];
//		        [self.nc addObserver:self selector:@selector(dataSetChanged:) name:@"dataSetChanged" object:nil];
		        this.setActiveDataSet(formDataModel.getBillDataSet());
		    }
	}
	protected ESPService providerService = null;
	/**
	 * 单据加载服务
	 */
	public ESPService getproviderService()
	{
		return providerService;
	}
	public void setproviderService(ESPService es)
	{
		providerService = es;
	}
	
	@Override
	public String getDataObjectGUID() {
		// TODO Auto-generated method stub
		  if (nodeTaskDataSet == null) return null;
		    return nodeTaskDataSet.getOBJ_GUID();
	}
	@Override
	public String getDataObjectBIZ_UNIT() {
		// TODO Auto-generated method stub
		 if(nodeTaskDataSet != null){
		        String bizUnit = nodeTaskDataSet.getBIZ_UNIT();
		        if(bizUnit != null && bizUnit.replace(" ", "").length() > 0)
		            return bizUnit;
		    }
		    if(formDataModel != null)
		        return formDataModel.getBIZ_UNIT();
		    return null;
	}
	@Override
	public String getDataObjectBIZ_DATE() {
		// TODO Auto-generated method stub
		 if(nodeTaskDataSet != null){
		        String bizDate = nodeTaskDataSet.getBIZ_DATE();
		        if(bizDate != null && bizDate.replace(" ", "").length() > 0)
		            return bizDate;
		    }
		    if(formDataModel != null)
		        return formDataModel.getBIZ_DATE();
		    return null;
	}
	@Override
	public String getDataObjectBIZ_DJBH() {
		// TODO Auto-generated method stub
		if(nodeTaskDataSet != null){
	       String bizDJBH = nodeTaskDataSet.getBIZ_DJBH();
	        if(bizDJBH != null && bizDJBH.replace(" ", "").length() > 0)
	            return bizDJBH;
	    }
	    if(formDataModel != null)
	        return formDataModel.getBIZ_DJBH();
	    return null;
	}
	@Override
	public NodeTaskDataSet getNodeTaskDataSet() {
		// TODO Auto-generated method stub
		return nodeTaskDataSet;
	}
	@Override
	public String getMDL_ID() {
		// TODO Auto-generated method stub
		 return this.bizMDLID;
	}
	@Override
	public void setMDL_ID(String aMDL_ID){
	    this.bizMDLID = aMDL_ID;
	}
	@Override
	public String getFLOW_ID() {
		// TODO Auto-generated method stub
		 if(nodeTaskDataSet == null) return null;
		    return nodeTaskDataSet.getFLOW_ID();
	}
	@Override
	public String getNODE_ID() {
		// TODO Auto-generated method stub
		   if(nodeTaskDataSet == null) return null;
		    return nodeTaskDataSet.getNODE_ID();
	}
	@Override
	public void setNODE_ID(String node_ID) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public List<DMComponent> getDmCompList() {
		// TODO Auto-generated method stub
		return dmCompList;
	}
	@Override
	public void setDmCompList(List<DMComponent> value) {
		// TODO Auto-generated method stub
		dmCompList=value;
	}
	@Override
	public EFDataSet getDataSet(String dataSetKey) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public List<String> getDataSetKey() {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public List<String> getDOMetaData(String obj_id) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public void insertDMComponent(DMComponent dmComponent) {
		// TODO Auto-generated method stub
		if(dmCompList== null){
			dmCompList=new ArrayList<DMComponent>();
		}
		dmCompList.add(dmComponent);
	}
	@Override
	public void removeDMComponent(DMComponent dmComponent) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public Object loadByParam(Object param, Object userObject) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public void setNodeTaskDataSet(NodeTaskDataSet ntd) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void setFLOW_ID(String flow_ID) {
		// TODO Auto-generated method stub
		
	}
	public EFFormDataModel getFormDataModel() {
		return formDataModel;
	}
	public void setFormDataModel(EFFormDataModel formDataModel) {
		this.formDataModel = formDataModel;
	}
	public String getBizMDLID() {
		return bizMDLID;
	}
	public void setBizMDLID(String bizMDLID) {
		this.bizMDLID = bizMDLID;
	}
	
}
