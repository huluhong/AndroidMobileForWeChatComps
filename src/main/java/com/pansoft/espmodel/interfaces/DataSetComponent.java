package com.pansoft.espmodel.interfaces;

import java.util.List;

import com.efounder.builder.base.data.EFDataSet;
import com.pansoft.espcomp.interfaces.DMComponent;

public interface DataSetComponent {
	// DMComponent列表
	public List<DMComponent> getDmCompList();
	public void setDmCompList(List<DMComponent> value);
    // 与组件相关的操作
	public EFDataSet getDataSet(String dataSetKey);
	public List<String> getDataSetKey();
	public List<String> getDOMetaData(String obj_id) ; // DOMetaData[] 
	public void insertDMComponent(DMComponent dmComponent);
	public void removeDMComponent(DMComponent dmComponent);
	// 与导航相关的操作
    public Object loadByParam(Object param, Object userObject);
 //   public  load(po : Object = null,userObject:Object=null) :Object;
//			public  save(showTooltip : Boolean = true, otherParam : Object = null) :Object;
//			public  first(po : JParamObject = null) :Object;
//			public  prior(po : JParamObject = null) :Object;
//			public  next(po : JParamObject = null) :Object;
//			public  last(po : JParamObject = null) :Object;
//			public  search(rowSet:EFRowSet) :Object;
//			// 与编缉相关的操作
//			public create() :Object;
//			
//			public createItem(itemContentID : String, isAppend : Boolean, customDataSetID : String = null) : EFRowSet;
//			public createPart(partCtnID : String, isAppend : Boolean, customDataSetID : String = null) : EFRowSet;
//			
//			public removeItem(itemContentID : String) : Boolean;
//			public removePart(itemContentID : String) : Boolean;
//			
//			public isModified():Boolean;
//			
//			public canEdit() :Object;
//			public startEdit() :Object;
//			public cancelEdit() :Object;
//			public stopEdit() :Object;
//			public isEditing():Boolean;		
}
