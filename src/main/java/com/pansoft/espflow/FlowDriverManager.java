package com.pansoft.espflow;

import com.efounder.eai.EAI;
import com.efounder.eai.data.JParamObject;
import com.efounder.eai.data.JResponseObject;

public class FlowDriverManager {
	
	/* 流程提交
	 * @param submitSuccessHandler 提交成功处理函数
	 * @param submitErrorHandler 提交处理失败处理函数
	 * @param PO 参数对象
	 * @param userObject
	 */
	public static void submitTask(JParamObject PO) throws Exception{
		EAI.DAL.IOM("FlowTaskService", "submitTask", PO);
	}
	
	/**
	 * 流程取回
	 * @param retakeSuccessHandler 提交成功处理函数
	 * @param retakeErrorHandler 提交处理失败处理函数
	 * @param PO 参数对象
	 * @param userObject
	 * @throws Exception 
	 */
	public static void retakeTask(JParamObject PO) throws Exception
	{
		EAI.DAL.IOM("FlowTaskService","retakeTask", PO);
	}
	
	/**
	 * 流程退回
	 * @param rollbackSuccessHandler 提交成功处理函数
	 * @param rollbackErrorHandler 提交处理失败处理函数
	 * @param PO 参数对象
	 * @param userObject
	 * @throws Exception 
	 */
	public static void rollBackTask(JParamObject PO) throws Exception
	{
		EAI.DAL.IOM("FlowTaskService", "rollBackTask", PO);
	}
	
	/**
	 * 流程重置
	 * zhtbin
	 * @throws Exception 
	 */
	public static void breakTask(JParamObject PO ) throws Exception
	{
		EAI.DAL.IOM("FlowTaskService", "breakTask", PO);//DALFlowTaskService.breakTask
	}
	
	/**
	 * 虚拟自动提交机器，显示虚拟提交流程图
	 * @param paramObject JParamObject
	 * @throws Exception 
	 */
	public static JResponseObject autoSubmitTaskMachine(JParamObject PO) throws Exception
	{
		return EAI.DAL.IOM("FlowTaskService", "autoSubmitTaskMachine", PO);
	}
}
