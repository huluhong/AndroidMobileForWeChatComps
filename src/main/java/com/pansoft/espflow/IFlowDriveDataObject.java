package com.pansoft.espflow;


import com.efounder.bz.flow.drive.NodeTaskDataSet;

public interface IFlowDriveDataObject {
	/**
	 *
	 * @return String
	 */
	String getDataObjectGUID();
	/**
	 *
	 * @return String
	 */
	String getDataObjectBIZ_UNIT();
	/**
	 *
	 * @return String
	 */
	String getDataObjectBIZ_DATE();
	/**
	 *
	 * @return String
	 */
	String getDataObjectBIZ_DJBH();
	/**
	 *
	 * @return NodeTaskDataSet
	 */
	NodeTaskDataSet getNodeTaskDataSet();
	/**
	 *
	 * @param ntd NodeTaskDataSet
	 */
	void setNodeTaskDataSet(NodeTaskDataSet ntd);
	/**
	 *
	 * @return String
	 */
	String getMDL_ID();
	/**
	 *
	 * @return 
	 */
	void setMDL_ID(String aMDL_ID);
	/**
	 *
	 * @return String
	 */
	String getFLOW_ID();
	/**
	 *
	 * @param flow_ID String
	 */
	void setFLOW_ID(String flow_ID);
	/**
	 *
	 * @return String
	 */
	String getNODE_ID();
	/**
	 *
	 * @param node_ID String
	 */
	void setNODE_ID(String node_ID);
}
