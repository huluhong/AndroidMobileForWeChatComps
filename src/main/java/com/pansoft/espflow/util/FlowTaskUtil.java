
package com.pansoft.espflow.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import android.util.Log;

import com.efounder.builder.base.data.EFDataSet;
import com.efounder.builder.base.data.EFRowSet;
import com.efounder.builder.base.data.ESPRowSet;
import com.efounder.eai.EAI;
import com.efounder.eai.data.JParamObject;
import com.efounder.eai.data.JResponseObject;
import com.pansoft.espform.base.DataContainer;
import com.pansoft.espmodel.FormModel;
import com.pansoft.espservice.ESPService;
import com.pansoft.xmlparse.FormatCol;
import com.pansoft.xmlparse.FormatRow;
import com.pansoft.xmlparse.FormatSet;
import com.pansoft.xmlparse.FormatTable;
import com.pansoft.xmlparse.MobileFormatUtil;

public class FlowTaskUtil {
	public static EFRowSet flowRowSet;
	public static EFDataSet getFlowTaskData(JParamObject po, String taskType) throws Exception {
		po.SetValueByParamName("TASK_TYPE", taskType);
		String taskDataSetID = po.GetValueByParamName("taskDataSetID", "");
		JResponseObject ro = null;
		if(taskDataSetID.equals("TASKFinishDataSet")){
			ro = EAI.DAL.IOM("TaskIOService", "loadEndFlowTaskInfo", po);
		}else{
			ro = EAI.DAL.IOM("TaskIOService", "loadFlowTaskInfoNew", po);
		}

		if (ro == null || ro.ErrorCode != 0)
			return null;

		EFDataSet taskDataSet = (EFDataSet) ro.ResponseObject;
		if (taskDataSet == null || taskDataSet.getRowCount() == 0)
			return null;

		EFDataSet pendingDataSet = new EFDataSet();
		pendingDataSet.setTableName(taskDataSetID);
		flowRowSet = null;
		EFRowSet nodeRowSet = null;
		EFRowSet taskRowSet = null;
		for (int i = 0; i < taskDataSet.getRowCount(); i++) {
			flowRowSet = taskDataSet.getRowSet(i);
			EFDataSet nodeDataSet = null;
			nodeDataSet = flowRowSet.getDataSet("NODEDataSet");
			if (nodeDataSet == null){
				nodeDataSet = flowRowSet.getDataSet("TASKFinishDataSet");
			}
			if (nodeDataSet == null)
				continue;
			for (int j = 0; j < nodeDataSet.getRowCount(); j++) {
				nodeRowSet = nodeDataSet.getRowSet(j);
				EFDataSet taskDS = nodeRowSet.getDataSet(taskDataSetID);
				if (taskDS == null)
				{
					taskDS = nodeRowSet.getDataSet("NODEDataSet");
				}
				if (taskDS == null)
					continue;
				for (int k = 0; k < taskDS.getRowCount(); k++) {
					taskRowSet = taskDS.getRowSet(k);
					if (taskRowSet == null)
						continue;
					taskRowSet.putObject("flowRowSet", flowRowSet);
					taskRowSet.putObject("nodeRowSet", nodeRowSet);
					pendingDataSet.addRowSet(taskRowSet);
				}
			}
		}
		return pendingDataSet;
	}

	public static Map<String, Object> getFlowTaskDic(JParamObject po) throws Exception {
		String taskDataSetID = po.GetValueByParamName("taskDataSetID", "");
		JResponseObject ro = null;
		if(taskDataSetID.equals("TASKFinishDataSet")){
			ro = EAI.DAL.IOM("TaskIOService", "loadEndTaskInfoNew", po);
		}else{
			ro = EAI.DAL.IOM("TaskIOService", "loadFlowTaskInfoNew", po);
		}

		if (ro == null || ro.ErrorCode != 0)
			return null;

		// 任务的DataSet
		EFDataSet taskDataSet = (EFDataSet) ro.ResponseObject;
		if (taskDataSet.getRowCount() == 0)
			return null;

		// 返回一个Dic 存放流程ID--DataSet
		Map<String, Object> taskDic = new HashMap<String, Object>();
		// 任务
		EFRowSet flowRowSet = null;
		EFRowSet nodeRowSet = null;
		EFRowSet taskRowSet = null;
		for (int i = 0; i < taskDataSet.getRowCount(); i++) {
			flowRowSet = taskDataSet.getRowSet(i);
			// 每一种流程的信息（UID，NAME，DataSet）
			Map<String, Object> rowDic = new HashMap<String, Object>();
			// 流程ID
			String uid = flowRowSet.getString("FLOW_ID", "");
			// [rowDic setObject:uid forKey:@"UID"];
			rowDic.put("UID", uid);
			// 流程名称
			String name = flowRowSet.getString("FLOW_MC", "");
			rowDic.put("NAME", name);
			// [rowDic setObject:name forKey:@"NAME"];

			EFDataSet nodeDataSet = flowRowSet.getDataSet("NODEDataSet");
			if (nodeDataSet == null)
				continue;
			for (int j = 0; j < nodeDataSet.getRowCount(); j++) {
				nodeRowSet = nodeDataSet.getRowSet(j);
				EFDataSet taskDS = nodeRowSet.getDataSet(taskDataSetID);
				if (taskDS == null)
					continue;
				EFDataSet pendingDataSet = new EFDataSet();// [[[EFDataSet
				// alloc]
				// init]autorelease];
				for (int k = 0; k < taskDS.getRowCount(); k++) {
					taskRowSet = taskDS.getRowSet(k);// .rowSetList
					if (taskRowSet == null)
						continue;
					taskRowSet.putObject("flowRowSet", flowRowSet);
					taskRowSet.putObject("nodeRowSet", nodeRowSet);
					pendingDataSet.addRowSet(taskRowSet);
				}
				rowDic.put(uid, pendingDataSet);
				rowDic.put("COUNT", pendingDataSet.getRowCount());
				// [rowDic setObject:pendingDataSet forKey:uid];
				// [rowDic setObject:[NSString
				// stringWithFormat:@"%d",pendingDataSet.rowSetList.count]
				// forKey:@"COUNT"];
			}
			taskDic.put(uid, rowDic);
			// [taskDic setObject:rowDic forKey:uid];
		}
		return taskDic;
	}

	public static FormModel openTaskWithRowSet(EFRowSet rowSet) throws Exception {
		if (rowSet == null)
			return null;
		FormModel formModel = new FormModel();// [[[FormModel alloc]
		formModel.setBizMDLID(rowSet.getString("MDL_ID", ""));
		if (formModel.getDataContainer() == null) {
			formModel.setDataContainer(new DataContainer());
			formModel.getDataContainer().setValue("OP_ID", rowSet.getString("OP_ID", ""));
			formModel.getDataContainer().setValue("POP_ID", rowSet.getString("POP_ID", ""));
			formModel.getDataContainer().setValue("TASK_UNIT", rowSet.getString("TASK_UNIT", ""));
			formModel.getDataContainer().setValue("FLOW_ID", rowSet.getString("FLOW_ID", ""));
			formModel.getDataContainer().setValue("NODE_ID", rowSet.getString("NODE_TAG", ""));
			formModel.getDataContainer().setValue("BIZ_DJBH", rowSet.getString("BIZ_DJBH", ""));
			formModel.getDataContainer().setValue("OBJ_GUID", rowSet.getString("OBJ_GUID", ""));
		}
		formModel.setproviderService(new ESPService());
		formModel.getproviderService().setServiceKey("FBWPProvider");
		//formModel.getproviderService().setServiceKey("ESPBillProvider");
		formModel.loadByDJBHWithoutMetaData(rowSet.getString("BIZ_UNIT", ""),
				rowSet.getString("BIZ_DATE", ""), rowSet.getString("BIZ_DJBH", ""));
		//formModel.loadByGuidWithoutMetaData(rowSet.getString("OBJ_GUID", ""), "F_GUID");
		return formModel;
	}
	/**
	 * 将EFData转成Map
	 * @author lch
	 * @param dataset 为getdatafromRo的数据
	 * @parm  ft 为从xml转成的格式
	 */
	public static Map<String ,List<EFRowSet>> openTaskFromEFDataSetToMap(EFDataSet dataSet)  {
		Map<String ,List<EFRowSet>> map = new HashMap<String ,List<EFRowSet>>();
		/*set的作用主要是归类，根据Flow_Name归类*/
		Set<String> set=new HashSet<String>();

		for(int i=0;i<dataSet.getRowCount();i++){
			EFRowSet rs = dataSet.getRowSet(i);
			String groupName = rs.getString("FLOW_NAME", "");

			//List<FormatCol> formatColList = ft.getFormatColList();
			//List<FormatRow> formatRowList = ft.getFormatRowList();
			//如果set中没有此类型
			if(!set.contains(groupName)){
				set.add(groupName);
				List<EFRowSet> rowListMap = new ArrayList<EFRowSet>();
				rowListMap.add(rs);
				map.put(groupName, rowListMap);
				//如果set中存在此类型
			}else{
					/*List<Map<String,String>> childListMap = map.get(groupName);
					Map<String,String> childMap = new HashMap<String,String>();
					for(int j=0;j<formatColList.size();j++){
					childMap.put(formatColList.get(j).getCaption(),rs.getString(formatColList.get(j).getDataSetColID(), ""));
					}
					childListMap.add(childMap);*/
				List<EFRowSet> rowListMap = map.get(groupName);
				rowListMap.add(rs);


			}
		}
		return map;
	}

	/**
	 * 得到新的数据 shouwenguanli
	 * @param dataSet
	 * @return
	 */
	public static Map<String ,List<EFRowSet>> openTaskFromEFDataSetToMapNew(EFDataSet dataSet,String type)  {
		Map<String ,List<EFRowSet>> map = new HashMap<String ,List<EFRowSet>>();
		/*set的作用主要是归类，根据Flow_Name归类*/
		Set<String> set=new HashSet<String>();
		if (dataSet.getRowCount()==0) {
			List<EFRowSet> rowListMap = new ArrayList<EFRowSet>();
			map.put(type, rowListMap);

			return map;
		}else {


			for(int i=0;i<dataSet.getRowCount();i++){
				EFRowSet rs = dataSet.getRowSet(i);

				//List<FormatCol> formatColList = ft.getFormatColList();
				//List<FormatRow> formatRowList = ft.getFormatRowList();
				//如果set中没有此类型
				if(!set.contains(type)){
					set.add(type);
					List<EFRowSet> rowListMap = new ArrayList<EFRowSet>();
					rowListMap.add(rs);
					map.put(type, rowListMap);
					//如果set中存在此类型
				}else{
					/*List<Map<String,String>> childListMap = map.get(groupName);
					Map<String,String> childMap = new HashMap<String,String>();
					for(int j=0;j<formatColList.size();j++){
					childMap.put(formatColList.get(j).getCaption(),rs.getString(formatColList.get(j).getDataSetColID(), ""));
					}
					childListMap.add(childMap);*/
					List<EFRowSet> rowListMap = map.get(type);
					rowListMap.add(rs);


				}
			}
			return map;}
	}
	/**
	 * 每个item中行的集合
	 * @author lch
	 */
	public static List<Map<String,String>> openDetaiFromRowsetToMap(ESPRowSet rowset,FormatTable ft){
		List<Map<String,String>> list = new ArrayList<Map<String,String>>();

		List<FormatRow> formatRowList = ft.getFormatRowList();
		for(int k=0;k<formatRowList.size();k++){
			FormatRow formatRow = formatRowList.get(k);
			List<FormatCol> formatColList = formatRow.getList();
			Map<String,String> childmap = new HashMap<String,String>();
			for(int j=0;j<formatColList.size();j++){

				childmap.put(formatColList.get(j).getCaption(),rowset.getObject(formatColList.get(j).getDataSetColID(), "")+"");
			}
			list.add(childmap);
		}

		return list;
	}

	/**
	 * 每个item中行的集合
	 * @author lch
	 */
	public static List<Map<String,String>> openDetaiFromRowsetToMap(ESPRowSet rowset,FormatTable ft,String prefix){
		List<Map<String,String>> list = new ArrayList<Map<String,String>>();

		List<FormatRow> formatRowList = ft.getFormatRowList();
		for(int k=0;k<formatRowList.size();k++){
			FormatRow formatRow = formatRowList.get(k);
			List<FormatCol> formatColList = formatRow.getList();
			Map<String,String> childmap = new HashMap<String,String>();
			for(int j=0;j<formatColList.size();j++){

				childmap.put(formatColList.get(j).getCaption(),rowset.getObject(prefix+"."+formatColList.get(j).getDataSetColID(), "")+"");
			}
			list.add(childmap);
		}

		return list;
	}
	public static String openTaskFromEFDataSetToString(EFDataSet dataSet,FormatTable ft)  {

		String taskString = "";
		Map<String ,List<EFRowSet>>  maplist = openTaskFromEFDataSetToMap(dataSet);
		for(String key : maplist.keySet()){
			List<EFRowSet> list = maplist.get(key);
			for(int i=0;i<list.size();i++){
				EFRowSet rowSet = list.get(i);
				List<Map<String,String>> listmap =	openDetaiFromRowsetToMap(rowSet,ft);
				for(int j=0;j<listmap.size();j++){
					Map<String,String> map = listmap.get(j);
					taskString += map.toString();
				}
			}
		}
		return taskString;

	}


/*	*//**
	 * 将EFData转成Map
	 * @author lch
	 * @param dataset 为getdatafromRo的数据
	 * @parm  ft 为从xml转成的格式
	 *//*
	public static Map<String ,List<List<Map<String,String>>>> openTaskFromEFDataSetToMap(EFDataSet dataSet,FormatTable ft)  {
		Map<String ,List<List<Map<String,String>>>> map = new HashMap<String ,List<List<Map<String,String>>>>();
		set的作用主要是归类，根据Flow_Name归类
		Set<String> set=new HashSet<String>();

		for(int i=0;i<dataSet.getRowCount();i++){
			EFRowSet rs = dataSet.getRowSet(i);
			String groupName = rs.getString("FLOW_NAME", "");

			//List<FormatCol> formatColList = ft.getFormatColList();
			List<FormatRow> formatRowList = ft.getFormatRowList();
			//如果set中没有此类型
			if(!set.contains(groupName)){
				set.add(groupName);
				List<List<Map<String,String>>> rowListMap = new ArrayList<List<Map<String,String>>>();
				List<Map<String, String>> childListMap = new ArrayList<Map<String,String>>();
			    for(int k=0;k<formatRowList.size();k++){
			    	FormatRow formatRow = formatRowList.get(k);
			    	List<FormatCol> formatColList = formatRow.getList();
					Map<String,String> childmap = new HashMap<String,String>();
				    for(int j=0;j<formatColList.size();j++){

						childmap.put(formatColList.get(j).getCaption(),rs.getObject(formatColList.get(j).getDataSetColID(), "")+"");
				    }
				    childListMap.add(childmap);

			    }
			    rowListMap.add(childListMap);
			    map.put(groupName, rowListMap);
			    //如果set中存在此类型
			}else{
					List<Map<String,String>> childListMap = map.get(groupName);
					Map<String,String> childMap = new HashMap<String,String>();
					for(int j=0;j<formatColList.size();j++){
					childMap.put(formatColList.get(j).getCaption(),rs.getString(formatColList.get(j).getDataSetColID(), ""));
					}
					childListMap.add(childMap);
				List<List<Map<String,String>>> rowListMap = map.get(groupName);
			    for(int k=0;k<formatRowList.size();k++){
			    	List<Map<String, String>> childListMap = new ArrayList<Map<String,String>>();
			    	FormatRow formatRow = formatRowList.get(k);
			    	List<FormatCol> formatColList = formatRow.getList();
					Map<String,String> childmap = new HashMap<String,String>();
				    for(int j=0;j<formatColList.size();j++){
						childmap.put(formatColList.get(j).getCaption(),rs.getString(formatColList.get(j).getDataSetColID(), ""));
						childListMap.add(childmap);
				    }
				    rowListMap.add(childListMap);
				List<Map<String, String>> childListMap = new ArrayList<Map<String,String>>();
			    for(int k=0;k<formatRowList.size();k++){
			    	FormatRow formatRow = formatRowList.get(k);
			    	List<FormatCol> formatColList = formatRow.getList();
					Map<String,String> childmap = new HashMap<String,String>();
				    for(int j=0;j<formatColList.size();j++){
						childmap.put(formatColList.get(j).getCaption(),rs.getObject(formatColList.get(j).getDataSetColID(), "")+"");
				    }
				    childListMap.add(childmap);

			    }
			    rowListMap.add(childListMap);

				}
		}
		return map;
	}*/

	/*public static String openTaskFromEFDataSetToString(EFDataSet dataSet,FormatTable ft)  {

		return openTaskFromEFDataSetToMap(dataSet).toString();

	}*/


}
