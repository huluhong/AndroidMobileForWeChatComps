package com.pansoft.espflow.util;

import com.efounder.eai.EAI;
import com.efounder.eai.data.JParamObject;
import com.efounder.eai.data.JResponseObject;

/**
 * 油田部使用 yqs
 */
public class FlowYTBDriverManager {
	
	/* 流程提交
	 * @param submitSuccessHandler 提交成功处理函数
	 * @param submitErrorHandler 提交处理失败处理函数
	 * @param PO 参数对象
	 * @param userObject
	 */
	public static void submitTask(JParamObject PO) throws Exception{
		PO.SetValueByParamName("serviceName", "FlowTaskService");
		PO.SetValueByParamName("serviceMethod", "submitTask");
		JResponseObject ro = EAI.DAL.SVR("YTBService", PO);
		//EAI.DAL.IOM("FlowTaskService", "submitTask", PO);
	}
	
	/**
	 * 流程取回

	 * @throws Exception 
	 */
	public static void retakeTask(JParamObject PO) throws Exception
	{
		PO.SetValueByParamName("serviceName", "FlowTaskService");
		PO.SetValueByParamName("serviceMethod", "retakeTask");
		JResponseObject ro = EAI.DAL.SVR("YTBService", PO);
		//EAI.DAL.IOM("FlowTaskService","retakeTask", PO);
	}
	
	/**
	 * 流程退回

	 * @throws Exception 
	 */
	public static void rollBackTask(JParamObject PO) throws Exception
	{
		PO.SetValueByParamName("serviceName", "FlowTaskService");
		PO.SetValueByParamName("serviceMethod", "rollBackTask");
		JResponseObject ro = EAI.DAL.SVR("YTBService", PO);
		//EAI.DAL.IOM("FlowTaskService", "rollBackTask", PO);
	}
	
	/**
	 * 流程重置
	 * zhtbin
	 * @throws Exception 
	 */
	public static void breakTask(JParamObject PO ) throws Exception
	{
		EAI.DAL.IOM("FlowTaskService", "breakTask", PO);//DALFlowTaskService.breakTask
	}
	
	/**
	 * 虚拟自动提交机器，显示虚拟提交流程图
	 * @param  PO
	 * @throws Exception 
	 */
	public static JResponseObject autoSubmitTaskMachine(JParamObject PO) throws Exception
	{
		PO.SetValueByParamName("serviceName", "FlowTaskService");
		PO.SetValueByParamName("serviceMethod", "autoSubmitTaskMachine");
		return EAI.DAL.SVR("YTBService", PO);
		//return EAI.DAL.IOM("FlowTaskService", "autoSubmitTaskMachine", PO);
	}
}
