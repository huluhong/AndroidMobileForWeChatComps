package com.pansoft.espflow.util;

import com.efounder.builder.base.data.EFRowSet;
import com.efounder.bz.flow.drive.NodeTaskDataSet;
import com.efounder.eai.EAI;
import com.efounder.eai.data.JParamObject;
import com.efounder.eai.data.JResponseObject;
import com.pansoft.espflow.IFlowDriveDataObject;
import com.pansoft.utils.CommonPo;

public class FlowRetakeUtil {
	
	public static String retakeTask(IFlowDriveDataObject flowDriveDataObject,
		JParamObject PO) throws Exception{
	    if(flowDriveDataObject == null) return null;
	    
	    NodeTaskDataSet nodeTaskDataSet = flowDriveDataObject.getNodeTaskDataSet();
	    if(nodeTaskDataSet == null || nodeTaskDataSet.getRowCount() == 0){
	        //[AlertUtil showAlertViewWithTitle:@"提示" withMessage:@"任务列表中不存在任务信息!" withButtons:nil];
	        return "任务列表中不存在任务信息!";
	    }
	    if(PO == null){
	    	//PO =JParamObject.Create();
	        PO = CommonPo.getPo();
	    } 
	    // 登录的责任中心
	    String unit_ID = (String)PO.getEnvValue("UNIT_ID", "");
	    EFRowSet rowSet = null;
	    // 自循时使用当前登录单位获取最后一个任务
	    if ( nodeTaskDataSet.isLoopNode()){
	    	rowSet = (EFRowSet)(nodeTaskDataSet.lastByToUnit(unit_ID));
	    }else{
	        rowSet=nodeTaskDataSet.getRowSet(nodeTaskDataSet.last());
	    }
	    
	    if (rowSet == null ) return null;
	    
	    // 如果最后一条的任务数据，处于处理完毕状态，则可以取回此条数据
	    if(!"processed".equals(rowSet.getString("RESR_STATUS", ""))){
	    	 //[AlertUtil showAlertViewWithTitle:@"提示" withMessage:@"任务还没有处理完毕，不允许取回!" withButtons:nil];
		     return "任务还没有处理完毕，不允许取回!";
	    }
	    // 出去的原因必须是 提交，才可以取回
	    if(!"submit".equals(rowSet.getString("RESR_OUT_CAUSE", ""))){
	    	//[AlertUtil showAlertViewWithTitle:@"提示" withMessage:@"只有提交的任务才可以取回!" withButtons:nil];
	        return "只有提交的任务才可以取回!";
	    }
	    EFRowSet lastTaskRowSet = nodeTaskDataSet.getRowSet(nodeTaskDataSet.last());
	    PO.setValue("OP_LEVEL", "00");
	    PO.setValue("FLOW_ID", flowDriveDataObject.getFLOW_ID());
	    PO.setValue("NODE_ID", flowDriveDataObject.getNODE_ID());
	    PO.setValue("BIZ_MDL", flowDriveDataObject.getMDL_ID());
	    PO.setValue("OBJ_GUID", flowDriveDataObject.getDataObjectGUID());
	    PO.setValue("MDL_ID", flowDriveDataObject.getMDL_ID());
	    PO.setValue("BIZ_UNIT", flowDriveDataObject.getDataObjectBIZ_UNIT());
	    PO.setValue("BIZ_DATE", flowDriveDataObject.getDataObjectBIZ_DATE());
	    PO.setValue("BIZ_DJBH", flowDriveDataObject.getDataObjectBIZ_DJBH());
	    PO.setValue("OP_ID", nodeTaskDataSet.getOP_ID());
	    PO.setValue("LOOP_ID", rowSet.getString("LOOP_ID", ""));
	    PO.setValue("TASK_UNIT", rowSet.getString("TASK_UNIT", ""));
	    
	    PO.setValue("UserCaption", lastTaskRowSet.getObject("OP_USER_NAME", ""));
	    //[PO setParamWithObject:[rowSet getStringForKey:@"TASK_UNIT" withDefaultValue:@""] forKey:@"TASK_UNIT"];
	    String pFlow_ID = rowSet.getString("PFLOW_ID", "");
	    if("".equals(pFlow_ID))
	        pFlow_ID = rowSet.getString("FROMFLOW_ID", "");
	    PO.setValue("PFLOW_ID", pFlow_ID);
	    PO.setValue("BIZ_LOGIN_UNIT", flowDriveDataObject.getDataObjectBIZ_UNIT());
//	    FlowDriverManager.retakeTask(PO); 
	    JResponseObject ro = EAI.DAL.IOM("FlowTaskService","retakeTask", PO);
	    if (ro != null) {
			System.out.println("retakeTask(RowSet) getErrorCode: " + ro.getErrorCode());
			System.out.println("retakeTask(RowSet) getErrorString: " + ro.getErrorString());
			if (ro.getErrorCode() == 0) {
				return "提交成功";
			}else {
				return "提交失败";
			}
		} else {
			System.out.println("retakeTask(RowSet) ro is null");
			return "提交失败";
		}
	 }
}
