package com.pansoft.espflow.util;

import com.efounder.builder.base.data.EFRowSet;
import com.efounder.bz.flow.drive.NodeTaskDataSet;
import com.efounder.eai.data.JParamObject;
import com.pansoft.espflow.FlowDriverManager;
import com.pansoft.espflow.IFlowDriveDataObject;
import com.pansoft.utils.CommonPo;

public class FlowResetUtil {
	
	public static void submitTask(IFlowDriveDataObject flowDriveDataObject, String taskType, 
			JParamObject PO) throws Exception{
	    if (flowDriveDataObject == null) return;
	    
	    NodeTaskDataSet nodeTaskDataSet = flowDriveDataObject.getNodeTaskDataSet();
	    if (nodeTaskDataSet == null || nodeTaskDataSet.getRowCount() == 0 )
	    {
	        //[AlertUtil showAlertViewWithTitle:@"提示" withMessage:@"任务不存在!" withButtons:nil];
	        return;
	    }
	    if (PO == null)
	    {
	        //PO = JParamObject.Create();
	    	PO = CommonPo.getPo();
	    }
	    EFRowSet lastTaskRowSet = nodeTaskDataSet.getRowSet(nodeTaskDataSet.last());
	    //把break type 放到第0个rowset里
	    EFRowSet nodeFirstRowSet = nodeTaskDataSet.getRowSet(0);
	    nodeFirstRowSet.putString("BREAK_TASK_TYPE", taskType);
	    PO.setObject("OP_USER", lastTaskRowSet.getObject("OP_USER", ""));
	    PO.setObject("OP_USER_NAME", lastTaskRowSet.getObject("OP_USER_NAME", ""));
	    PO.setObject("OP_LEVEL", "00");
	    PO.setObject("FLOW_ID", flowDriveDataObject.getFLOW_ID());
	    PO.setObject("NODE_ID", flowDriveDataObject.getNODE_ID());
	    PO.setObject("BIZ_MDL", flowDriveDataObject.getMDL_ID());
	    PO.setObject("MDL_ID", flowDriveDataObject.getMDL_ID());
	    PO.setObject("OBJ_GUID", flowDriveDataObject.getDataObjectGUID());
	    PO.setObject("BIZ_UNIT", flowDriveDataObject.getDataObjectBIZ_UNIT());
	    PO.setObject("BIZ_DATE", flowDriveDataObject.getDataObjectBIZ_DATE());
	    PO.setObject("BIZ_DJBH", flowDriveDataObject.getDataObjectBIZ_DJBH());
	    PO.setObject("BREAK_TASK_TYPE", nodeFirstRowSet.getObject("BREAK_TASK_TYPE", "0"));
	    PO.setObject("BIZ_LOGIN_UNIT", flowDriveDataObject.getDataObjectBIZ_UNIT());
	    FlowDriverManager.breakTask(PO);
	    
	}
}
