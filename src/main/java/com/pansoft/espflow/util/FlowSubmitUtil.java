
package com.pansoft.espflow.util;

import android.text.TextUtils;

import com.efounder.builder.base.data.EFRowSet;
import com.efounder.bz.flow.drive.NodeTaskDataSet;
import com.efounder.eai.EAI;
import com.efounder.eai.data.JParamObject;
import com.efounder.eai.data.JResponseObject;
import com.pansoft.espflow.IFlowDriveDataObject;

public class FlowSubmitUtil {

	public static String submitTask(IFlowDriveDataObject flowDriveDataObject, String edgeID,
			JParamObject po) throws Exception {
		if (flowDriveDataObject == null)
			return "任务列表为空！";

		NodeTaskDataSet nodeTaskDataSet = flowDriveDataObject.getNodeTaskDataSet();
		if (nodeTaskDataSet == null || nodeTaskDataSet.getRowCount() == 0) {
			return "任务列表中不存在任务信息！";
		}

		if (po == null) {
			po = JParamObject.Create();
		}

		EFRowSet lastTaskRowSet = nodeTaskDataSet.getRowSet(nodeTaskDataSet.last());
		if (lastTaskRowSet == null) {
			return "没有取到最后一条任务信息！";
		}

		// 资源来源原因（create/submit/retake/..）
		String incause = lastTaskRowSet.getString("RESR_IN_CAUSE", "");
		// 如果不是取回，两个节点间不能是同一人
		if (!"retake".equals(incause) && !"create".equals(incause)) {
			String submit_user = lastTaskRowSet.getString("OP_SUBMIT_NAME", "");
			String sys_user = (String) po.getEnvValue("UserCaption", "");
			if (submit_user.equals(sys_user)) {
				return "相邻节点不能处理自己提交的单据！";
			}
		}

		if (Boolean.parseBoolean((String) po.getValue("allowOthersSubmit", "true"))) {
			// 父流程ID
			String pFlow = lastTaskRowSet.getString("PFLOW_ID", "");
			// 开始节点只能提交自己的单据，被退回的单据除外
			if (!TextUtils.isEmpty(pFlow) && nodeTaskDataSet.isStartNode()
					&& !"rollback".equals(incause)) {
				String opUser = lastTaskRowSet.getString("OP_USER_NAME", "");
				String sysUser2 = (String) po.getEnvValue("UserCaption", "");
				if (!opUser.equals(sysUser2)) {
					return "开始节点只能提交自己的单据！";
				}
			}
		}

		String processMessage = po.GetValueByParamName("processMessage", "同意");
		po.setValue("OP_LEVEL", "00");
		po.setValue("OP_PROC_NOTE", processMessage);
		po.setValue("FLOW_ID", flowDriveDataObject.getFLOW_ID());
		po.setValue("NODE_ID", flowDriveDataObject.getNODE_ID());
		po.setValue("EDGE_ID", edgeID);
		po.setValue("OBJ_GUID", flowDriveDataObject.getDataObjectGUID());
		po.setValue("BIZ_MDL", flowDriveDataObject.getMDL_ID());
		po.setValue("MDL_ID", flowDriveDataObject.getMDL_ID());
		po.setValue("BIZ_UNIT", flowDriveDataObject.getDataObjectBIZ_UNIT());
		po.setValue("BIZ_DATE", flowDriveDataObject.getDataObjectBIZ_DATE());
		po.setValue("BIZ_DJBH", flowDriveDataObject.getDataObjectBIZ_DJBH());
		po.setValue("OP_ID", nodeTaskDataSet.getOP_ID());
		po.setValue("TASK_UNIT", lastTaskRowSet.getObject("TASK_UNIT", ""));
		po.setValue("BIZ_LOGIN_UNIT", flowDriveDataObject.getDataObjectBIZ_UNIT());
		po.setValue("UserCaption", lastTaskRowSet.getObject("OP_USER_NAME", ""));
		//po.setServiceURL("http://192.168.3.4:8088/EnterpriseServer/Android");
		
		
		/*po.setEnvValue("UserName", "P0034");
		po.setEnvValue("UserPass", "pghy123456");
		po.setEnvValue("LoginCheckType", "ADCHECK");// 胜利
		po.SetValueByParamName("get_mobile_resource", "1");
		po.SetValueByParamName("resource_key", "tablet_resource");
		po.setEnvValue("LoginCheckType", null);*/
		
		
		
		/*JResponseObject ro  = EAI.DAL.IOM("SecurityObject", "loginProduct", po, null,
				null);*/
		JResponseObject ro = EAI.DAL.IOM("FlowTaskService", "submitTask", po,null,null);
		if (ro != null) {
			System.out.println("submitTask(FormModel) getErrorCode: " + ro.getErrorCode());
			System.out.println("submitTask(FormModel) getErrorString: " + ro.getErrorString());
			if (ro.getErrorCode() == 0) {
				return "提交成功";
			}else {
				return "提交失败";
			}
		} else {
			System.out.println("submitTask(FormModel) ro is null");
			return "提交失败";
		}
//		return null;
	}

	public static String submitTask(EFRowSet flowTaskRowset, String edgeId, JParamObject po) {
		if (flowTaskRowset == null)
			return "所选单据为空！";

		if (po == null) {
			po = JParamObject.Create();
		}

		// 提交之前，是否需要先处理一下 add by lzk
		String previewSubmit = flowTaskRowset.getString("PREVIEW_SUBMIT", "0");
		if (previewSubmit.equals("1")) {
			return "请先处理一下，否则不允许提交！";
		}

		// 如果不是取回，两个节点间不能是同一人
		String incause = flowTaskRowset.getString("RESR_IN_CAUSE", "");
		//if (incause != "retake" && incause != "create") {
		if (!"retake".equals(incause) && "create".equals(incause)) {
			String submitUser = flowTaskRowset.getString("OP_SUBMIT_NAME", "");
			String sysUser = flowTaskRowset.getString("UserCaption", "");
			if (submitUser.equals(sysUser)) {
				final String djbh = flowTaskRowset.getString("BIZ_DJBH", "");
				return djbh + "相邻节点不能处理自己提交的单据！";
			}
		}

		// 父流程ID
		String parentFlow = flowTaskRowset.getString("PFLOW_ID", "").trim();
		// 开始节点只能提交自己的单据，被退回的单据除外
		if (TextUtils.isEmpty(parentFlow) && !incause.equals("rollback")) {
			// TODO && flowRowSet.startNode
			String opUser = flowTaskRowset.getString("OP_USER_NAME", "");
			String sysUser = (String) po.getEnvValue("UserCaption", "");
			if (!opUser.equals(sysUser)) {
				final String djbh = flowTaskRowset.getString("BIZ_DJBH", "");
				return djbh + "开始节点只能提交自己的单据！";
			}
		}

		String processMessage = po.GetValueByParamName("processMessage", "同意");
		po.setValue("OP_LEVEL", "00");
		po.setValue("OP_PROC_NOTE", processMessage);
		po.setValue("FLOW_ID", flowTaskRowset.getString("FLOW_ID", ""));
		po.setValue("NODE_ID", flowTaskRowset.getString("NODE_TAG", ""));
		po.setValue("EDGE_ID", edgeId);
		po.setValue("OBJ_GUID", flowTaskRowset.getString("OBJ_GUID", ""));
		po.setValue("BIZ_MDL", flowTaskRowset.getString("MDL_ID", ""));
		po.setValue("MDL_ID", flowTaskRowset.getString("MDL_ID", ""));
		po.setValue("BIZ_UNIT", flowTaskRowset.getString("BIZ_UNIT", ""));
		po.setValue("BIZ_DATE", flowTaskRowset.getString("BIZ_DATE", ""));
		po.setValue("BIZ_DJBH", flowTaskRowset.getString("BIZ_DJBH", ""));
		po.setValue("OP_ID", flowTaskRowset.getString("OP_ID", ""));
		po.setValue("TASK_UNIT", flowTaskRowset.getString("TASK_UNIT", ""));
		po.setValue("BIZ_LOGIN_UNIT", flowTaskRowset.getString("BIZ_UNIT", ""));

		// 处理提交函数
		try {
			JResponseObject ro = EAI.DAL.IOM("FlowTaskService", "submitTask", po);
			if (ro != null) {
				System.out.println("submitTask(FormModel) getErrorCode: " + ro.getErrorCode());
				System.out.println("submitTask(FormModel) getErrorString: " + ro.getErrorString());
				if (ro.getErrorCode() == 0) {
					return "提交成功";
				}else {
					return "提交失败";
				}
			} else {
				System.out.println("submitTask(FormModel) ro is null");
				return "提交失败";
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
}
