
package com.pansoft.espflow.util;

import com.efounder.builder.base.data.EFRowSet;
import com.efounder.bz.flow.drive.FlowConstants;
import com.efounder.bz.flow.drive.NodeTaskDataSet;
import com.efounder.eai.EAI;
import com.efounder.eai.data.JParamObject;
import com.efounder.eai.data.JResponseObject;
import com.pansoft.espflow.IFlowDriveDataObject;
import com.pansoft.utils.CommonPo;

/**
 * 油田部使用 yqs
 */
public class FlowYTBRollbackUtil {

	public static String rollbackTask(IFlowDriveDataObject flowDriveDataObject, JParamObject po)
			throws Exception {
		if (flowDriveDataObject == null)
			return "所选单据为空！";

		if (po == null) {
			//po = JParamObject.Create();
			po= CommonPo.getPo();
		}

		NodeTaskDataSet nodeTaskDataSet = flowDriveDataObject.getNodeTaskDataSet();
		if (nodeTaskDataSet == null || nodeTaskDataSet.getRowCount() == 0) {
			return "任务列表中不存在任务信息！";
		}
		// 自循环节点
		if (nodeTaskDataSet.isLoopNode())
			return "自循环单据不能退回！";

		// [nodeTaskDataSet last];
		EFRowSet lastTaskRowSet = nodeTaskDataSet.getRowSet(nodeTaskDataSet.last());
		if (nodeTaskDataSet.isStartNode()
				|| !"pending".equals(lastTaskRowSet.getString("RESR_STATUS", "")))
			return "当前状态的单据不能退回！";

		po.setValue("OP_LEVEL", "00");
		po.setValue("OP_PROC_NOTE", "取回");
		po.setValue("FLOW_ID", flowDriveDataObject.getFLOW_ID());
		po.setValue("NODE_ID", flowDriveDataObject.getNODE_ID());
		po.setValue("BIZ_MDL", flowDriveDataObject.getMDL_ID());
		po.setValue("OBJ_GUID", flowDriveDataObject.getDataObjectGUID());
		po.setValue("MDL_ID", flowDriveDataObject.getMDL_ID());
		po.setValue("BIZ_UNIT", flowDriveDataObject.getDataObjectBIZ_UNIT());
		po.setValue("BIZ_DATE", flowDriveDataObject.getDataObjectBIZ_DATE());
		po.setValue("BIZ_DJBH", flowDriveDataObject.getDataObjectBIZ_DJBH());
		po.setValue("OP_ID", nodeTaskDataSet.getOP_ID());
		po.setValue("LOOP_ID", lastTaskRowSet.getString("LOOP_ID", ""));
		
		po.setValue("UserCaption", lastTaskRowSet.getObject("OP_USER_NAME", ""));

		// 拆分流程，在合并节点退回时，需要将所有的任务都退回，所以需要设置POP_ID为空
		if (nodeTaskDataSet.isMergeNode()) {
			// [PO setParamWithObject:nil forKey:@"POP_ID"];
		} else {
			po.setValue("POP_ID", lastTaskRowSet.getString("POP_ID", ""));
		}
		po.setValue("TASK_UNIT", lastTaskRowSet.getString("TASK_UNIT", ""));
		String pFlow_ID = lastTaskRowSet.getString("PFLOW_ID", "");
		if ("".equals(pFlow_ID)) {
			pFlow_ID = lastTaskRowSet.getString("FROMFLOW_ID", "");
			;
		}
		po.setValue("PFLOW_ID", pFlow_ID);
		po.setValue("BIZ_LOGIN_UNIT", flowDriveDataObject.getDataObjectBIZ_UNIT());

		po.SetValueByParamName("serviceName", "FlowTaskService");
		po.SetValueByParamName("serviceMethod", "rollBackTask");
		JResponseObject ro = EAI.DAL.SVR("YTBService", po);
		//JResponseObject ro = EAI.DAL.IOM("FlowTaskService", "rollBackTask", po);
		if (ro != null) {
			System.out.println("rollBackTask(FormModel) getErrorCode: " + ro.getErrorCode());
			System.out.println("rollBackTask(FormModel) getErrorString: " + ro.getErrorString());
			if (ro.getErrorCode() == 0) {
				return "提交成功";
			}else {
				return "提交失败";
			}
		} else {
			System.out.println("rollBackTask(FormModel) ro is null");
			return "提交失败";
		}
	}

	public static String rollbackTask(EFRowSet flowTaskRowset, JParamObject po) throws Exception {
		if (flowTaskRowset == null)
			return "所选单据为空！";

		// 如果最后一条的任务数据，处于待处理状态，并且不是开始节点，则可以回退此条数据
		if (FlowConstants._RESR_NODE_STATUS_PENDING_ != flowTaskRowset.getString(
				FlowConstants._RESR_STATUS_COL_, null))
			return "当前状态单据不能退回！";

		if (po == null) {
			po = JParamObject.Create();
		}

		po.setValue("OP_LEVEL", "00");
		po.setValue("OP_PROC_NOTE", "取回");
		po.setValue("FLOW_ID", flowTaskRowset.getString("FLOW_ID", ""));
		po.setValue("NODE_ID", flowTaskRowset.getString("NODE_TAG", ""));
		po.setValue("OBJ_GUID", flowTaskRowset.getString("OBJ_GUID", ""));
		po.setValue("BIZ_MDL", flowTaskRowset.getString("MDL_ID", ""));
		po.setValue("MDL_ID", flowTaskRowset.getString("MDL_ID", ""));
		po.setValue("BIZ_UNIT", flowTaskRowset.getString("BIZ_UNIT", ""));
		po.setValue("BIZ_DATE", flowTaskRowset.getString("BIZ_DATE", ""));
		po.setValue("BIZ_DJBH", flowTaskRowset.getString("BIZ_DJBH", ""));
		po.setValue("LOOP_ID", flowTaskRowset.getString("LOOP_ID", ""));
		po.setValue("OP_ID", flowTaskRowset.getString("OP_ID", ""));
		po.setValue("POP_ID", flowTaskRowset.getString("POP_ID", ""));
		po.setValue("TASK_UNIT", flowTaskRowset.getString("TASK_UNIT", ""));
		po.setValue("BIZ_LOGIN_UNIT", flowTaskRowset.getString("TASK_UNIT", ""));
		po.setValue("PFLOW_ID",
				flowTaskRowset.getString("PFLOW_ID", flowTaskRowset.getString("FROMFLOW_ID", "")));

		po.SetValueByParamName("serviceName", "FlowTaskService");
		po.SetValueByParamName("serviceMethod", "rollBackTask");
		JResponseObject ro = EAI.DAL.SVR("YTBService", po);
		//JResponseObject ro = EAI.DAL.IOM("FlowTaskService", "rollBackTask", po);
		if (ro != null) {
			System.out.println("rollBackTask(RowSet) getErrorCode: " + ro.getErrorCode());
			System.out.println("rollBackTask(RowSet) getErrorString: " + ro.getErrorString());
			if (ro.getErrorCode() == 0) {
				return "提交成功";
			}else {
				return "提交失败";
			}
		} else {
			System.out.println("rollBackTask(RowSet) ro is null");
			return "提交失败";
		}
	}
}
