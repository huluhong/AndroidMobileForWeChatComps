package com.pansoft.form;

import com.efounder.builder.base.data.EFDataSet;
import com.pansoft.espmodel.interfaces.DataSetComponent;

public interface DMColComponent {
	/**
	 * 数据组件
	 */
	DataSetComponent getDataSetComponent();
	
	/**
	 * 
	 */
	String getDataSetID();
	void setDataSetID(String value);
	
	/**
	 * 数据集列ID
	 */
	String getDataSetColID();
	void setDataSetColID(String dataSetColID);
	
	/**
	 * 数据集的名称列ID，需要一并保存到数据库中，用于显示不存在外键字典的列名称，
	 * 存在外键字典则不取此列的名称
	 */
	String getDataSetNameColID();
	void setDataSetNameColID(String value);
	
	/**
	 * 内部数据集ID
	 */
	String getInternalDataSetID();
	void setInternalDataSetID(String dataSetID);
	
	/**
	 * 显示数据集ID
	 */
	void setViewDataSetID(String viewDataSetID);
	String getViewDataSetID();
	
	/**
	 * 显示数据集列ID
	 */
	void setViewDataSetColID(String viewDataSetColID);
	String getViewDataSetColID();
	
	/**
	 * 值数据集列ID，存储的列ID
	 */
	void setValueDataSetColID(String valueDataSetColID);
	String getValueDataSetColID();
	
	/**
	 * 使用内部数据集
	 */
	Boolean getIsUserInternalDataSetID();
	void setIsUserInternalDataSetID(Boolean v);
	
	/**
	 * 列标志，根据是否使用内部数据集返回列的标志
	 */
	String getIdentifier();
	
	/**
	 * 扩展属性
	 */
	Object getPropertyMap();
	void setPropertyMap(Object value);
	
	/**
	 * 获取扩展属性
	 */
	Object getProperty(String key, Object defVal);
	void setProperty(String key,  Object value);
	
	/**
	 * 小数的精度
	 */
	int getNumberPrecision();
	void seNumberPrecision(int value);
		
	/**
	 * 数值的格式
	 */
	String getNumberFormat();
	void setNumberFormat(String numberFormat);
	
	/**
	 * 列类型，C/I/N等
	 */
	String getColumnType();
	void setColumnType(String value);
	
	/**
	 * 编辑器类型
	 */
	String getEditorType();
	void setEditorType(String value);
	
	/**
	 * 数据格式
	 */
	String getDateFormat();
	void setDateFormat(String dateFormat);
	
	/**
	 * 公式
	 */
	String getFormulaOne();
	void setFormulaOne(String formulaOne);
	/**
	 * 编辑掩码
	 */
	String getEditMask();
	void setEditMask(String editMask);
	
	//
	String getFkeyID();
	//
	void setFkeyID(String fkey);
	//
	String getRlglID();
	//
	void setRlglID(String rlglID);
	
	/**
	 * 数据改变事件
	 */
	void dataChanged(EFDataSet ds);
}
