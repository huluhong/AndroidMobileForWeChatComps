package com.pansoft.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.GestureDetector;
import android.view.GestureDetector.SimpleOnGestureListener;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ScrollView;
/**
 *  HlistView滑动卡顿现象
 */
public class CustomScrollView extends ScrollView { 
	private static final String TAG = "CustomScrollView";
	
    private GestureDetector mGestureDetector;   
    View.OnTouchListener mGestureListener;   
    
    public CustomScrollView(Context context, AttributeSet attrs) {   
        super(context, attrs);   
        mGestureDetector = new GestureDetector(new YScrollDetector());   
        setFadingEdgeLength(0);   
    }   
    
    @Override  
    public boolean onInterceptTouchEvent(MotionEvent ev) { 
    	Log.i(TAG, "---onInterceptTouchEvent="+(super.onInterceptTouchEvent(ev) && mGestureDetector.onTouchEvent(ev))+",super.onInterceptTouchEvent(ev):"+super.onInterceptTouchEvent(ev)+",mGestureDetector.onTouchEvent(ev):"+mGestureDetector.onTouchEvent(ev));
        return super.onInterceptTouchEvent(ev) && mGestureDetector.onTouchEvent(ev);   
    }   
    
    // Return false if we're scrolling in the x direction     
    class YScrollDetector extends SimpleOnGestureListener {   
        @Override  
        public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {   
            if(Math.abs(distanceY) > Math.abs(distanceX)) {   
                return true;   
            }   
            return false;   
        }   
    }   
}  