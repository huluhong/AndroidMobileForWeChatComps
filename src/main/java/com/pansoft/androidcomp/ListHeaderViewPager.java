package com.pansoft.androidcomp;

import android.content.Context;
import androidx.viewpager.widget.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;

public class ListHeaderViewPager extends ViewPager {

	int mLastMotionY;
	int mLastMotionX;
	
	public ListHeaderViewPager(Context context) {
		super(context);
	}
	
	public ListHeaderViewPager(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	@Override
	public boolean dispatchTouchEvent(MotionEvent ev) {
		getParent().requestDisallowInterceptTouchEvent(true); //只需这句话，让父类不拦截触摸事件就可以了。
        return super.dispatchTouchEvent(ev);
	}
}