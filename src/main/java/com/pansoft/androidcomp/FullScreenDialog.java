package com.pansoft.androidcomp;

import com.pansoft.android.espbase.R;

import android.app.Dialog;
import android.content.Context;

/**
 * 全屏的透明对话框
 * 
 * @author zhangjg
 * @date Mar 13, 2014 10:41:00 AM
 */
public class FullScreenDialog extends Dialog {

	private FullScreenDialog(Context context, int theme) {
		super(context, theme);
		
		setContentView(R.layout.progressbar);
		setCancelable(true);
		setCanceledOnTouchOutside(false);
	}

	private FullScreenDialog(Context context) {
		super(context);
	}

	private FullScreenDialog(Context context, boolean cancelable,
			OnCancelListener cancelListener) {
		super(context, cancelable, cancelListener);
	}
	
	
	public static FullScreenDialog create(Context context){
		
		return new FullScreenDialog(context, R.style.Dialog_FullScreen);
		
	}
	
}
