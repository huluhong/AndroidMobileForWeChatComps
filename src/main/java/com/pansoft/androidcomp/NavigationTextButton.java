package com.pansoft.androidcomp;

import com.pansoft.android.espbase.R;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

/**
 * 用于导航的标签按钮, 可以设置字体颜色和按下时的字体颜色
 * 
 * @author zhangjg
 * @date Mar 12, 2014 11:59:54 AM
 */
public class NavigationTextButton extends TextView{

	private int pressedTextColor; 
	
	private int normalTextColor;
	
	public NavigationTextButton(Context context) {
		super(context);
	}
	
	public NavigationTextButton(Context context, AttributeSet attrs) {
		super(context, attrs);
		
		TypedArray types = context.obtainStyledAttributes(attrs, 
                R.styleable.NavigationTextButton); 
		
        final int count = types.getIndexCount(); 
        for (int i = 0; i < count; ++i) { 
            int attr = types.getIndex(i); 
            
            if(attr == R.styleable.NavigationTextButton_pressed_text_color){
            	pressedTextColor = types.getColor(attr, this.getCurrentTextColor());
            	continue;
            }
            
            if(attr == R.styleable.NavigationTextButton_normal_text_color){
            	normalTextColor = types.getColor(attr, this.getCurrentTextColor());
            	
            	continue;
            }
        } 
        types.recycle(); 
        
        this.setTextColor(normalTextColor);
        
	}

	
	@Override
	public boolean onTouchEvent(MotionEvent event) {
		
		super.onTouchEvent(event);
		
		int action = event.getAction();
		switch (action) {
		case MotionEvent.ACTION_DOWN:
			NavigationTextButton.this.setTextColor(pressedTextColor);
			break;
			
		case MotionEvent.ACTION_UP:
			NavigationTextButton.this.setTextColor(normalTextColor);
			break;

		default:
			break;
		}
		return true;
	}
}
