package com.pansoft.androidcomp;

import com.pansoft.android.espbase.R;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;

/***
 * 
 * @author huayang
 *
 */
public class ImageIndicator extends RelativeLayout{
	
	public static final int VISIBLE = 0;
	
	public static final int GONE = 1;
	
	private ImageView imageBg;
	private ImageView imageFg;

	public ImageIndicator(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		
	}

	public ImageIndicator(Context context, AttributeSet attrs) {
		super(context, attrs);
		
		LayoutInflater inflater=(LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		inflater.inflate(R.layout.image_indicator, this);
		imageBg=(ImageView) findViewById(R.id.image_bg);
		imageFg=(ImageView)findViewById(R.id.image_indicator);
		
	}

	public ImageIndicator(Context context) {
		super(context);
		
	}
	
	
	public void setBgImageResource(int resId){
		
		imageBg.setImageResource(resId);
	}
	
	
	public void setFgImageResource(int resId){
		
		imageFg.setImageResource(resId);
	}
	
	public void setIndicatorVisibility(int visibility){
		
		if(visibility == VISIBLE){
			imageFg.setVisibility(View.VISIBLE);
		}else if(visibility == GONE){
			imageFg.setVisibility(View.GONE);
		}
		
	}
	
}
