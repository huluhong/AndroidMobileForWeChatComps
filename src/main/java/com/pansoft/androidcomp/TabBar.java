package com.pansoft.androidcomp;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.core.xml.StubObject;
import com.efounder.util.AppBroadcastReceiver;
import com.pansoft.android.espbase.R;
import com.pansoft.resmanager.ResFileManager;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class TabBar extends LinearLayout {
	
	private  int onclickNumber = 0;/**全局的点击tabbar次数*/
	private static final String TAG = "TabBar";
	private static final String CAPTION="caption";
	private static final String SELECTEDICON="selectedIcon";
	private static final String NORMALICON="normalIcon";
	private final Context mContext;
	private int mCurrentTabIndex;
	private int mTabCount;
	private int mTabWidth;
	private OnCurrentTabChangedListener mTabChangedListener;
	private List<ImageView> imageList=new ArrayList<ImageView>();
	private List<TextView> textList=new ArrayList<TextView>();
	private List<Object> espMenuList;
	private List<Bitmap> bitmapList;
	private List<Bitmap> bitmap_pressList;
	public TabBar(Context context, ArrayList<Object> espMenuList) {
		super(context);
		// TODO Auto-generated constructor stub
		this.espMenuList=espMenuList;
		bitmapList=new ArrayList<Bitmap>();
		bitmap_pressList=new ArrayList<Bitmap>();
		this.mContext=context;
		setWillNotDraw(false); // 重要!!!
		mCurrentTabIndex = -1;
		mTabCount = 0;
		init();
	}
	public void init() {
		this.setOrientation(LinearLayout.VERTICAL);
		this.setPadding(0, 0, 0, 0);
		//读取sd卡上的图片
		for(int i=0; i<espMenuList.size(); i++){
			String normalIcon=((StubObject)espMenuList.get(i)).getObject(NORMALICON, "").toString();
			//zhangjg 2014 01 24
			normalIcon = ResFileManager.IMAGE_DIR + "/" + normalIcon;
//			normalIcon = ResFileManager.IMAGE_DIR  + normalIcon;
			File file = new File(normalIcon); 
            if (file.exists()) { 
            	Bitmap bm = BitmapFactory.decodeFile(normalIcon); 
            	bitmapList.add(bm);  
            }
            String selectdeIcon=((StubObject)espMenuList.get(i)).getObject(SELECTEDICON, "").toString();
            selectdeIcon = ResFileManager.IMAGE_DIR + "/" + selectdeIcon;
//            selectdeIcon = ResFileManager.IMAGE_DIR  + selectdeIcon;
            File file_press=new File(selectdeIcon);
            if (file_press.exists()) { 
            	Bitmap bm = BitmapFactory.decodeFile(selectdeIcon); 
            	bitmap_pressList.add(bm);  
            }
            
		}
		WindowManager wm = (WindowManager) mContext.getSystemService(Context.WINDOW_SERVICE);
		Display dp = wm.getDefaultDisplay();
		mTabWidth = dp.getWidth();
		mTabCount = espMenuList.size();
		if (mTabCount > 0) {
			mTabWidth = mTabWidth / mTabCount; // 计算每个tab的宽度
			mCurrentTabIndex = 0;
		}
		LayoutParams outParams = new LayoutParams(mTabWidth,
				LayoutParams.WRAP_CONTENT);
		outParams.gravity=Gravity.CENTER;
		View.OnClickListener clkListener = new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				doSomething(v.getContext());
				// TODO Auto-generated method stub
				int index = (Integer) v.getTag();
				Log.v("index", "index:" +index);
				if (index != mCurrentTabIndex) {
					if (mTabChangedListener != null)
						mTabChangedListener.onCurrentTabChanged(index);
					mCurrentTabIndex = index;
					//mTabBar.invalidate();
					setCurrentTabColor(index);
				}
			}
			

			private void doSomething(Context context) {
				// TODO Auto-generated method stub
				onclickNumber++;
				String msg = AppBroadcastReceiver.msg;
//				Toast.makeText(context, "您点击了"+onclickNumber+"次TabBar", Toast.LENGTH_SHORT).show();
				if(onclickNumber>=3){
					onclickNumber = 0;
					if(msg.equals("FALSE")){
//						Toast.makeText(context, "试用版，请申请授权", Toast.LENGTH_SHORT).show();
		    			AlertDialog.Builder builder= new Builder(context);
		    			builder.setMessage("试用版,请申请授权！");
		    			builder.setTitle("提示");
		    			builder.setNegativeButton(R.string.common_text_confirm, new DialogInterface.OnClickListener() {
		    				
		    				@Override
		    				public void onClick(DialogInterface arg0, int arg1) {
		    					
		    				}
		    			});
		    			builder.create().show();
					}
				}
			}
			
		};
		//添加一个线条
		ImageView line = new ImageView(mContext);
		line.setBackgroundColor(getResources().getColor(R.color.tabline));
		LayoutParams lineParams = new LayoutParams(LayoutParams.FILL_PARENT,
				1);
		this.addView(line, lineParams);
		LinearLayout tabcontent = new LinearLayout(mContext);
		tabcontent.setOrientation(LinearLayout.HORIZONTAL);
		LayoutParams tabcontentParams = new LayoutParams(LayoutParams.FILL_PARENT,
				LayoutParams.WRAP_CONTENT);
		tabcontent.setLayoutParams(tabcontentParams);
		this.addView(tabcontent);
		//逐个添加Tab
		for (int i = 0; i < mTabCount; ++i) {
			LinearLayout tab = new LinearLayout(mContext);
			tab.setGravity(Gravity.CENTER);
			tab.setOrientation(LinearLayout.VERTICAL);
			tab.setPadding(0, 5, 0, 0);
			tab.setTag(i); // 设置内部标号
			tab.setClickable(true);
			ImageView imv = new ImageView(mContext);
			imv.setScaleType(ScaleType.FIT_XY);
			imv.setImageBitmap(bitmapList.get(i));
			if(0 == i){
				imv.setImageBitmap(bitmap_pressList.get(0));
			}
			imageList.add(imv);
			TextView tv = new TextView(mContext);
			tv.setGravity(Gravity.CENTER_HORIZONTAL);
			String caption=((StubObject)espMenuList.get(i)).getObject(CAPTION, "").toString();
			tv.setText(caption);
			if(0 == i){
				tv.setTextColor(getResources().getColor(R.color.textcolor));
			}
			textList.add(tv);
			tab.addView(imv, 60, 60);//目前标签的宽和高设置为60*60，图片全部拉伸填充
			tab.addView(tv);
			tab.setOnClickListener(clkListener);
			tabcontent.addView(tab, outParams);
		}
	}
	/**
	 * 点击标签后更改所有的标签状态
	 * @param index
	 */
	public void setCurrentTabColor(int index){
		for(int i=0; i<mTabCount; i++){
			if(index == i){
				imageList.get(i).setImageBitmap(bitmap_pressList.get(i));
				textList.get(i).setTextColor(getResources().getColor(R.color.textcolor));
			}else{
				imageList.get(i).setImageBitmap(bitmapList.get(i));
				textList.get(i).setTextColor(Color.BLACK);
			}
		}
	}
	/**
	 * 设置当前Tab的序号
	 * @param index
	 */
	public void setCurrentTab(int index) {
		if (index > -1 && index < mTabCount&&index!=mCurrentTabIndex) {
			mCurrentTabIndex = index;
			this.invalidate();
			if (mTabChangedListener != null)
				mTabChangedListener.onCurrentTabChanged(mCurrentTabIndex);
		}
	}

	public void setOnCurrentTabChangedListener(
			OnCurrentTabChangedListener listener) {
		mTabChangedListener = listener;
	}
	/**
	 * 获取Tab个数
	 * 
	 * @return Tab个数
	 */
	public int getTabCount() {
		return mTabCount;
	}

	public interface OnCurrentTabChangedListener {
		public void onCurrentTabChanged(int index);
	}

}
