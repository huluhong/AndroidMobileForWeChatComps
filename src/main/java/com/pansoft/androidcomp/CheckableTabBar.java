package com.pansoft.androidcomp;


import com.pansoft.android.espbase.R;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.LinearLayout;

/**
 * 一个简单的TabBar实现
 * 注意:  tabbar中的子View(也就是各个菜单项组件) 不能再设置OnTouchListener, 否则会影响该控件的行为
 * 
 * @author zhangjg
 * @date Mar 12, 2014 4:51:24 PM
 */
public class CheckableTabBar extends LinearLayout {
	
	private int itemCount;			//菜单项的数目
	private Drawable checkedBg ;   //菜单项选中时的背景
	private Drawable unCheckedBg ; //菜单项未选中时的背景
	private int defCheckedItemIndex; //默认选中的菜单项的索引, 从0开始
	
	private int curentCheckedItemIndex = -1;   //当前选中的索引
	private int lastCheckedItemIndex = -1;     //上一次选中的索引
	
	private OnCheckedTabChangeListener onCheckedTabChangeListener;
	


	public CheckableTabBar(Context context) {
		super(context);
	}

	public CheckableTabBar(Context context, AttributeSet attrs) {
		super(context, attrs);
		
		TypedArray types = context.obtainStyledAttributes(attrs, 
                R.styleable.CheckableTabBar); 
		
        final int count = types.getIndexCount(); 
        for (int i = 0; i < count; ++i) { 
            int attr = types.getIndex(i); 
            
            switch (attr) {
			/*case R.styleable.CheckableTabBar_checked_item_bg:
				checkedBg = types.getDrawable(attr);
				break;
				
			case R.styleable.CheckableTabBar_uncheck_item_bg:
				unCheckedBg = types.getDrawable(attr);
				break;
				
			case R.styleable.CheckableTabBar_item_count:
				itemCount = types.getInteger(attr, 0);
				break;
				
			case R.styleable.CheckableTabBar_default_checked_item:
				defCheckedItemIndex = types.getInteger(attr, 0);
				break;*/

			default:
				break;
			}
        } 
        types.recycle();
        
		
	}
	

	/**
	 * 选中一个菜单项
	 * 
	 * @param itemIndex
	 */
	public void checkItem(int itemIndex){
		if(itemIndex < 0  ||  itemIndex > itemCount - 1){
			throw new RuntimeException("传入了错误的索引. itemIndex : " + itemIndex + ", itemCount : " + itemCount);
			
		}
		
		if(curentCheckedItemIndex != itemIndex){  //改变了相中项
			
			lastCheckedItemIndex = curentCheckedItemIndex;
			curentCheckedItemIndex = itemIndex;
			
			//改变当前项的背景
			getItemView(itemIndex).setBackgroundDrawable(checkedBg);
			
			//删除原来选中项的背景
			View lastCheckedItem = getItemView(lastCheckedItemIndex);
			
			if(lastCheckedItem != null){
				lastCheckedItem.setBackgroundDrawable(unCheckedBg);
			}
			
			//通知改变
			if(onCheckedTabChangeListener != null){
				
				View lastCheckedItemView = null;
				
				if(lastCheckedItemIndex != -1){
					lastCheckedItemView = getItemView(lastCheckedItemIndex);
				}
				
				onCheckedTabChangeListener.OnCheckedTabChanged(lastCheckedItemIndex, 
						curentCheckedItemIndex, 
						getItemView(itemIndex), 
						lastCheckedItemView);
			}
		}
		
		
	}
	
	/**
	 * 设置监听器
	 * 
	 * @param onCheckedTabChangeListener
	 */
	public void setOnCheckedTabChangeListener(
			OnCheckedTabChangeListener onCheckedTabChangeListener) {
		this.onCheckedTabChangeListener = onCheckedTabChangeListener;
	}
	
	
	@Override
	protected void onAttachedToWindow() {
		
		super.onAttachedToWindow();
		
		int childCount = getChildCount();
		for(int i = 0; i < childCount; i ++){
			View child = getChildAt(i);
			child.setOnTouchListener(new OnChildTouchListener(i));
		}
		
		//选中默认项
		checkItem(defCheckedItemIndex);
		
	}
	
	/**
	 * 内部监听器
	 * 
	 * @author zhangjg
	 * @date Mar 12, 2014 5:20:07 PM
	 */
	private class OnChildTouchListener implements View.OnTouchListener{

		private int  itemIndex ; 
		
		public OnChildTouchListener(int itemIndex){
			this.itemIndex = itemIndex;
		}
		
		
		@Override
		public boolean onTouch(View v, MotionEvent event) {
			
			if(event.getAction() == MotionEvent.ACTION_DOWN){  //当被按下
				
				checkItem(itemIndex);
				
			}
			return false;
		}
		
	}
	
	public View getItemView(int itemIndex){
		return getChildAt(itemIndex);
	}

	/**
	 * 对外的监听器
	 * 
	 * @author zhangjg
	 * @date Mar 12, 2014 5:17:40 PM
	 */
	public static interface OnCheckedTabChangeListener{
		
		
		public void OnCheckedTabChanged(int lastCheckedIndex, 
				int currentCheckedIndex, View checkedTabItem, View lastCheckedItem);
	}
	
}
