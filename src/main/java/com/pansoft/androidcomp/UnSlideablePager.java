package com.pansoft.androidcomp;

import android.content.Context;
import androidx.viewpager.widget.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;

public class UnSlideablePager extends ViewPager {

	public UnSlideablePager(Context context) {
		super(context);
		
	}

	public UnSlideablePager(Context context, AttributeSet attrs) {
		super(context, attrs);
		
	}
	
	
	@Override
	public boolean onInterceptTouchEvent(MotionEvent arg0) {
		return false;
	}
	
	@Override
	public boolean onTouchEvent(MotionEvent arg0) {
		//super.onTouchEvent(arg0);
		
		return true;
	}

}
