package com.pansoft.espaction;

import com.efounder.builder.base.data.EFRowSet;
import com.efounder.bz.flow.drive.NodeTaskDataSet;
import com.efounder.eai.data.JParamObject;
import com.efounder.eai.data.JResponseObject;
import com.efounder.form.EFFormDataModel;
import com.pansoft.espflow.FlowDriverManager;
import com.pansoft.espflow.IFlowDriveDataObject;
import com.pansoft.espflow.util.FlowSubmitUtil;
import com.pansoft.espmodel.FormModel;

public class FlowSubmitAction implements IAction {
	
	public IFlowDriveDataObject flowDriveDataObject;
	private String[] outEdgeIds;
	public JParamObject PO;
	private boolean allowOthersSubmit;
	private boolean showProMsgAtStart;
	private boolean showSuggest;
	private boolean isAssignReviewer;
	private boolean node2node;
	private String submitEdgeID;
	@Override
	public String doAction() {
		// TODO Auto-generated method stub
		if(flowDriveDataObject == null){
	        //[AlertUtil showAlertViewWithTitle:@"提示" withMessage:@"流程驱动对象设置不能为空!" withButtons:nil];
	        return null;
	    }
	    NodeTaskDataSet nodeTaskDataSet = flowDriveDataObject.getNodeTaskDataSet();
	    if(nodeTaskDataSet == null){
	        //[AlertUtil showAlertViewWithTitle:@"提示" withMessage:@"当前不存在任何任务，不允许提交!" withButtons:nil];
	        return null;
	    }
	    // 如果任务数为0，且当前处于开始节点则提示是否添加到任务流程上
//	    if ( [nodeTaskDataSet getRowCount] == 0 && nodeTaskDataSet.startNode )
//	    {
//	        //Alert  当前任务没有添加到流程上
//	        return;
//	    }
	    
	    // 如果只有一条边则默认提交到第一条边
	    outEdgeIds  = nodeTaskDataSet.getOutEdges();
	    if ( outEdgeIds == null || outEdgeIds.length == 0 )
	    {
	        //[AlertUtil showAlertViewWithTitle:@"提示" withMessage:@"当前节点没有设置要提交到的节点!" withButtons:nil];
	        return null;
	    }
	    EFRowSet billRowSet = null;
	    if(flowDriveDataObject instanceof FormModel){
	    	EFFormDataModel formDataModel = ((FormModel)flowDriveDataObject).getFormDataModel();
	        if(formDataModel != null){
	            billRowSet = formDataModel.getBillDataSet().getRowSet(0);
	        }
	    }
	    
	    if(PO == null)
	        this.PO = JParamObject.Create();
	    // 允许别人提交自己编制的单据
	    if (allowOthersSubmit){
	        PO.SetValueByParamName("allowOthersSubmit", "1");
	    }
		// 开始节点显示处理意见，默认显示
	    if (!showProMsgAtStart){
	        PO.SetValueByParamName("showProMsgAtStart", "0");
	    }
	    //在流程任务处理窗口中不显示处理意见
	    if (!showSuggest) {
	        PO.SetValueByParamName("showSuggest", "0");;
	    }
	    if(isAssignReviewer){
	        try {
				this.virtualSubmit(nodeTaskDataSet);
			} catch (Exception e) {
				e.printStackTrace();
			}
	    }else{
	        try {
				return this.nodeChek(outEdgeIds);
			} catch (Exception e) {
				e.printStackTrace();
			}
	    }
	    return null;
	}
	public JResponseObject virtualSubmit(NodeTaskDataSet nodeTaskDataSet) throws Exception{
	    if(nodeTaskDataSet == null){
	        //[AlertUtil showAlertViewWithTitle:@"提示" withMessage:@"获取参数失败，请尝试重新打开!" withButtons:nil];
	        return null;
	    }
	    EFRowSet taskRowSet  = nodeTaskDataSet.getRowSet(nodeTaskDataSet.getRowCount()-1);
	    if(taskRowSet == null){
	        //[AlertUtil showAlertViewWithTitle:@"提示" withMessage:@"获取参数失败，请尝试重新打开!" withButtons:nil];
	        return null;
	    }
	    
	    
	    PO.SetValueByParamName("FLOW_ID", taskRowSet.getString("FLOW_ID", ""));
	    PO.SetValueByParamName("NODE_ID", taskRowSet.getString("NODE_TAG", ""));
	    PO.SetValueByParamName("OBJ_GUID", taskRowSet.getString("OBJ_GUID", ""));
	    PO.SetValueByParamName("BIZ_MDL", taskRowSet.getString("MDL_ID", ""));
	    PO.SetValueByParamName("MDL_ID", taskRowSet.getString("MDL_ID", ""));
	    PO.SetValueByParamName("BIZ_UNIT", taskRowSet.getString("BIZ_UNIT", ""));
	    PO.SetValueByParamName("BIZ_DATE", taskRowSet.getString("BIZ_DATE", ""));
	    PO.SetValueByParamName("BIZ_DJBH", taskRowSet.getString("BIZ_DJBH", ""));
	    PO.SetValueByParamName("OP_ID", taskRowSet.getString("OP_ID", ""));
	    PO.SetValueByParamName("TASK_UNIT", taskRowSet.getString("TASK_UNIT", ""));
	    PO.SetValueByParamName("BIZ_LOGIN_UNIT", taskRowSet.getString("BIZ_UNIT", "") );
	    PO.SetValueByParamName("OP_USER_NAME", taskRowSet.getString("OP_USER_NAME", ""));
	    PO.SetValueByParamName("NODE_TAG_NAME", taskRowSet.getString("NODE_TAG_NAME", ""));
	    PO.SetValueByParamName("OP_LEVEL", "00");
	    PO.SetValueByParamName("OP_PROC_NOTE", "同意_虚拟提交");
	    return FlowDriverManager.autoSubmitTaskMachine(PO);
	}

	public String  nodeChek(String[] aOutEdgeIds) throws Exception{
	    // 仅有一条边
	    if (aOutEdgeIds.length == 1)
	    {
	        String edgeid = aOutEdgeIds[0];
	        // 流程提交到某条边
	        if(node2node){
	            // 点对点
	            //FlowSubmitUtil.submitN2NTask(flowDriveDataObject, edgeid, this, PO);
	        }else{
	            return FlowSubmitUtil.submitTask(flowDriveDataObject, edgeid, PO);
	        }
	    }
	    // 有多条边
	    else
	    {
	        // 如果设置了提交的边，则提交到指定的边
	        if (!"".equals(submitEdgeID))
	        {
	            if(node2node){
//	                FlowSubmitUtil.submitN2NTask(flowDriveDataObject, submitEdgeID, this, PO);
	            }else{
	            	return FlowSubmitUtil.submitTask(flowDriveDataObject, submitEdgeID, PO);
	            }
	        }
	        // 如果没有指定边，则提交一个空边，后台全是自动边的话就会根据条件自动处理
	        else
	        {
	            if(node2node){
	               // FlowSubmitUtil.submitN2NTask(flowDriveDataObject, null, this, PO);
	            }else{
	            	return FlowSubmitUtil.submitTask(flowDriveDataObject, null, PO);
	            }
	        }
	    }
	    
	    return null;
	}
	@Override
	public void Update() {
		// TODO Auto-generated method stub

	}

	public boolean isAllowOthersSubmit() {
		return allowOthersSubmit;
	}

	public void setAllowOthersSubmit(boolean allowOthersSubmit) {
		this.allowOthersSubmit = allowOthersSubmit;
	}

}
