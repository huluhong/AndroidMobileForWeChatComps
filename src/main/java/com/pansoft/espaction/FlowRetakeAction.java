package com.pansoft.espaction;

import com.pansoft.espflow.IFlowDriveDataObject;
import com.pansoft.espflow.util.FlowRetakeUtil;

public class FlowRetakeAction implements IAction{
	public IFlowDriveDataObject flowDriveDataObject;
	@Override
	public String doAction() {
		if(flowDriveDataObject == null){
	        //[AlertUtil showAlertViewWithTitle:@"提示" withMessage:@"流程驱动对象设置不能为空!" withButtons:nil];
	        return "流程驱动对象设置不能为空!";
	    }
		try {
			return FlowRetakeUtil.retakeTask(flowDriveDataObject, null);
		} catch (Exception e) {
			e.printStackTrace();
		}
	    //[FlowRetakeUtil retakeTaskWithFlowDriveDataObject:flowDriveDataObject andPO:nil];
		return null;
	}
	@Override
	public void Update() {
		// TODO Auto-generated method stub
		
	}
}
