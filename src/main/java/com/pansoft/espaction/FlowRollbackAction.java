package com.pansoft.espaction;

import com.pansoft.espflow.IFlowDriveDataObject;
import com.pansoft.espflow.util.FlowRollbackUtil;

public class FlowRollbackAction implements IAction {

	public  IFlowDriveDataObject flowDriveDataObject;
	@Override
	public String doAction() {
		// TODO Auto-generated method stub
		if(flowDriveDataObject == null){
	        //[AlertUtil showAlertViewWithTitle:@"提示" withMessage:@"流程驱动对象设置不能为空!" withButtons:nil];
	        return "流程驱动对象设置不能为空!" ;
	    }
	    try {
			return FlowRollbackUtil.rollbackTask(flowDriveDataObject, null);
		} catch (Exception e) {
			e.printStackTrace();
		}
	    return null;
	}

	@Override
	public void Update() {
		// TODO Auto-generated method stub

	}

}
