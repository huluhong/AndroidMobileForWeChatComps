package com.pansoft.appcontext;

import android.os.Environment;

import com.efounder.constant.EnvironmentVariable;

import java.io.File;

import static com.efounder.frame.utils.Constants.KEY_SETTING_APPID;

/**
 * 应用程序中的常量
 * 
 * @author zhangjg
 * @date Feb 25, 2014 1:55:19 PM
 */
public class AppConstant {
	private AppConstant(){}
	/**
	 * 外部存储的根目录, 一般是 /sdcard
	 */
	 static String sdDir = null; 
	static{
		
	       boolean sdCardExist = Environment.getExternalStorageState() 
	                           .equals(android.os.Environment.MEDIA_MOUNTED);   //判断sd卡是否存在 
	       if   (sdCardExist)   
	       {                               
	         sdDir = Environment.getExternalStorageDirectory().toString();//获取跟目录 
	      }else{
	    	  sdDir =  Environment.getRootDirectory().toString();
	      }
	}
	//public static final String EXTERNAL_ROOT = Environment.getExternalStorageDirectory().toString();
	public static final String EXTERNAL_ROOT = sdDir;
	
	/**
	 * 应用程序根目录 
	 */
    public static final String APP_ROOT = EXTERNAL_ROOT + "/"+EnvironmentVariable.getProperty(KEY_SETTING_APPID);
	
	static{
		File rootDir = new File(APP_ROOT);    
		if(!rootDir.exists()){
			rootDir.mkdirs();
		}
	}
}
