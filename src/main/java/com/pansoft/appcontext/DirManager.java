package com.pansoft.appcontext;

import java.io.File;

import android.os.Environment;

/**
 * 用于让特定的用户(一般是当前登录的用户)管理自己的目录结构
 * 
 * 应该在登陆成功之后检查并创建自己的用户目录
 * 调用方式: 
 * 		DirManager.getInstance("userId").createUserDir();
 * 
 * 注意: 该类只管理用户的根目录,也就是以自己的用户名命名的目录, 其他业务相关的
 * 子目录的管理任务,不由该类负责, 应该写在各自的业务模块中, 如云会议等
 * 
 * @author zhangjg
 * @date Feb 21, 2014 10:27:32 AM
 */
public class DirManager {
	
	//要管理哪个用户的目录结构
	private String userId;
	
	private String userDirName;
	
	private DirManager(String userId) {
		super();
		this.userId = userId;
		this.userDirName = AppConstant.APP_ROOT + "/" + userId;
	}

	/**
	 * 获取目录管理的实例
	 * @param userId
	 * @return
	 */
	public static DirManager getInstance(String userId){
		return new DirManager(userId);
	}
	
	/**
	 * 判断用户目录是否存在
	 * @return
	 */
	public boolean isUserDirExist(){
		return new File(userDirName).exists();
	}
	
	/**
	 * 创建用户目录
	 */
	public void createUserDir(){
		File userDir = new File(userDirName);
		
		if(!userDir.exists()){
			userDir.mkdirs();
		}
	}
	
	/**
	 * 获取用户Id
	 * @return
	 */
	public String getUserId(){
		return userId;
	}
	
	
	/**
	 * 获取当前用户的用户目录
	 * @return 如果用户目录还未创建, 则返回空
	 */
	public File getUserDir(){

		File userDir = new File(userDirName);
		return userDir.exists() ? userDir : null;
	}
	
	/**
	 * 获取用户目录的路径名
	 * @return
	 */
	public String getUserDirName(){
		return userDirName;
	}
}
