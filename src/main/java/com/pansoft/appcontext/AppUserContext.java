package com.pansoft.appcontext;

import java.util.HashMap;


/**
 * 保存当前登录的用户的信息
 * 这些信息在一个完整的登陆周期内有效
 * 
 * 在登陆成功后必须调用login方法或对应的setter方法保存用户信息
 * 
 * 在登陆之后的其他操作中, 如果创建了其他用户信息, 如创建了用户目录或用户特定的数据库, 也最好
 * 将这些信息保存在该上下文对象中
 * 
 * 在退出登录后,调用logout方法删除用户信息
 * 
 * 登陆成功后可能要做以下相关的操作
 * 
 *  //保存用户信息
 * 	AppUserContext.getInstance().login(userid, username, passWd);
 * 	
 *	//如果需要的话, 创建用户目录
 *	DirManager dirMag = DirManager.getInstance("userId");
 *	dirMag.createUserDir();
 *
 *	//最好将用户目录信息存入AppUserContext上下文对象
 *	AppUserContext.getInstance().setUserRoot(dirMag.getUserDirName());
 *
 *	
 * 
 * 
 * @author zhangjg
 * @date Feb 25, 2014 1:39:05 PM
 */
public class AppUserContext {
	
	//单例模式ceshi
	private  static final  AppUserContext instance = new AppUserContext();
	private AppUserContext(){}
	
	public static AppUserContext getInstance(){
		return instance;
	}
	
	
	private HashMap<String , Object> otherProperties = new HashMap<String, Object>();
	
	private String userId ;
	
	private String userName; 
	
	private String passWd;
	
	private String userRoot;
	
	private String userDbPath;
	
	
	
	
	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassWd() {
		return passWd;
	}

	public void setPassWd(String passWd) {
		this.passWd = passWd;
	}

	public String getUserRoot() {
		return userRoot;
	}

	public void setUserRoot(String userRoot) {
		this.userRoot = userRoot;
	}

	public String getUserDbPath() {
		return userDbPath;
	}

	public void setUserDbPath(String userDbPath) {
		this.userDbPath = userDbPath;
	}
	
	public void save (String key, Object value){
		otherProperties.put(key, value);
	}
	
	public void delete(String key){
		otherProperties.remove(key);
	}

	/**
	 * 登陆时保存信息
	 * @param userId
	 * @param userName
	 * @param passWd
	 */
	public void login(String userId, String userName, String passWd){
		
		this.userId = userId;
		
		this.userName = userName;
		
		this.passWd = passWd;
		
	}
	
	/**
	 * 登陆时保存信息
	 * @param userId
	 * @param userName
	 * @param passWd
	 * @param userRoot
	 * @param userDbPath
	 */
	public void login(String userId, String userName, String passWd, String userRoot, String userDbPath){
		login(userId, userName, passWd);
		
		this.userRoot = userRoot;
		
		this.userDbPath = userDbPath;
	}
	
	public void logout(){
		this.userId = null;
		
		this.userName = null;
		
		this.passWd = null;
		
		this.userRoot = null;
		
		this.userDbPath = null;
		
		otherProperties.clear();
	}
	
}
