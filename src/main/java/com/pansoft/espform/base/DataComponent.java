package com.pansoft.espform.base;

import java.util.ArrayList;
import java.util.List;

import com.efounder.builder.base.data.EFDataSet;

public class DataComponent {

	public DataComponent()
	{
		/*
		//组织唯一的Label
		String clazzName = getQualifiedClassName(this);
		clazzName = clazzName.substr(clazzName.indexOf("::") + 2);
		
		var curDate : Date = new Date();
		var idStr : String = curDate.time.toString();
		//生成唯一id
		id = clazzName + idStr;
		label = id;
		*/
	}
	
//	private String _id;
//
//	/**
//	 * 组件的唯一ID
//	 */
//	public String getId()
//	{
//		return _id;
//	}
//
//	/**
//	 * @private
//	 */
//	public void setId(String value)
//	{
//		_id = value;
//	}
//	
//	private String _label;
//
//	public String getLabel()
//	{
//		return _label;
//	}
//
//	public void setLabel(String value)
//	{
//		_label = value;
//	}
//	
//	private Object _icon = null;
//	/**
//	 * 组件的图标
//	 */
//	public Object getIcon()
//	{
//		var compIcon : Class = IconProvider.getComponentIcon(getQualifiedClassName(this));
//		if ( compIcon != null ) return compIcon;
//		return _icon;
//	}
//	public void setIcon(Object value)
//	{
//		_icon = value;
//	}
	
	//数据集ID
	private String dataSetID="";

	public String getDataSetID()
	{
		return dataSetID;
	}

	public void setDataSetID(String value)
	{
		dataSetID = value;
	}
	
	//下级数据集ID (childds01,childds02;childds0101,childds0102)
	private String nextDataSetID ="";

	public String getNextDataSetID()
	{
		return nextDataSetID;
	}

	public void setNextDataSetID(String value)
	{
		nextDataSetID = value;
	}

	
	private EFDataSet dataSet;

	public EFDataSet getDataSet()
	{
		return dataSet;
	}

	public void setDataSet(EFDataSet value)
	{
		dataSet = value;
	}

	private DataComponent parent = null;
	/**
	 * 
	 * 
	 **/
	public DataComponent getParent(){
		return parent;
	}
	/**
	 * 
	 * 
	 **/
	//private ArrayCollection  _chidren= new ArrayCollection();
	private List<DataComponent> chidren= new ArrayList<DataComponent>();
	/**
	 * 
	 * 
	 **/
	public List<DataComponent> getChidren(){
		return chidren;
	}
	
	/**
	 * @param dc
	 * @param index 插入位置
	 **/
	public void  insertChildAt(DataComponent dc,int index) 
	{
		if ( getChidren() == null ) chidren = new ArrayList<DataComponent>();
		if (index == -1) 
			chidren.add(dc);
		else
			chidren.add(index, dc);
		dc.parent = this;
	}
	
	/**
	 * 插入下一级
	 */
	public void  insertChild(DataComponent dc)
	{
		insertChildAt(dc, -1);
	}
	/**
	 * 
	 * 
	 **/
	public void removeChild(DataComponent dc) {
		if ( getChidren() == null ) return;
		chidren.remove(chidren.indexOf(dc));
	}
	/**
	 * 
	 */
	public void removeAllChild()
	{
		if ( getChidren() == null ) return;
		chidren.clear();
	}
	
	protected DataContainer dataContainer = null;
	/**
	 * 数据根容器，在任何数据组件中，都可以访问dataContainer
	 */
	public DataContainer getDataContainer()
	{
		return dataContainer;
	}
	public void setDataContainer(DataContainer value)
	{
		this.dataContainer = value;
	}
	
	//--------------------------------------------------------------------------
	//
	// IScriptObject methods
	//
	//--------------------------------------------------------------------------
//	public String prototype ;
//	private Object _scriptObject = null;
//	public Object getScriptObject() {
//		return _scriptObject;
//	}
//	public void setScriptObject(Object so) {
//		this._scriptObject = so;
//	}
//	
//	public String getEventScript(String key) {
//		if ( getScriptObject() == null ) return null;
//		return scriptObject[key] as String;
//	}
//	public void setEventScript( String key, String script) {
//		if ( scriptObject == null ) scriptObject = new Object();
//		scriptObject[key] = script;
//	}
//	
//	private Object _scriptContext = null;
//	public Object getScriptContext(){
//		return _scriptContext;
//	}
//	public void setScriptContext(Object obj){
//		_scriptContext = obj;
//	}
	
	//--------------------------------------------------------------------------
	//
	// IEventDispatcher methods
	//
	//--------------------------------------------------------------------------
	
//	protected EventDispatcher _listener  = new EventDispatcher();
//	
//	public void addEventListener(String type, listener:Function, 
//			Boolean useCapture , int priority, Boolean useWeakReference)
//	{
//		_listener.addEventListener(
//			type, listener, useCapture,
//			priority, useWeakReference);
//	}
//	
//	public Boolean dispatchEvent(Event event)
//	{
//		return _listener.dispatchEvent(event);
//	}
//	
//	public Boolean hasEventListener(String type)
//	{
//		return _listener.hasEventListener(type);
//	}
//	
//	public void removeEventListener(String type, listener:Function, 
//			Boolean useCapture)
//	{
//		_listener.removeEventListener(type, listener, useCapture);
//	}
//	
//	public Boolean willTrigger(String type)
//	{
//		return _listener.willTrigger(type);
//	}
	
	
//	/**
//	 * 重写toString
//	 */
//	public String toString()
//	{
//		return this.label;
//	}
	
	

}
