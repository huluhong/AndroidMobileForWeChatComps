package com.pansoft.espform.base;

import com.core.xml.StubObject;

public class DataContainer extends DataComponent {

	/**
	 * 打开菜单节点的内容上下文，存储节点的携带的参数和个性化参数
	 */
	protected StubObject nodeContext = null;

	/**
	 * 获取参数
	 * @param key String 键
	 * @param defValue Object 默认值
	 * @return Object
	 */
	public Object getValue(String key, Object defValue)
	{
		if ( nodeContext == null ) return defValue;
		// 先从nodeContext中取
		Object value = nodeContext.getObject(key, defValue);
		if ( value == null ) 
		{
			return defValue;
		}else{
			return value;
		}
		
	}

	/**
	 * 设置参数
	 * @param key String 键
	 * @param value Object 值
	 */
	public void setValue(String key, Object value)
	{
		if ( nodeContext == null )
		nodeContext = new StubObject();
		//nodeContext[key] = value;
		nodeContext.setObject(key, value);
	}
}
