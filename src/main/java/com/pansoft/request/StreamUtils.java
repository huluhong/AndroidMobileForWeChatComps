package com.pansoft.request;

import java.io.InputStream;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

public class StreamUtils {
	
	public static Bitmap inputStream2Bitmap(InputStream is){
		
		return BitmapFactory.decodeStream(is);
	}

}
