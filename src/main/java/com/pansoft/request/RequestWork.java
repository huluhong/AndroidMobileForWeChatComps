package com.pansoft.request;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * 具体的工作类
 * 
 * @author huayang
 *
 */
class RequestWork extends RequestData{

	private String[] params = null;
	private RequestListener listener ;
	int fileSize = 0;
	protected InputStream is = null;
	
	public RequestWork(RequestListener listener , Object params) {
		this.params = (String[]) params;
		this.listener = listener;
		
		if (params == null) 
			 return;
	}

	@Override
	protected Object doInBackground(Object... param) {
		
		URL url = null;
		
		String urlPath =  params[0];
		
		try {
			url = new URL(urlPath);
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setConnectTimeout(5 * 1000);
			conn.setRequestMethod("GET");
			conn.setRequestProperty("Connetion", "Keep-Alive");
			fileSize = conn.getContentLength();
			is = conn.getInputStream();
			
			
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return is ;
	}

	@Override
	protected void onPostExecute(Object result) {
		
		if (listener != null) {
			listener.onRequestOver(result, String.valueOf(fileSize));
		}
	}

	
}

