package com.pansoft.request;

import java.io.InputStream;


/**
 * 具体的实现类
 * 
 * 一个例子，下载一个地址上的图片
 * @author huayang
 * 
 */
public class RequestProcessConcrate extends RequestProcessInterfaceAdapter {

	@Override
	public void doRequest(final RequestListener listener,String flag, Object change ,final String... params) {
		
		new RequestWork(listener, params){

			@Override
			protected Object doInBackground(Object... param) {
				return StreamUtils.inputStream2Bitmap((InputStream)super.doInBackground(params[0]));
			}

			@Override
			protected void onPostExecute(Object result) {
				super.onPostExecute(result);
			}
			
			
			
		}.execute();
	}

}



