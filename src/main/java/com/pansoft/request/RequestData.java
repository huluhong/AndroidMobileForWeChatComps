package com.pansoft.request;

import android.os.AsyncTask;

/**
 * 通用的请求类，可继承，覆盖 实现特殊的要求，或执行其他后台任务。
 * 也可以不继承，实现Runnable接口等，完成后台任务。
 * 
 * @author huayang
 *
 */
public class RequestData  extends AsyncTask<Object, Object, Object> {
	

	@Override
	protected Object doInBackground(Object... params) {
		return null;
	}
	
	@Override
	protected void onPostExecute(Object result) {
		super.onPostExecute(result);
		
	}
	
}