package com.pansoft.request;

/**
 * 所有具体的实现类必须实现这个接口 或者 继承实现了这个接口的adapter类
 * @author huayang
 *
 */
public interface RequestProcessInterface{
	
	public class Methods{
		public static final String doRequest = "doRequest";
	}
	
	void doRequest(RequestListener listener ,String flag, Object  changeparams ,String... params);

}
