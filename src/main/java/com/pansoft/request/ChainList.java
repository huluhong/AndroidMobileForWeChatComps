package com.pansoft.request;

import java.util.ArrayList;

public class ChainList <T> extends ArrayList<T> {
	

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public ChainList<T> addObject(T object){
		add(object);
		return this;
	}
	

}
