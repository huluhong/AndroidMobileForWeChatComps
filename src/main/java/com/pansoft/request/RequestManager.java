package com.pansoft.request;

import java.lang.annotation.Annotation;
import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * 解析注解,执行方法
 * @author huayang
 *
 */
public class RequestManager{

	private static RequestManager instance = new RequestManager();

	public static RequestManager getInstance() {

		return instance;
	}

	public void insertObject(String fieldName,Class<?> target) {
		insertObject(fieldName,null, target);
	}
	

	public void insertObject(String fieldName,RequestListener listener ,Class<?> target) {

		Field[] fields = target.getDeclaredFields();

		if (fields == null || fields.length <= 0) {
			return;
		}

		for (Field field : fields) {

			if (field != null) {

				Annotation[] annos = field.getAnnotations();

				if (annos == null || annos.length <= 0) {
					continue;
				}
				if(fieldName != null && fieldName.equals(field.getName()))
					invokeAnnotation(listener, annos,null);
			}
		}
		
	}
	
	public void insertMethod(String methodName ,RequestListener listener ,Class<?> target,Object changableParams) {
		
		Method[] methods = target.getDeclaredMethods();
		
		if(methods == null || methods.length <= 0){
			return;
		}
		
		for(Method method :methods){
			
			if(method != null ){
				
				Annotation[] annos = method.getAnnotations();
				
				if (annos == null || annos.length <= 0) {
					continue;
				}
				
				if(methodName!= null && methodName.equals(method.getName()))
					invokeAnnotation(listener, annos, changableParams);

			}
		}
	}
	
	private void invokeAnnotation(RequestListener listener ,Annotation[] annos, Object changableParams){
		
		String[] params = null;
		for (Annotation anno : annos) {
			if (anno != null && anno instanceof Params) {

				Params p = (Params) anno;
				String className = p.className();

				params = p.params();
				int paramSize = params.length;
				
				Object arrayObj = Array.newInstance(String.class, paramSize);
				for(int i=0;i<paramSize;i++){
					Array.set(arrayObj, i, params[i]);
					System.out.println("this is " + params[i]);
				}
				
				String flag = p.flag();
				
				try {
					Class<?> c = Class.forName(className);
					if (params != null) {
						Method m = c.getMethod(p.method(),
								RequestListener.class,String.class,Object.class, String[].class);
						RequestProcessInterface concrate = (RequestProcessInterface) c.newInstance();
						
						if (m != null){
							m.invoke(concrate, listener,flag, changableParams, arrayObj);
							
							System.out.println("has invoked ... ");
						}
					}

				} catch (InvocationTargetException e) {
					System.out.println("错误 ： " + e.getTargetException());
				} catch (Exception e){
					e.printStackTrace();
				}
			}
		}
	}

	@Deprecated
	RequestListener defaultListener = new RequestListener() {

		@Override
		public void onRequestRunning(int totalSize, int downloadSize,
				Object message) {

		}

		@Override
		public void onRequestOver(Object target,String flag) {
		}
	};
}
