package com.pansoft.request;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import com.pansoft.request.RequestProcessInterface.Methods;

/**
 * 执行任务的 < 方法 | 成员  > 的注解，现主要用在方法上。
 * @author huayang
 *
 */

@Target(value = {ElementType.FIELD,ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface Params  {
	
	public String className();
	public String method() default Methods.doRequest;
	public String[] params() default {};
	public String requestListener() default "";
	public String flag() default "";
}
