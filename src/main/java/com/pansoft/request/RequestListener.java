package com.pansoft.request;

/**
 * 任务完成的回调接口
 * @author huayang
 *
 */
public interface RequestListener {
	
	/**
	 * 任务完成后的回调方法
	 * @param target 请求到的数据
	 * @param flag 这里可以是一个flag 用于多个任务的区分，当一个任务时，可以随意传些其他有用的数据。
	 */
	void onRequestOver(Object target,String flag);
	
	/**
	 * 任务执行中的回调方法
	 * 主要为下载时设计，当执行其他任务时，需要回传其他的数据结构，
	 * 可以封装在一个结构里，放在message参数里（object类型）。
	 * @param totalSize
	 * @param downloadSize
	 * @param message
	 */
	void onRequestRunning(int totalSize,int downloadSize,Object message);
}
