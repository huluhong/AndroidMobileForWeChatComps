package com.pansoft.view.uitableview;

public interface IListItem {
 
	public boolean isClickable();
	
	public void setClickable(boolean clickable);
	
}
