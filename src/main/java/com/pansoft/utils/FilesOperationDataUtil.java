package com.pansoft.utils;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.HashMap;

import android.content.Context;
import android.content.res.Resources;
import android.util.Log;

/*
 * @author zhenglaikun
 * 2014.11.19
 * 清空存储的文件内容
 * 初始化设置信息
 */
public class FilesOperationDataUtil {

	private static String port, service, address, path, name, sign;
	private static Boolean isSecurity;

	/*
	 * 删除应用程序存储的文件
	 */
	public static void clearFileData(Context context) {

		File file = new File(context.getFilesDir() + File.separator + "1.temp");
		if (!file.exists()) {
			Log.i("xxx", "文件为空");
			return;
		}
		try {
			if (deleteFile(file))
				Log.i("xxx", "文件已删除");
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

	}

	/*
	 * 删除目录下文件
	 */
	private static boolean deleteFile(File file) {

		if (file.isDirectory()) {
			String[] children = file.list();
			// 递归删除目录中的子目录下
			for (int i = 0; i < children.length; i++) {
				boolean success = deleteFile(new File(file, children[i]));
				if (!success) {
					return false;
				}
			}
		}
		// 目录此时为空，可以删除
		return file.delete();
	}
	/*
	 * 创建文件
	 */
	public static void createFile(Context context, Object object) {

		FileOutputStream fileOutputStream = null;
		// 创建自定义目录
		String dirPath = context.getFilesDir() + File.separator;
		// 给目录可读写权限 ，空格不能省
		String str = "chmod " + dirPath + " " + "777" + " && busybox chmod "
				+ dirPath + " " + "777";

		// 存到本地文件
		File localFile = new File(dirPath);

		localFile.mkdirs();

		File newFile = new File(dirPath + "1.temp");

		try {
			// 如果文件不存在
			if (!newFile.exists()) {
				
				Runtime.getRuntime().exec(str);

				newFile.createNewFile();
			}

			fileOutputStream = new FileOutputStream(newFile);
			// 定义对象流
			ObjectOutputStream objectOutputStream = new ObjectOutputStream(
					fileOutputStream);

			// 按顺序读写
			objectOutputStream.writeObject(object);

			Log.i("xxx", "写入成功"+newFile.getAbsolutePath());
			
			objectOutputStream.flush();
			fileOutputStream.close();
			objectOutputStream.close();

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

	/*
	 * 读文件
	 */
	public static ArrayList<HashMap<String, Object>> readFile(Context context) {
		ArrayList<HashMap<String, Object>> arrayList = new ArrayList<HashMap<String,Object>>();
		String dirPath = context.getFilesDir() + File.separator;
		File newFile = new File(dirPath + "1.temp");
		//返回值 object
        Object object = null;
		if (!newFile.exists()) {
			Log.i("xxx", "读取文件失败，文件已删除");
			return null;
		}
		try {
			// 读文件
			FileInputStream fileInputStream = new FileInputStream(newFile);

			// 反序列化对象
			ObjectInputStream in = new ObjectInputStream(fileInputStream);

			arrayList = (ArrayList<HashMap<String, Object>>) in.readObject();
			Log.i("xxx", "读取文件：" + arrayList.size());
			ArrayList<HashMap<String, Object>> list1 = (ArrayList<HashMap<String, Object>>) arrayList
					.get(0).get("responseList");
			
			// 强制把缓冲区的数据发出去，不必等到缓冲区满
			in.close();
			fileInputStream.close();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return arrayList;
	}
}
