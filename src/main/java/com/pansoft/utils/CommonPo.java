package com.pansoft.utils;

import android.content.Context;
import android.util.Log;

import com.efounder.constant.EnvironmentVariable;
import com.efounder.eai.EAI;
import com.efounder.eai.data.JParamObject;
import com.efounder.util.AppContext;
import com.efounder.util.StorageUtil;

public class CommonPo{
	private static JParamObject PO;
	private static StorageUtil storageUtil;
	/*
	 * 设置信息初始化
	 */
	private static String port, service, address, path, name, sign;
	private static Boolean isSecurity;
	private static String userName, passWord;
	public static void setPoToNull(){
		PO = null;
	}
	public static JParamObject getPo(){
		AppContext context = AppContext.getInstance();
		storageUtil = new StorageUtil(context, "storage");
		port = storageUtil.getString("port");
		address = storageUtil.getString("address");
		service = storageUtil.getString("service");
		name = storageUtil.getString("name");
		path = storageUtil.getString("path");
		sign = storageUtil.getString("sign");
		isSecurity = storageUtil.getBoolean("isSecurity", false);
		userName = EnvironmentVariable.getUserName();
		passWord = EnvironmentVariable.getPassword();
		
		if(PO==null){
			
		PO = JParamObject.Create();
		
	
		
		PO.setEnvValue("licenseID", "753B7C4C94467F1C31BC20CB3E913E6D08F81F676704C4F7");
		/*PO.setEnvValue("UserName","NM029");
        PO.setEnvValue("UserPass", "");*/
		PO.setEnvValue("ServerURL","http://"+address+":"+port+"/EnterpriseServer");
		PO.setEnvValue("productPath","http://"+address+":"+port+"/EnterpriseServer/Android");
	/*	PO.setEnvValue("ServerURL","http://10.75.131.128:8080/EnterpriseServer");
		PO.setEnvValue("productPath","http://10.75.131.128:8080/EnterpriseServer/Android");*/
		//PO.setEnvValue("ServerURL","http://zyesp.zyof.com.cn:8085/EnterpriseServer");
		//PO.setEnvValue("productPath","http://zyesp.zyof.com.cn:8085/EnterpriseServer/Android");
		PO.setEnvValue("DataBaseName", service); // 数据服务
		PO.setEnvValue("DBNO", sign);// 数据标示
		PO.setEnvValue("Password",passWord);
		PO.setEnvValue("Product", name);// 服务名称
		PO.setEnvValue("UserName", userName);
		
		if (isSecurity) {
			EAI.Protocol = "https";
		} else {
			EAI.Protocol = "http";
		}
		EAI.Server = address;
		EAI.Port = port;
		EAI.Path = path;
		EAI.Service = "Android";
	
		}
		return PO;
	}
}