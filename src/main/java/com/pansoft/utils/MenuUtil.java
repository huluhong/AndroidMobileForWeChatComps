package com.pansoft.utils;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;

import com.pansoft.resmanager.ResFileManager;
import com.pansoft.resmanager.ResFileManager.DownloadType;


/*
 * @auto:lch
 * date:12-30
 */
@Deprecated
  public class MenuUtil {
	  
	  private static final String KEY_INIT = "init";
	  private static final String KEY_UPDATE = "update";
	  private static final String KEY_PATH = "path";
	  private static final String KEY_VERSION = "version";
	  
	  private static final int MSG_INIT_SHOW = 0x0001;
	  private static final int MSG_UPDATE_SHOW = 0x0002;
	  private static final int MSG_INIT_CLOSE = 0x0004;
	  private static final int MSG_UPDATE_CLOSE = 0x0008;
	  private static final int MSG_OPEN = 0x000c;
	  

	

		/**
		 * 数据下载地址
		 */
	  private static String ServerURL;
	  private static Context context ;
	  private static menuResDownRollBack rollback;
	  
	  private static final int DOWN_UPDATE = 1;  
      
	  private static final int DOWN_OVER = 2;  
	  private static final int DOWN_EXCEPTION = 3;  
	  
	  private static final int ZIP_START = 4;  
	    private static final int ZIP_OVER = 5;  
	
	  
	  public MenuUtil(){
		 
	  }
	/*  *//**
	   * 获取meau 资源包
	   *//*
	  public static void getResourcesFromServer(Context context ,JResponseObject result,String serverURL){
		  MenuUtil.context = context;
		  MenuUtil.ServerURL =  serverURL;
		  context.getSharedPreferences("res_version", context.MODE_PRIVATE).edit().remove("res_version").commit();
		  if (VersionManager.isFirstToRequest(context)) {
				downloadData(result, KEY_INIT);
			} else {
				downloadData(result, KEY_UPDATE);
			}
	  }*/
	  
	  public static void downloadData(Context context,String serverURL, String initORupdate) {
		  MenuUtil.context = context;
		  MenuUtil.ServerURL =  serverURL;
		  ResFileManager.interceptFlag = false;
		/*    Map<?, ?> response = (Map<?, ?>) result.getResponseMap();
			Map<?, ?> resource = (Map<?, ?>) response.get(KEY_RESOURCE);
			StubObject so = (StubObject) resource.get(initORupdate);
			Hashtable<?, ?> ht = so.getStubTable();
			serverVersion = (String) ht.get(KEY_VERSION);
			String localVersion = VersionManager.getVersion(context);
			final String path = (String) ht.get(KEY_PATH);*/
			
		   
			ServerURL = serverURL;

			if (initORupdate.equals(KEY_INIT)) {

				sendMessage(MSG_INIT_SHOW);
 
				new Thread(new Runnable() {

					@Override
					public void run() {
						/*ResFileManager.downloadRes(ServerURL , DownloadType.INIT,
								downloadCallBack);*/
						ResFileManager.downloadRes(ServerURL , DownloadType.INIT,
								mHandler);
					}
				}).start();

			} else if (initORupdate.equals(KEY_UPDATE)) {

				sendMessage(MSG_UPDATE_SHOW);

			
			} else {

				sendMessage(MSG_OPEN);

			}

		}

		/***
		 * 下载完成后的回调函数
		 *//*
		static ResCallback downloadCallBack = new ResCallback() {

			@Override
			public void onSuccess(DownloadType type) {
				if (type.equals(DownloadType.INIT)) {

					VersionManager.setInitVersion(context);
					sendMessage(MSG_INIT_CLOSE);

				} else if (type.equals(DownloadType.UPDATE)) {

					//VersionManager.setVersion(context, serverVersion);
					sendMessage(MSG_UPDATE_CLOSE);

				}
			}

			@Override
			public void onFail() {
				
			}
		};*/
		

		@SuppressLint("HandlerLeak")
		static Handler startDownloading = new Handler() {
			public void handleMessage(Message msg) {

				switch (msg.arg1) {
					case MSG_INIT_SHOW:
					//	starAnActivity(context, Downloading.class);
						break;

					case MSG_UPDATE_SHOW:
					//	starAnActivity(context, Downloading.class);
						break;

					case MSG_INIT_CLOSE:
						rollback.successdown();
						//Downloading.thisActivity.finish();
						/*starAnActivity(context, MainActivity.class);
						((Activity)context).finish();*/
						break;

					case MSG_UPDATE_CLOSE:
						rollback.faildown();
						//Downloading.thisActivity.finish();
						/*starAnActivity(context, MainActivity.class);
						((Activity)context).finish();*/
						break;

					case MSG_OPEN:

						/*Bitmap bm = null;

						bm = BitmapFactory.decodeFile(ResFileManager.IMAGE_DIR + "/" + "zzt.png");

						ImageView i = ((ImageView) findViewById(R.id.image_test));
						i.setImageBitmap(bm);

						i.setBackgroundColor(Color.CYAN);*/
/*
						try {
							FileInputStream fis = new FileInputStream(ResFileManager.PACKAGE_DIR + "/"
									+ "Tab_Menu.xml");
							int len = 0;
							byte[] buffer = new byte[1024];

							while ((len = fis.read(buffer)) != -1) {

								Log.i("LEN :", "  " + len);

							}

							FileReader fr = new FileReader(new File(ResFileManager.PACKAGE_DIR + "/"
									+ "Tab_Menu.xml"));
							BufferedReader br = new BufferedReader(fr);
							String s = null;
							StringBuilder sb = new StringBuilder();

							while ((s = br.readLine()) != null) {
								sb.append(s);
							}
							String all = sb.toString();
							byte[] data = all.getBytes();

							System.out.println(sb.toString());
						} catch (Exception e) {
							e.printStackTrace();
						}*/
						//starAnActivity(context, MainActivity.class);
						break;
					default:
						break;
				}
			};
		};

		private static void sendMessage(int message) {
			Message msg = startDownloading.obtainMessage();
			msg.arg1 = message;
			startDownloading.sendMessage(msg);
		}
		
/*		private static  void starAnActivity(Context c, Class<?> clazz) {
			Intent in = new Intent(c, clazz);
			context.startActivity(in);
        }
*/		public static void setDownRollBack(menuResDownRollBack rollback1){
			rollback = rollback1;
		}
		/**
		 * 下载成功后的回调
		 */
       public interface menuResDownRollBack{
    	   public void successdown();
    	   public void setProgress(int i);
    	   public void faildown();
    	   public void zip_start();
    	   public void zip_over();
    	   
       }
       
       private static Handler mHandler = new Handler(){  
           public void handleMessage(Message msg) {  
               switch (msg.what) {  
               case DOWN_UPDATE:  
            	   Bundle b = msg.getData();
            	   int progress = b.getInt("progress");
            	   rollback.setProgress(progress);
            	/*   Log.i("xxxxxxxxx",".........."+progress);
                   mProgress.setProgress(progress); */ 
                   break;  
               case DOWN_OVER:  
            	/*   mProgress.setVisibility(View.GONE);*/
            	   rollback.successdown();
            	   break;
               case DOWN_EXCEPTION:  
            	 /*  mProgress.setVisibility(View.GONE);*/
            	   rollback.faildown();
                  // installApk();  
                   break;  
               case ZIP_OVER:
            	   rollback.zip_over();
            	   break;
               case ZIP_START:
            	   rollback.zip_start();
                   
               default:  
                   break;  
               }  
           }; 
       };
       
       public static void stopDown(){
    	   ResFileManager.interceptFlag = true;
       }

	
  }