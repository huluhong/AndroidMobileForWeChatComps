package com.pansoft.utils;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * 判断软件是否是第一次启动
 * 
 * @author zhangjg
 * @date 2014-4-15 上午10:56:09
 */
public class FirstUseManager {
	
	public static final String SP_NAME = "is_first_use";
	
	public static final String KEY = "is_first_use";
	
	public static FirstUseManager getInstance (Context context) {
		return new FirstUseManager(context);
	}
	
	private Context context;
	
	private FirstUseManager(Context context){
		this.context = context;
	}
	
	/**
	 * 软件第一次启动之后， 调用该方法
	 */
	public  void setFirstUseNo() {
		SharedPreferences sp = context.getSharedPreferences(SP_NAME, Context.MODE_PRIVATE);
		sp.edit().putBoolean(KEY, false).commit();
	}
	
	/**
	 * 判断是否是第一次使用
	 * @return
	 */
	public boolean isFirstUse(){
		SharedPreferences sp = context.getSharedPreferences(SP_NAME, Context.MODE_PRIVATE);
		return sp.getBoolean(KEY, true);
	}
	
}
