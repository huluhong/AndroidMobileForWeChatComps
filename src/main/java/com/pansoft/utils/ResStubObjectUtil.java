package com.pansoft.utils;

import com.core.xml.StubObject;
import com.efounder.service.Registry;

import java.util.List;

/**
 * 解析配置文件
 * @author will
 */
public class ResStubObjectUtil {

    private static final String KEY_MENU_ROOT = "menuRoot";
    private static Boolean hasBigMenu = null;

    /**
     * 是否有大图标
     * @return true 有
     */
    public static boolean getHasBigMenu() {

        if (hasBigMenu != null) {
            return hasBigMenu;
        }
        List<StubObject> mainMenus = Registry.getRegEntryList(KEY_MENU_ROOT);

        if (mainMenus == null) {
            return false;
        }

        for (StubObject stubObject : mainMenus) {
            if (stubObject.getString("topOut", "").equals("true")) {
                return true;
            }
        }
        return false;
    }
}
