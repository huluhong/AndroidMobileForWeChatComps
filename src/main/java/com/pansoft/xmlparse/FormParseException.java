package com.pansoft.xmlparse;

/**
 * 表单解析异常
 * @author zhangjg
 *
 */
public class FormParseException extends Exception{
	
	public FormParseException(String detials){
		super(detials);
	}
}
