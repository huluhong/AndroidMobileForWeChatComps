package com.pansoft.xmlparse;

import java.util.HashMap;
import java.util.Map;

public class FormatCol {
    /**
     * 显示名称的前缀
     */
    private String caption;
    /**
     * 显示的字段
     */
    private String dataSetColID;
    /**
     * 外键关联表
     */
    private String viewDataSetID;
    /**
     * 外键关联表的字段
     */
    private String viewDataSetColID;
    /**
     * 显示的格式
     */
    private String xsfs;

    private String textAlign;
    private String columnType;
    private String numberPrecision;
    private String textFormat;
    private String dateFormat;

    private String mask;
    //字段图标
    private String icon;
    //其他字段放入 map
    private Map<String, String> fieldMap = new HashMap<>();

    public Map<String, String> getFieldMap() {
        return fieldMap;
    }

    public FormatCol putField(String key, String value) {
        this.fieldMap.put(key, value);
        return this;
    }

    public String getField(String key, String defaultValue) {
        String value = this.fieldMap.get(key);
        if (value == null) {
            value = defaultValue;
        }
        return value;
    }

    public String getIcon() {
        return icon;
    }

    public FormatCol setIcon(String icon) {
        this.icon = icon;
        return this;
    }

    public String getTextAlign() {
        return textAlign;
    }

    public void setTextAlign(String textAlign) {
        this.textAlign = textAlign;
    }

    public String getColumnType() {
        return columnType;
    }

    public void setColumnType(String columnType) {
        this.columnType = columnType;
    }

    public String getNumberPrecision() {
        return numberPrecision;
    }

    public void setNumberPrecision(String numberPrecision) {
        this.numberPrecision = numberPrecision;
    }

    public String getTextFormat() {
        return textFormat;
    }

    public void setTextFormat(String textFormat) {
        this.textFormat = textFormat;
    }

    public String getDateFormat() {
        return dateFormat;
    }

    public void setDateFormat(String dateFormat) {
        this.dateFormat = dateFormat;
    }

    public String getCaption() {
        return caption;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }

    public String getDataSetColID() {
        return dataSetColID;
    }

    public void setDataSetColID(String dataSetColID) {
        this.dataSetColID = dataSetColID;
    }

    public String getViewDataSetID() {
        return viewDataSetID;
    }

    public void setViewDataSetID(String viewDataSetID) {
        this.viewDataSetID = viewDataSetID;
    }

    public String getViewDataSetColID() {
        return viewDataSetColID;
    }

    public void setViewDataSetColID(String viewDataSetColID) {
        this.viewDataSetColID = viewDataSetColID;
    }

    public String getXsfs() {
        return xsfs;
    }

    public void setXsfs(String xsfs) {
        this.xsfs = xsfs;
    }

    @Override
    public String toString() {
        return "FormatCol [caption=" + caption + ", dataSetColID="
                + dataSetColID + ", viewDataSetID=" + viewDataSetID
                + ", viewDataSetColID=" + viewDataSetColID + ", xsfs=" + xsfs
                + "]";
    }

    public String getMask() {
        return mask;
    }

    public void setMask(String mask) {
        this.mask = mask;
    }


}
