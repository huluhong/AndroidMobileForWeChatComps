package com.pansoft.xmlparse;

import java.util.HashMap;
import java.util.Map;

/**
 * 后台model和本地android端model的映射
 * @author yanxianhai
 *
 */
public class ModelMap {
private static final  Map<String, String> map = new HashMap<String, String>();
	
	
	static{
		//初始化map
		map.put("com.efounder.form.model::ModelContainer", 
				"com.pansoft.espmodel.ESPModel");
		map.put("com.efounder.action.mdm::MdmEditItemAction", 
				"com.pansoft.espmodel.ESPBasicButtoModel");
		map.put("com.efounder.model.column::ColumnModel", 
				"com.pansoft.espmodel.ESPColumnModel");
		map.put("com.efounder.model.column::Column", 
				"com.pansoft.espmodel.ESPColumn");
		map.put("com.efounder.model.column::ColumnGroup", 
				"com.pansoft.espmodel.ESPColumnGroup");
		map.put("com.efounder.form.model::ModelContainer", 
				"com.pansoft.espmodel.ESPModelContainer");
		
	}
	
	public static String getMappedModelName(String mappingName){
		return map.get(mappingName);
		
	}
}
