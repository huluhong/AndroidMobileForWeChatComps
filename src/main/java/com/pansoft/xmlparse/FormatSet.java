package com.pansoft.xmlparse;

import java.util.ArrayList;
import java.util.List;

public class FormatSet {
    /**
     * 应该保持package标签的id，目前还没保存
     */
    private String id;
    /**
     * 应该保持package标签的caption，目前还没保存
     */
    private String caption;
    private List<FormatService> serviceList = new ArrayList<>();
    private List<FormatTable> tableList = new ArrayList<>();
    private String textAlign;
    private String columnType;
    private String numberPrecision;

    private String mask;

    public String getTextAlign() {
        return textAlign;
    }

    public void setTextAlign(String textAlign) {
        this.textAlign = textAlign;
    }

    public String getColumnType() {
        return columnType;
    }

    public void setColumnType(String columnType) {
        this.columnType = columnType;
    }

    public String getNumberPrecision() {
        return numberPrecision;
    }

    public void setNumberPrecision(String numberPrecision) {
        this.numberPrecision = numberPrecision;
    }

    public String getTextFormat() {
        return textFormat;
    }

    public void setTextFormat(String textFormat) {
        this.textFormat = textFormat;
    }

    public String getDateFormat() {
        return dateFormat;
    }

    public void setDateFormat(String dateFormat) {
        this.dateFormat = dateFormat;
    }

    private String textFormat;
    private String dateFormat;

    public List<FormatTableTab> getTableTabList() {
        return tableTabList;
    }

    public void setTableTabList(List<FormatTableTab> tableTabList) {
        this.tableTabList = tableTabList;
    }

    private List<FormatTableTab> tableTabList = new ArrayList<>();

    public FormatService getFormatServiceById(String formatServiceId) {
        if (serviceList == null) return null;
        else {
            for (FormatService formatService : serviceList) {
                if (formatServiceId.equals(formatService.getId())) {
                    return formatService;
                }
            }
        }
        return null;
    }

    public FormatTable getFormatTableById(String formatTableId) {
        if (tableList == null) return null;
        else {
            for (FormatTable formatTable : tableList) {
                if (formatTableId.equals(formatTable.getId())) {
                    return formatTable;
                }
            }
        }
        return null;
    }

    public FormatTableTab getFormatTableTabById(String formatTableId) {
        if (tableTabList == null) return null;
        else {
            for (FormatTableTab formatTable : tableTabList) {
                if (formatTable.getId().equals(formatTableId)) {
                    return formatTable;
                }
            }
            return null;
        }

    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCaption() {
        return caption;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }

    public List<FormatService> getServiceList() {
        return serviceList;
    }

    public void setServiceList(List<FormatService> serviceList) {
        this.serviceList = serviceList;
    }

    public List<FormatTable> getTableList() {
        return tableList;
    }

    public void setTableList(List<FormatTable> tableList) {
        this.tableList = tableList;
    }

    @Override
    public String toString() {
        return "FormatSet [id=" + id + ", caption=" + caption
                + ", serviceList=" + serviceList + ", tableList=" + tableList
                + "]";
    }

    public String getMask() {
        return mask;
    }

    public void setMask(String mask) {
        this.mask = mask;
    }

}
