package com.pansoft.xmlparse;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class BillsInfo {
	
	public static final String ID = "id";
	public static final String CAPTION = "caption";
	public static final String DATASETCOLID = "dataSetColID";
	public static final String TABLE = "table";
	public static final String FOREIGNKEY = "foreignkey";
	public static final String MDL_ID = "MDL_ID";
	public static final String KEY_COL = "keyCol";
	
	private HashMap<String, BillType> billTypeMap = new HashMap<String, BillType>();//保存BillType的集合
	
	private HashMap<String, String> attr = new HashMap<String, String>();
	private BillType billType = new BillType();
	
	
	public HashMap<String, String> getAttr() {
		return attr;
	}
	public void setAttr(HashMap<String, String> attr) {
		this.attr = attr;
	}
	public BillType getBillType() {
		return billType;
	}
	public void setBillType(BillType billType) {
		this.billType = billType;
	}
	public void clearBillType(){
		this.billType = new BillType();
	}
	
	// ----
	
//	public String/*[] */getHeaderShowItem(){
//		return billType.getHeader().getAttr().get(KEY_COL)/*.split(";")*/;
//	}
	public String/*[] */getHeaderShowItem(String id){
		BillType btype = this.getBillTypeMap().get(id);
		return btype.getHeader().getAttr().get(KEY_COL)/*.split(";")*/;
	}
	
	public String getHeaderId(){
		return billType.getHeader().getAttr().get(ID);
	}
	public String getBillTypeHeaderID(String id){
		BillType btype = this.getBillTypeMap().get(id);
		return btype.getHeader().getAttr().get(ID);
	}
//	public String getHeaderTable(){
//		return billType.getHeader().getAttr().get(TABLE);
//	}
	public String getHeaderTable(String id){
		BillType btype = this.getBillTypeMap().get(id);
		return btype.getHeader().getAttr().get(TABLE);
	}
	
	public String getModelId(){
		return billType.getAttr().get(MDL_ID);
	}
	
	public String getMdId(String id){

		return this.getBillTypeMap().get(id).getAttr().get(MDL_ID);
	}
	
	public String getNodeTableName(String id){
		BillType btype = this.billTypeMap.get(id);
//		return billType.getNode().getAttr().get(TABLE);
		return btype.getNode().getAttr().get(TABLE);
		
	}
	
	public String getHeaderTableName(){
		return billType.getHeader().getAttr().get(TABLE);
	}
	
	public Map<String,String> getBillTypeAttr(){
		
		return billType.getAttr();
	}
	
	public String getBillTypeAttrCaption(){
		
		return (String) getBillTypeAttr().get(CAPTION);
	}
	
	public List<HashMap<String, String>> getNodeList(String id){
		BillType btype = this.billTypeMap.get(id);
//		return billType.getNode().getValues();
		return btype.getNode().getValues();
	}

	public List<HashMap<String, String>> getHeaderList(String id){
		BillType btpe = this.getBillTypeMap().get(id);
		return btpe.getHeader().getValues();
	}
//	public List<HashMap<String, String>> getHeaderList(){
//		return billType.getHeader().getValues();
//	}

	public HashMap<String, BillType> getBillTypeMap() {
		return billTypeMap;
	}
	public void setBillTypeMap(HashMap<String, BillType> billTypeMap) {
		this.billTypeMap = billTypeMap;
	}
	
	// ----
}

class BillType{
	
	private HashMap<String, String> attr = new HashMap<String, String>();
	
	private Node node = new Node();
	
	private String billTypeId;
	
	private Header header = new Header();

	public HashMap<String, String> getAttr() {
		return attr;
	}

	public void setAttr(HashMap<String, String> attr) {
		this.attr = attr;
	}

	public Node getNode() {
		return node;
	}

	public void setNode(Node node) {
		this.node = node;
	}

	public List<HashMap<String, String>> getNodeList(){
		
		return this.node.getValues();
	}
	public Header getHeader() {
		return header;
	}

	public void setHeader(Header header) {
		this.header = header;
	}

	public String getBillTypeId() {
		return this.attr.get("id");
	}

	public void setBillTypeId(String billTypeId) {
		this.billTypeId = billTypeId;
	}
	
	
	
}

class Node{
	
	private HashMap<String, String> attr = new HashMap<String, String>();
	private List<HashMap<String, String>> values = new ArrayList<HashMap<String,String>>();
	
	
	public HashMap<String, String> getAttr() {
		return attr;
	}
	public void setAttr(HashMap<String, String> attr) {
		this.attr = attr;
	}
	public List<HashMap<String, String>> getValues() {
		return values;
	}
	public void setValues(List<HashMap<String, String>> values) {
		this.values = values;
	}
	
}

class Header{
	
	private HashMap<String, String> attr = new HashMap<String, String>();
	private List<HashMap<String, String>> values = new ArrayList<HashMap<String,String>>();
	
	
	public HashMap<String, String> getAttr() {
		return attr;
	}
	public void setAttr(HashMap<String, String> attr) {
		this.attr = attr;
	}
	public List<HashMap<String, String>> getValues() {
		return values;
	}
	public void setValues(List<HashMap<String, String>> values) {
		this.values = values;
	}
	
}

