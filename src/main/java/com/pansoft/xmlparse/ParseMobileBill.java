package com.pansoft.xmlparse;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashMap;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import android.content.Context;

/**
 * 
 * @author huayang
 *
 */
public class ParseMobileBill {
	
	private String path ; 
//	private static ParseMobileBill instance = new ParseMobileBill("/mnt/sdcard/ESPMobile/res/unzip_res/Package/Mobile_BILL.xml");
//	
//	public static ParseMobileBill getInstance (){
//		return instance;
//	}
	
	private String pkg = "package";
	//private String mobile_service = "mobile_service";
	//private String mobile_bill = "mobile_bill";
	private String BILL_TYPE = "BILL_TYPE";
	private String node = "node";
	//private String Row = "Row";
	private String Col = "Col";
	private String Header = "Header";
	
	private boolean isNode = false;
	private boolean isHeader = false ;
	
    
    public ParseMobileBill(String path) {
		this.path = /*path*/"/mnt/sdcard/ESPMobile/res/unzip_res/Package/Mobile_BILL.xml";
	}
    
    public ParseMobileBill() {
	}

	public BillsInfo parseXML(){
		
		BillsInfo bi = null;
        
        try {
            XmlPullParserFactory pullParserFactory=XmlPullParserFactory.newInstance();
            XmlPullParser xmlPullParser=pullParserFactory.newPullParser();
            try {
				xmlPullParser.setInput(new FileInputStream(path), "UTF-8");
			} catch (IOException e1) {
				e1.printStackTrace();
			}
            
            int eventType=xmlPullParser.getEventType();
            
            try {
                while(eventType!=XmlPullParser.END_DOCUMENT){
                    String nodeName=xmlPullParser.getName();
                    switch (eventType) {
                    //文档开始
                    case XmlPullParser.START_DOCUMENT:
                        bi = new BillsInfo();
                        break;
                    //开始节点
                    case XmlPullParser.START_TAG:
                    	
                    	if(pkg.equals(nodeName)){
                    		
                    		int ac = xmlPullParser.getAttributeCount();
                            
                            for(int i=0;i<ac;i++){
                            	bi.getAttr().put(xmlPullParser.getAttributeName(i), xmlPullParser.getAttributeValue(i));
                            }
                    		
                    	}
                        //判断如果其实节点为student
                        if(BILL_TYPE.equals(nodeName)){
                          
                        	for(int i=0;i<xmlPullParser.getAttributeCount();i++){
                        		bi.getBillType().getAttr().put(xmlPullParser.getAttributeName(i), xmlPullParser.getAttributeValue(i));
                        	}
                        	
                        }else if(node.equals(nodeName)){
                        	
                        	isNode = true;
                        	
                        	for(int i=0;i<xmlPullParser.getAttributeCount();i++){
                        		bi.getBillType().getNode().getAttr().put(xmlPullParser.getAttributeName(i),xmlPullParser.getAttributeValue(i));
                        	}
                        	
                        }else if(Col.equals(nodeName)){
                        	
                        	HashMap<String, String> col = new HashMap<String, String>();
                        	
                        	for(int i=0;i<xmlPullParser.getAttributeCount();i++){
	                        	col.put(xmlPullParser.getAttributeName(i), xmlPullParser.getAttributeValue(i));
	                        }
	                        	
                        	if(isNode){
	                        	bi.getBillType().getNode().getValues().add(col);
                        	}
                        	
                        	if(isHeader){
	                        	bi.getBillType().getHeader().getValues().add(col);
                        	}
                        	
                        }else if(Header.equals(nodeName)){
                        	
                        	isHeader = true ;
                        	
                        	for(int i=0;i<xmlPullParser.getAttributeCount();i++){
                        		bi.getBillType().getHeader().getAttr().put(xmlPullParser.getAttributeName(i), xmlPullParser.getAttributeValue(i));
                        	}
                        	
                        }
                        
                        break;
                        
                    //结束节点
                    case XmlPullParser.END_TAG:
                     
                    	if(BILL_TYPE.equals(nodeName)){
                    		BillType bt = bi.getBillType();
                    		String btId = bi.getBillType().getBillTypeId();
                    		bi.getBillTypeMap().put(btId, bt);
                    		bi.clearBillType();
                    	}
                    	if(node.equals(nodeName)){
                        	
                        	isNode = false;
                    	}
                    	
                    	if(Header.equals(nodeName)){
                    		
                    		isNode = false;
                    	}
                    	
                        break;
                    default:
                        break;
                    }
                    eventType=xmlPullParser.next();
                }
            } catch (NumberFormatException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (XmlPullParserException e) {
            e.printStackTrace();
        }

        return bi;
    }
    
}