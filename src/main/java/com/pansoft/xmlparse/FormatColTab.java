package com.pansoft.xmlparse;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by zhangshunyun on 2017/8/29.
 */
@Deprecated
public class FormatColTab {
    private String tabName;
    private String icon;
    private String selectIcon;
    //其他字段放入 map
    private Map<String, String> fieldMap = new HashMap<>();

    public Map<String, String> getFieldMap() {
        return fieldMap;
    }

    public FormatColTab putField(String key, String value) {
        this.fieldMap.put(key, value);
        return this;
    }

    public String getDataSetColID() {
        return dataSetColID;
    }

    public void setDataSetColID(String dataSetColID) {
        this.dataSetColID = dataSetColID;
    }

    private String dataSetColID;

    public String getTabName() {
        return tabName;
    }

    public void setTabName(String tabName) {
        this.tabName = tabName;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getSelectIcon() {
        return selectIcon;
    }

    public void setSelectIcon(String selectIcon) {
        this.selectIcon = selectIcon;
    }

    @Override
    public String toString() {
        return "FormatColTab{" +
                "tabName='" + tabName + '\'' +
                ", icon='" + icon + '\'' +
                ", selectIcon='" + selectIcon + '\'' +
                '}';
    }
}
