//package com.pansoft.xmlparse;
//
//import com.pansoft.resmanager.ResFileManager;
//
//import org.w3c.dom.Document;
//import org.w3c.dom.Element;
//import org.w3c.dom.Node;
//import org.xml.sax.InputSource;
//
//import java.io.File;
//import java.io.StringReader;
//
//import javax.xml.parsers.DocumentBuilder;
//import javax.xml.parsers.DocumentBuilderFactory;
//
///**
// * 解析Mobile_FMT文件工具
// *
// * @author long
// * 2015-3-14 14:51:51
// */
//public class MobileFormatUtil备份 {
//    private FormatSet formatSet;
//
//    public FormatSet getFormatSet() {
//        return formatSet;
//    }
//
//    private String resPath = ResFileManager.PACKAGE_DIR + File.separator + "Mobile_FMT.xml";
//    private static MobileFormatUtil备份 mInstance;
//    private File resFile;
//
//    private MobileFormatUtil备份() {
//        resFile = new File(resPath);
//        parseXML(resFile);
//    }
//
//    public static MobileFormatUtil备份 getInstance() {
//        if (mInstance == null) {
//            synchronized (MobileFormatUtil备份.class) {
//                if (mInstance == null) {
//                    mInstance = new MobileFormatUtil备份();
//                }
//            }
//        }
//        return mInstance;
//    }
//
//    public static void release() {
//        mInstance = null;
//    }
//
//    /**
//     * 解析Mobile_FMT 文件 返回对象OBJECT
//     *
//     * @param file
//     * @return
//     */
//    private FormatSet parseXML(File file) {
//        FormatSet formatSetTemp = null;
//        String xmlString = ResFileManager.decryptFile(file);
//        try {
//            formatSetTemp = parseXMl(stringToElement(xmlString));
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        return formatSetTemp;
//    }
//
//    private FormatSet parseXMl(Node rootNode) throws Exception {
//        String id = ((Element) rootNode).getAttribute("id");
//        if ("mobile_fmt".equals(id)) {
//            formatSet = FormatParse.xml2FormatSet(rootNode);
//            return formatSet;
//        }
//        return formatSet;
//    }
//
//    /**
//     * 灏�瀛�绗�涓茶浆���涓�element
//     *
//     * @param xml
//     * @return
//     * @throws Exception
//     */
//    public static Element stringToElement(String xml) throws Exception {
//        StringReader sr = new StringReader(xml);
//        InputSource is = new InputSource(sr);
//        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
//        DocumentBuilder builder = factory.newDocumentBuilder();
//        Document doc = builder.parse(is);
//        Element rootElement = doc.getDocumentElement();
//        return rootElement;
//
//    }
//}
