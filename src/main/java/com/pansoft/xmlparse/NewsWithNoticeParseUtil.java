package com.pansoft.xmlparse;

import com.pansoft.espmodel.NewsAndNotice;
import com.pansoft.resmanager.ResFileManager;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.xml.sax.InputSource;

import java.io.File;
import java.io.StringReader;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

/**
 * 解析Mobile_FMT文件工具
 * @author long
 *2015-3-14 14:51:51
 */
@Deprecated
public class NewsWithNoticeParseUtil {
	private NewsAndNotice nwn;
	public NewsAndNotice getNWN() {
		return nwn;
	}
	private static String resPath = ResFileManager.PACKAGE_DIR+File.separator;
	private static NewsWithNoticeParseUtil mInstance;
	private File resFile;
	private NewsWithNoticeParseUtil(){
		resFile = new File(resPath);
		nwn = parseXML(resFile);
	}
	public static NewsWithNoticeParseUtil getInstance(String xml)
	{
		resPath =ResFileManager.PACKAGE_DIR+File.separator+xml;
		mInstance = new NewsWithNoticeParseUtil();
		/*if (mInstance == null)
		{
			synchronized (NewsWithNoticeParseUtil.class)
			{
				if (mInstance == null)
				{
					mInstance = new NewsWithNoticeParseUtil();
				}
			}
		}*/
		return mInstance;
	}
	/**
	 * 解析Mobile_FMT 文件 返回对象OBJECT
	 * @param file
	 * @return
	 */
	private NewsAndNotice parseXML(File file){
		NewsAndNotice nwn = null;
		String xmlString = ResFileManager.decryptFile(file);
		try {
			//nwnList1 = parseXMl(MenuParse.stringToElement(xmlString));
			Node rootNode = stringToElement(xmlString);
			String id = ((Element)rootNode).getAttribute("id");
			if ("option".equals(id)) {
				nwn = OptionParse.xml2FormatSet(rootNode);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return nwn;
	}
/*	private FormatSet parseXMl(Node rootNode) throws Exception {
		String id = ((Element) rootNode).getAttribute("id");
		if ("option".equals(id)) {
			formatSet = FormatParse.xml2FormatSet(rootNode);
			return formatSet;
		}
		return formatSet; 
	}*/
	/**
	 * 灏�瀛�绗�涓茶浆���涓�element
	 *
	 * @param xml
	 * @return
	 * @throws Exception
	 */
	public static Element stringToElement(String xml) throws Exception {
		StringReader sr = new StringReader(xml);
		InputSource is = new InputSource(sr);
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = factory.newDocumentBuilder();
		Document doc = builder.parse(is);
		Element rootElement = doc.getDocumentElement();
		return rootElement;

	}
}
