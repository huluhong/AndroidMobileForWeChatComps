package com.pansoft.xmlparse;

import java.util.List;

public class FormatRow {
	List<FormatCol> list;

	public List<FormatCol> getList() {
		return list;
	}

	public void setList(List<FormatCol> list) {
		this.list = list;
	}
}
