package com.pansoft.xmlparse;

import java.util.List;

/**
 * Created by zhangshunyun on 2017/8/29.
 */
@Deprecated
public class FormatRowTab {
    List<FormatColTab> list;

    public List<FormatColTab> getList() {
        return list;
    }

    public void setList(List<FormatColTab> list) {
        this.list = list;
    }
}
