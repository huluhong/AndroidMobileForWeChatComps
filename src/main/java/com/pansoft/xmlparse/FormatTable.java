package com.pansoft.xmlparse;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FormatTable {
    private String id;
    private String caption;
    private String detailCol;
    private String detailName;
    private String formHeadName;
    private String formPartName;
    //
    private String textAlign;
    private String columnType;
    private String numberPrecision;
    private String textFormat;
    private String dateFormat;
    private String mask;
    private String icon;

    //其他字段放入 map
    private Map<String, String> fieldMap = new HashMap<>();

    public Map<String, String> getFieldMap() {
        return fieldMap;
    }

    public FormatTable putField(String key, String value) {
        this.fieldMap.put(key, value);
        return this;
    }

    public String getField(String key, String defaultVale) {
        return fieldMap.get(key) == null ? defaultVale : fieldMap.get(key);

    }

    public String getIcon() {
        return icon;
    }

    public FormatTable setIcon(String icon) {
        this.icon = icon;
        return this;
    }

    public String getTextAlign() {
        return textAlign;
    }

    public void setTextAlign(String textAlign) {
        this.textAlign = textAlign;
    }

    public String getColumnType() {
        return columnType;
    }

    public void setColumnType(String columnType) {
        this.columnType = columnType;
    }

    public String getNumberPrecision() {
        return numberPrecision;
    }

    public void setNumberPrecision(String numberPrecision) {
        this.numberPrecision = numberPrecision;
    }

    public String getTextFormat() {
        return textFormat;
    }

    public void setTextFormat(String textFormat) {
        this.textFormat = textFormat;
    }

    public String getDateFormat() {
        return dateFormat;
    }

    public void setDateFormat(String dateFormat) {
        this.dateFormat = dateFormat;
    }

    private List<FormatRow> formatRowList;

    public String getFormHeadName() {
        return formHeadName;
    }

    public void setFormHeadName(String formHeadName) {
        this.formHeadName = formHeadName;
    }

    public String getFormPartName() {
        return formPartName;
    }

    public void setFormPartName(String formPartName) {
        this.formPartName = formPartName;
    }

    public String getDetailCol() {
        return detailCol;
    }

    public void setDetailCol(String detailCol) {
        this.detailCol = detailCol;
    }

    public String getDetailName() {
        return detailName;
    }

    public void setDetailName(String detailName) {
        this.detailName = detailName;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<FormatRow> getFormatRowList() {
        return formatRowList;
    }

    public void setFormatRowList(List<FormatRow> formatRowList) {
        this.formatRowList = formatRowList;
    }

    public String getCaption() {
        return caption;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }

    @Override
    public String toString() {
        return "FormatTable [id=" + id + ", caption=" + caption
                + ", detailCol=" + detailCol + ", detailName=" + detailName
                + ", formHeadName=" + formHeadName + ", formPartName="
                + formPartName + ", formatRowList=" + formatRowList + "]";
    }

    public String getMask() {
        return mask;
    }

    public void setMask(String mask) {
        this.mask = mask;
    }

}
