package com.pansoft.xmlparse;

import java.util.List;

public class FormBean {
	/**表单的id*/
	private String id;
	
	private String includedId;
	
	private String name;
	
	private String icon;
	
	private List<ViewLayout> viewList;
	
	/**深度，xml解析时使用*/
	public int depth;
	
	public void FromBean(){
	}
	public void setParam(String param, String value){
		if("id".equals(param)){
			setId(value);
		}else if("includedId".equals(param)){
			setIncludedId(value);
		}else if("name".equals(param)){
			setName(value);
		}else if("icon".equals(param)){
			setIcon(value);
		}
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getIncludedId() {
		return includedId;
	}
	public void setIncludedId(String includedId) {
		this.includedId = includedId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getIcon() {
		return icon;
	}
	public void setIcon(String icon) {
		this.icon = icon;
	}
	public List<ViewLayout> getViewList() {
		return viewList;
	}
	public void setViewList(List<ViewLayout> viewList) {
		this.viewList = viewList;
	}
	public int getDepth() {
		return depth;
	}
	public void setDepth(int depth) {
		this.depth = depth;
	}
	
}
