package com.pansoft.xmlparse;

public class FormTag {

	/*--------- FORM表单中的所有标签 ---------*/
	public static final String TAG_APP = "application";
	public static final String TAG_FORMS = "forms";
	public static final String TAG_FORM = "form";
	public static final String TAG_C = "C";
	public static final String TAG_MODLES ="models";
	public static final String TAG_P = "P";
	public static final String TAG_M = "M";
	public static final String TAG_K = "k";
	public static final String TAG_EVENTS = "events";
	public static final String TAG_CLAZZ="clazz";
	public static final String TAG_NAME="name";
	public static final String TAG_TYPE="type";
	public static final String TAG_ISREFTYPE="isRefType";
}
