package com.pansoft.xmlparse;

import java.util.HashMap;
import java.util.Map;

/**
 * 
 * 后台组件和android端组件的映射
 * @author zhangjg
 *
 */
public class ComponentMap {
	
	private static final  Map<String, String> map = new HashMap<String, String>();
	
	
	static{
		//初始化map
		map.put("com.efounder.form.application::FormViewContainer", 
				"com.pansoft.espcomp.ESPFormContainer");
		
		map.put("com.efounder.components::MobileTextInput", 
				"com.pansoft.espcomp.ESPBasicTextInput");
		
		map.put("com.efounder.controls::FormButton", 
				"com.pansoft.espcomp.ESPBasicButton");
		
		map.put("com.efounder.components::List", 
				"com.pansoft.espcomp.ESPListView");
		
		map.put("com.efounder.comp::DMGrid", 
				//"com.pansoft.espcomp.ESPTableView");
				"com.efounder.aichart.AichartsArea");
		
		//图表的映射  zhangjg 2013 12 13
		map.put("com.efounder.amChart::AmAreaAdgChart", 
				"com.pansoft.espcomp.ESPChartArea");
		
		
		map.put("com.efounder.amChart::AmXYAdgChart", 
				"com.pansoft.espcomp.ESPChartBubble");
		
		
		map.put("com.efounder.amChart::AmCandlestickAdgChart", 
				"com.pansoft.espcomp.ESPChartCandle");
		
		
		map.put("com.efounder.amChart::AmLineAdgChart", 
				"com.pansoft.espcomp.ESPChartLine");
		
		
		
		map.put("com.efounder.amChart::AmPieAdgChart", 
				"com.pansoft.espcomp.ESPChartPie");
		
		
		map.put("com.efounder.amChart::AmRadarAdgChart", 
				"com.pansoft.espcomp.ESPChartPolar");
		
		
		map.put("com.efounder.amChart::AmColumnAdgChartMX", 
				"com.pansoft.espcomp.ESPChartColumn");
		
		map.put("com.efounder.charts.basechart::BaseChartLine", 
//				"com.pansoft.espcomp.ESPChartArea");
				"com.efounder.aichart.AichartsLine");
		map.put("com.efounder.charts.basechart::BaseChartColumn", 
//				"com.pansoft.espcomp.ESPChartArea");
				"com.efounder.aichart.AichartsColumn");
		map.put("com.efounder.charts.basechart::BaseChartPie", 
//				"com.pansoft.espcomp.ESPChartArea");
				"com.efounder.aichart.AichartsPie");
		map.put("com.efounder.charts.basechart::BaseChartRose", 
//				"com.pansoft.espcomp.ESPChartArea");
				"com.efounder.aichart.AichartsRose");
		map.put("com.efounder.charts.basechart::BaseChartArea", 
//				"com.pansoft.espcomp.ESPChartArea");
				"com.efounder.aichart.AichartsArea");
				//"com.pansoft.espcomp.DMGrid");
		
		/*map.put("com.efounder.charts.basechart::BaseChartBubble", 
				"com.pansoft.espcomp.ESPChartBubble");
		
		
		map.put("com.efounder.charts.basechart::BaseChartCandlestick", 
				"com.pansoft.espcomp.ESPChartCandle");
		
		
		map.put("com.efounder.charts.basechart::BaseChartLine", 
				"com.pansoft.espcomp.ESPChartLine");
		
		
		map.put("com.efounder.charts.basechart::BaseChartPie", 
				"com.pansoft.espcomp.ESPChartPie");
		
		
		map.put("com.efounder.charts.basechart::BaseChartBar", 
				"com.pansoft.espcomp.ESPChartColumn");   //测试
		
		map.put("com.efounder.charts.basechart::BaseChartColumn", 
				"com.pansoft.espcomp.ESPChartColumn");
		*/
		
		
		/*
		 * 测试专用
		 */
		
		map.put("com.efounder.grid.builder::AdvancedDataGridBuilder", 
				"com.pansoft.espcomp.DMGrid");
		map.put("spark.components::VGroup", 
				"com.pansoft.espcomp.VGroup");
		map.put("spark.components::HGroup", 
				"com.pansoft.espcomp.HGroup");
		map.put("com.efounder.controls::FormLabel", 
				"com.pansoft.espcomp.FormLabel");
		map.put("com.efounder.amChart::AmColumnAdgChartMX", 
				"com.efounder.aichart.AichartsColumn");
		map.put("com.efounder.amChart::AmLineAdgChart", 
				"com.efounder.aichart.AichartsLine");
		map.put("com.efounder.amChart::AmPieAdgChart", 
				"com.efounder.aichart.AichartsPie");
		
		map.put("com.pansoft.androidcomp.timetiker.FormDateTimeField", "com.pansoft.androidcomp.timetiker.FormDateTimeField");
		map.put("com.pansoft.espcomp.CellReportComboBox", "com.pansoft.espcomp.CellReportComboBox");
		map.put("com.pansoft.android.affix.AffixImageExplorer", "com.pansoft.android.affix.AffixImageExplorer");
		map.put("com.pansoft.espcomp.affixserverbutton.AffixServerButton", "com.pansoft.espcomp.affixserverbutton.AffixServerButton");
		
		
	}
	
	public static String getMappedComponentName(String mappingName){
		return map.get(mappingName);
		
	}
}
