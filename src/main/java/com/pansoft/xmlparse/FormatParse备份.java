//package com.pansoft.xmlparse;
//
//import android.util.Log;
//
//import org.w3c.dom.Element;
//import org.w3c.dom.NamedNodeMap;
//import org.w3c.dom.Node;
//import org.w3c.dom.NodeList;
//
//import java.util.ArrayList;
//import java.util.List;
//
//public class FormatParse备份 {
//
//	private static String TAG_SERVICE="service";
//	private static String TAG_TABLENAME="TableName";
//	private static String TAG_ROW="Row";
//	private static String TAG_COL="Col";
//	/**
//	 * 解析移动显示格式设置xml
//	 * @param
//	 * @return
//	 */
//	public static FormatSet xml2FormatSet(Node rootNode){
//		FormatSet formatSet=new FormatSet();
//		String id=((Element)rootNode).getAttribute("id");
//		String caption=((Element)rootNode).getAttribute("caption");
//		String textAlign=((Element)rootNode).getAttribute("textAlign");
//		String columnType=((Element)rootNode).getAttribute("columnType");
//		String numberPrecision=((Element)rootNode).getAttribute("numberPrecision");
//		String textFormat=((Element)rootNode).getAttribute("textFormat");
//		String dateFormat=((Element)rootNode).getAttribute("dateFormat");
//		String mask=((Element)rootNode).getAttribute("mask");
//
//		formatSet.setId(id);
//		formatSet.setCaption(caption);
//		formatSet.setTextAlign(textAlign);
//		formatSet.setColumnType(columnType);
//		formatSet.setNumberPrecision(numberPrecision);
//		formatSet.setTextFormat(textFormat);
//		formatSet.setDateFormat(dateFormat);
//		formatSet.setMask(mask);
//		//获取service标签
//		NodeList itemsService = ((Element)rootNode).getElementsByTagName(TAG_SERVICE);
//		List<FormatService> serviceList=new ArrayList<FormatService>();
//		for (int i = 0; i < itemsService.getLength(); i++) {
//			FormatService formatService=new FormatService();
//	        Element personNode = (Element) itemsService.item(i);
//	        setAttributes(personNode, formatService);
//	        serviceList.add(formatService);
//		}
//		formatSet.setServiceList(serviceList);
//		//获取TableName标签
//		NodeList itemsTable = ((Element)rootNode).getElementsByTagName(TAG_TABLENAME);
//		List<FormatTable> tableList=new ArrayList<FormatTable>();
//		List<FormatTableTab> tableTabsList = new ArrayList<>();
//		for (int i = 0; i < itemsTable.getLength(); i++) {
//			FormatTable formatTable=new FormatTable();
//            FormatTableTab formatTableTab = new FormatTableTab();
//	        Element personNode = (Element) itemsTable.item(i);
//	        setAttributes(personNode, formatTable);
//            setAttributes(personNode,formatTableTab);
//	        //获取子标签col
//	        NodeList itemsRow = personNode.getElementsByTagName(TAG_ROW);
//	        List<FormatRow> rowList=new ArrayList<FormatRow>();
//            List<FormatRowTab> rowTabsList = new ArrayList<>();
//	        for(int j = 0; j< itemsRow.getLength(); j++){
//	        	FormatRow row = new FormatRow();
//                FormatRowTab rowTab = new FormatRowTab();
//	        	Element childNode = (Element) itemsRow.item(j);
//	            NodeList itemsCol = childNode.getElementsByTagName(TAG_COL);
//	            List<FormatCol> colList=new ArrayList<FormatCol>();
//				ArrayList<FormatColTab> colTabsList = new ArrayList<>();
//				for(int k = 0; k< itemsCol.getLength(); k++){
//		        	FormatCol formatCol=new FormatCol();
//					FormatColTab formatColTab = new FormatColTab();
//					Element colPersonNode = (Element) itemsCol.item(k);
//			        setAttributes(colPersonNode, formatCol);
//					setAttributes(colPersonNode,formatColTab);
//			        colList.add(formatCol);
//					colTabsList.add(formatColTab);
//		         }
//		        row.setList(colList);
//				rowTab.setList(colTabsList);
//		        rowList.add(row);
//                rowTabsList.add(rowTab);
//	        }
//	        formatTable.setFormatRowList(rowList);
//            formatTableTab.setFormatRowList(rowTabsList);
//	        tableList.add(formatTable);
//			tableTabsList.add(formatTableTab);
//		}
//		formatSet.setTableList(tableList);
//		formatSet.setTableTabList(tableTabsList);
//		return formatSet;
//	}
//	public static void setAttributes(Element node, Object object ){
//		NamedNodeMap nameNodeMap=node.getAttributes();
//        for(int i=0; i<nameNodeMap.getLength(); i++){
//        	String attributeName=nameNodeMap.item(i).getNodeName();
//        	String attributeValue=nameNodeMap.item(i).getNodeValue();
//        	Log.v("formatparse", attributeName+" "+ attributeValue);
//        	try {
//				PropertyUtils.setProperty(object, attributeName, "String", attributeValue);
//			} catch (Exception e) {
//				// TODO Auto-generated catch block
//				Log.e("FormatParse", "设置 "+attributeName+" 失败");
//				e.printStackTrace();
//			}
//        }
//	}
//}
