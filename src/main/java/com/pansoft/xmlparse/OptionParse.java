package com.pansoft.xmlparse;


import android.util.Log;

import com.pansoft.espmodel.NewsAndNotice;

import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * 以前给普光通知公告使用，请勿在使用
 */
@Deprecated
public class OptionParse {

	private static String TAG_SERVICE="node";
	//private static String TAG_TABLENAME="TableName";
	//private static String TAG_ROW="Row";
	//private static String TAG_COL="Col";
	/**
	 * 解析移动显示格式设置xml
	 *
	 * @return
	 */
	public static NewsAndNotice xml2FormatSet(Node rootNode){
		NewsAndNotice nwn=new NewsAndNotice();
		/*String id=((Element)rootNode).getAttribute("id");
		String caption=((Element)rootNode).getAttribute("caption");
		formatSet.setId(id);
		formatSet.setCaption(caption);*/
		//获取service标签
		NodeList itemsService = ((Element)rootNode).getElementsByTagName(TAG_SERVICE);
		//List<FormatService> serviceList=new ArrayList<FormatService>();
	        Element personNode = (Element) itemsService.item(0);
	        setAttributes(personNode, nwn);
		return nwn;
		}
		/*formatSet.setServiceList(serviceList);
		//获取TableName标签
		NodeList itemsTable = ((Element)rootNode).getElementsByTagName(TAG_TABLENAME);
		List<FormatTable> tableList=new ArrayList<FormatTable>();
		for (int i = 0; i < itemsTable.getLength(); i++) { 
			FormatTable formatTable=new FormatTable();
	        Element personNode = (Element) itemsTable.item(i);
	        setAttributes(personNode, formatTable);
	        //获取子标签col
	        NodeList itemsRow = personNode.getElementsByTagName(TAG_ROW);
	        List<FormatRow> rowList=new ArrayList<FormatRow>();
	        for(int j = 0; j< itemsRow.getLength(); j++){
	        	FormatRow row = new FormatRow();
	        	Element childNode = (Element) itemsRow.item(j);
	            NodeList itemsCol = childNode.getElementsByTagName(TAG_COL);
	            List<FormatCol> colList=new ArrayList<FormatCol>();
		        for(int k = 0; k< itemsCol.getLength(); k++){
		        	FormatCol formatCol=new FormatCol();
			        Element colPersonNode = (Element) itemsCol.item(k);
			        setAttributes(colPersonNode, formatCol);
			        colList.add(formatCol);
		         }
		        row.setList(colList);
		        rowList.add(row);
	        }
	        formatTable.setFormatRowList(rowList);
	        tableList.add(formatTable);
		}
		formatSet.setTableList(tableList);
		return nwnList;
	}*/
	public static void setAttributes(Element node, Object object ){
		NamedNodeMap nameNodeMap=node.getAttributes();
        for(int i=0; i<nameNodeMap.getLength(); i++){
        	String attributeName=nameNodeMap.item(i).getNodeName();
        	String attributeValue=nameNodeMap.item(i).getNodeValue();
        	Log.v("formatparse", attributeName+" "+ attributeValue);
        	try {
				PropertyUtils.setProperty(object, attributeName, "String", attributeValue);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				Log.e("FormatParse", "设置 "+attributeName+" 失败");
				e.printStackTrace();
			}
        }
	}
}
