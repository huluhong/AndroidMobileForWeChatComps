package com.pansoft.xmlparse;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by zhangshunyun on 2017/8/29.
 */
@Deprecated
public class FormatTableTab {
    private String id;
    private String caption;
    private String detailCol;
    private String detailName;
    private String formHeadName;
    private String formPartName;
    private List<FormatRowTab> formatRowList;
    private String icon;

    //其他字段放入 map
    private Map<String, String> fieldMap = new HashMap<>();

    public Map<String, String> getFieldMap() {
        return fieldMap;
    }

    public FormatTableTab putField(String key, String value) {
        this.fieldMap.put(key, value);
        return this;
    }

    public String getFormHeadName() {
        return formHeadName;
    }
    public void setFormHeadName(String formHeadName) {
        this.formHeadName = formHeadName;
    }
    public String getFormPartName() {
        return formPartName;
    }
    public void setFormPartName(String formPartName) {
        this.formPartName = formPartName;
    }

    public String getDetailCol() {
        return detailCol;
    }
    public void setDetailCol(String detailCol) {
        this.detailCol = detailCol;
    }
    public String getDetailName() {
        return detailName;
    }
    public void setDetailName(String detailName) {
        this.detailName = detailName;
    }
    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }
    public List<FormatRowTab> getFormatRowList() {
        return formatRowList;
    }
    public void setFormatRowList(List<FormatRowTab> formatRowList) {
        this.formatRowList = formatRowList;
    }
    public String getCaption() {
        return caption;
    }
    public void setCaption(String caption) {
        this.caption = caption;
    }
    @Override
    public String toString() {
        return "FormatTable [id=" + id + ", caption=" + caption
                + ", detailCol=" + detailCol + ", detailName=" + detailName
                + ", formHeadName=" + formHeadName + ", formPartName="
                + formPartName + ", formatRowList=" + formatRowList + "]";
    }
}
