package com.pansoft.xmlparse;

import java.util.HashMap;
import java.util.Map;

public class FormatService {
    private String id;
    private String key;
    //其他字段放入 map
    private Map<String, String> fieldMap = new HashMap<>();

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }




    public Map<String, String> getFieldMap() {
        return fieldMap;
    }

    public void putField(String key, String value) {
        this.fieldMap.put(key, value);
    }

    public String getField(String key, String defaultVale) {
        return fieldMap.get(key) == null ? defaultVale : fieldMap.get(key);

    }

    @Override
    public String toString() {
        return "FormatService [id=" + id + ", key=" + key + "]";
    }

}
