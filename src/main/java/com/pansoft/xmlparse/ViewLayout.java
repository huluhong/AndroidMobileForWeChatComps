package com.pansoft.xmlparse;

import java.util.List;

/**
 * 当前xml界面布局结构
 * @author yanxianhai
 *
 */
public class ViewLayout {
	/**视图id*/
	private String id;
	
	/**视图实例*/
	private Object object;
	
	/**子视图列表*/
	private List<ViewLayout> viewList;
	
	/**xml解析时的深度*/
	public int depth=0;
	
	/**
	 * 构造函数
	 */
	public ViewLayout(){
	}
}
