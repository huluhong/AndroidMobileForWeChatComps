package com.pansoft.xmlparse;

import java.util.Map;

import android.view.View;

/**
 * 
 * ESPMobile表单解析接口
 * @author zhangjg
 *
 */
public interface FormParser {

	/**
	 * 解析表单
	 * @throws Exception
	 */
	void parse()throws Exception;
	
	/**
	 * 获取一个form
	 * @param id
	 * @return
	 */
	View getForm(String id);
	
	/**
	 * 获取解析出来的所有form
	 * @return
	 */
	Map<String, View> getForms();
	
	/**
	 * 获取模型
	 * @param id
	 * @return
	 */
	Object getModle(String id);
	
	/**
	 * 获取所有模型
	 * @return
	 */
	Map<String, Object> getModles();
	
	/**
	 * 获得一个event
	 * @param id
	 * @return
	 */
	Object getEvent(String id);
	
	/**
	 * 获取所有evnet
	 * @return
	 */
	Map<String, Object> getEvents();
	
}
