package com.pansoft.espservice;

import com.efounder.eai.EAI;
import com.efounder.eai.data.JParamObject;
import com.efounder.eai.data.JResponseObject;
import com.pansoft.espform.base.DataComponent;

public class ESPService extends DataComponent{
	public  ESPService()
	{
		super();
	}
	
	/**
	 * 调用后台服务
	 * @param po 参数对象
	 * @param s 成功返回后执行的方法
	 * @param dao 附加参数
	 * @param f 调用失败，默认处理函数
	 * @param co 附加参数
	 * @param userObject StubObject 可在处理函数中获取此参数值
	 * @param ao 附加参数
	 * @throws Exception 
	 */
	public JResponseObject executeService(JParamObject po,Object s,Object dao,
			Object f,Object co ,Object userObject,Object ao) throws Exception 
	{
		if ( _serviceKey != null ){
			
			
			/*String objectName = "BZServiceComponentManager";
			String objectMethod = "syncRunService";*/
		
			//return EAI.DAL.IOM(objectName, objectMethod, po, null, null, null);

			
			return EAI.DAL.SVR(_serviceKey, po, dao, co, ao);
		}else{
			System.out.println("_serviceKey====null");
			return null;
		}
	}
	
	private String _serviceKey = "";

	/**
	 * 被调用后台服务的名称
	 */
	public String getServiceKey()
	{
		return _serviceKey;
	}

	/**
	 * @private
	 */
	public void setServiceKey(String value)
	{
		_serviceKey = value;
	}
}
