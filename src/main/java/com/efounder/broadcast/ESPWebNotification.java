package com.efounder.broadcast;

import java.util.HashMap;

import com.efounder.util.WebViewUtil;
import com.pansoft.espcomp.ESPWebView;
import com.pansoft.espmodel.EFWebNoticationObject;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.webkit.WebView;

	 /*
	  * WEBVIEW 监听广播
	  */
		public class ESPWebNotification extends BroadcastReceiver{
			ESPWebView espWebView;
			public ESPWebNotification(ESPWebView espWebView){
				this.espWebView = espWebView;
			}
		    @Override
		    public void onReceive(Context context, Intent intent) {
		        //String action = intent.getAction();
		        //String name = intent.getStringExtra("name");
		    	EFWebNoticationObject efWebNoticationObject = (EFWebNoticationObject) intent.getSerializableExtra("name");
		    	String result = WebViewUtil.openEspMap(efWebNoticationObject);
		    	String type = efWebNoticationObject.getType();
		    	if(type.equals("date")){
		    		espWebView.loadUrl("http://www.baidu.com");
		    		
		    	}else if(type.equals("")){
		    		
		    	}
		    	
		    }
		}
