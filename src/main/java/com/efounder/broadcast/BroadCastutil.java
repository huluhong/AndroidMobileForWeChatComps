package com.efounder.broadcast;

import java.util.LinkedList;
import java.util.List;

import android.content.BroadcastReceiver;
import android.content.Context;

public class BroadCastutil{
	private static BroadCastutil broadCastutil;
	private List<BroadcastReceiver> broadcastReceiverList=new LinkedList<BroadcastReceiver>();
	ESPWebNotification espWebNotification;
	public ESPWebNotification getEspWebNotification() {
		return espWebNotification;
	}
	private BroadCastutil(){
		
	}
	public static BroadCastutil getinstance(){
		if(null==broadCastutil){
			broadCastutil = new BroadCastutil();
		}
		return broadCastutil;
	}
	public void addbroadCast(BroadcastReceiver broadcastReceiver){
		
		broadcastReceiverList.add(broadcastReceiver);
	}
	public void addbroadCast(ESPWebNotification espWebNotification){
		this.espWebNotification = espWebNotification;
	}
	public void unregisterWebBroadcastList(Context context){
		context.unregisterReceiver(espWebNotification);
		espWebNotification = null;
	}
	public void unregisterBroadcastList(Context context){
		for (BroadcastReceiver broadcastReceiver:broadcastReceiverList){
			context.unregisterReceiver(broadcastReceiver);
		}
	}
}