package com.efounder.util;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.BitmapDrawable;
import android.os.Build;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.pansoft.android.espbase.R;
import com.utilcode.util.ReflectUtils;

import java.lang.reflect.Method;


public class LoadingDataUtilBlack {

    /*
     * 定义loadingDialog
     */
    private static Dialog loadingDialog;
    /*
     * 定义Appcontext全局变量
     */
    //public static AppContext appContext = AppContext.getInstance();
    // private static Context mContext;
    /*
     * 加载显示的文字
     */
    public static String stringShow;

    /*
     * 重载方法
     */
    public static void show(Context context) {
        context = context;
        if (context == null) {
            return;
        }
        if (!((Activity) context).isFinishing())
            show(context, context.getResources().getString(R.string.common_text_loading));
    }

    /*
     * 显示dialog
     */
    public static void show(Context context, String msg) {

        if (context == null) {
            return;
        }

        if (isClassExists(context.getResources().getString(R.string.loadingdataview))) {
            //检查是否存在配置的loadingdataview
//            try {
//                Class class1 = Class.forName(context.getResources().getString(R.string.loadingdataview));
//                Method showMethod = class1.getMethod("show", new Class[]{Context.class, String.class});
//                showMethod.invoke(null, new Object[]{context, msg});
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
            try {
                ReflectUtils reflectUtils = ReflectUtils.reflect(context.getResources().getString(R.string.loadingdataview));
                reflectUtils.method("show", context, msg);
            } catch (Exception e) {
                e.printStackTrace();
            }

            return;
        }

        if (loadingDialog == null || !loadingDialog.isShowing()) {

            // 用来载入LinearLayout
            LayoutInflater layoutInflater = LayoutInflater.from(context);

            // 创建视图
            View v = layoutInflater.inflate(R.layout.loadingdata_dialog_ios, null);

            // 加载布局
            LinearLayout layout = (LinearLayout) v.findViewById(R.id.dialog_view_ios);

            // 加载imageView

            ImageView imageView = (ImageView) v.findViewById(R.id.loading_img_ios);
            ImageView imageViewa = (ImageView) v.findViewById(R.id.loading_img_iosa);

            // 设置textVeiw
            TextView textView = (TextView) v.findViewById(R.id.loading_textView_ios);
            if (msg.equals("")) {
                textView.setText(msg);

            } else {
                //textView.setText(msg+"..." );
                textView.setText(msg);

            }

            Resources res = context.getResources();
            float fontsize = res.getDimension(R.dimen.activity_login_button_fontsize);
//            textView.setTextSize(16);
            textView.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 16);

            //获取Imageview设置图片的宽度和高度
            Bitmap bitmap = ((BitmapDrawable) imageViewa.getBackground()).getBitmap();


            AnimationDrawable animationDrawable = (AnimationDrawable) imageView.getBackground();

            animationDrawable.start();
            // 加载动画
            //Animation loadingAnimation = AnimationUtils.loadAnimation(appContext,
            //R.anim.loading_animation);

            // 使用ImageView显示动画
            //imageView.startAnimation(loadingAnimation);
            //imageViewa.startAnimation(loadingAnimation);
            loadingDialog = new Dialog(context, R.style.loadingData_dialog);
            loadingDialog.setCanceledOnTouchOutside(false);// 设置点击屏幕Dialog不消失
            // 不可以用“返回键”取消
            // loadingDialog.setCancelable(false);
            //loadingDialog.setCancelable(true);
            loadingDialog.setContentView(layout);

            //设置dialog的大小
            Window dialogWindow = loadingDialog.getWindow();
            WindowManager.LayoutParams lp = dialogWindow.getAttributes();
            lp.width = bitmap.getWidth() + textView.getLineHeight() + 200;
            int w = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);
            int h = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);
            textView.measure(w, h);
            //lp.height = bitmap.getHeight() + textView.getMeasuredHeight() + 110;
            lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
            dialogWindow.setAttributes(lp);

		/*loadingDialog.getWindow().setType(
                WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);*/
            if (!((Activity) context).isFinishing())
                loadingDialog.show();
        }
    }

    /*
     * 取消dialog
     */

    public static void dismiss() {
//        if (loadingDialog != null){
//            loadingDialog.dismiss();
//            loadingDialog=null;
//        }
        if (isClassExists(AppContext.getInstance().getResources().getString(R.string.loadingdataview))) {
            //检查是否存在配置的loadingdataview
            try {
                Class class1 = Class.forName(AppContext.getInstance().getResources().getString(R.string.loadingdataview));
                Method showMethod = class1.getMethod("dismiss", new Class[]{});
                showMethod.invoke(null, new Object[]{});
            } catch (Exception e) {
                e.printStackTrace();
            }
            return;
        }


        if (loadingDialog != null) {
            if (loadingDialog.isShowing()) {
                Context context = ((ContextWrapper) loadingDialog.getContext()).getBaseContext();
                if (context instanceof Activity) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                        if (!((Activity) context).isDestroyed())
                            loadingDialog.dismiss();
                    }
                } else {
                    loadingDialog.dismiss();
                }
            }
            loadingDialog = null;
        }
    }

    /*
     * 取消dialog
     */

    public static Dialog getDialog() {
        return loadingDialog;
    }


    private static final boolean isClassExists(String classFullName) {
        try {
            Class.forName(classFullName);
            return true;
        } catch (ClassNotFoundException e) {
            return false;
        }
    }
}
