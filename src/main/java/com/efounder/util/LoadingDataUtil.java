package com.efounder.util;

import android.app.Dialog;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.efounder.utils.ResStringUtil;
import com.pansoft.android.espbase.R;


public class LoadingDataUtil {

	/*
	 * 定义loadingDialog
	 */
	private static Dialog loadingDialog;

	/*
	 * 定义Appcontext全局变量
	 */
	public static AppContext appContext = AppContext.getInstance();

	/*
	 * 加载显示的文字
	 */
	public static String stringShow;

	/*
	 * 重载方法
	 */
	public static void show() {

		show(ResStringUtil.getString(R.string.common_text_loading));
	}

	/*
	 * 显示dialog
	 */
	public static void show(String msg) {
		if(loadingDialog ==null||!loadingDialog.isShowing()){

		// 用来载入LinearLayout
		LayoutInflater layoutInflater = LayoutInflater.from(appContext);

		// 创建视图
		View v = layoutInflater.inflate(R.layout.loadingdata_dialog, null);

		// 加载布局
		LinearLayout layout = (LinearLayout) v.findViewById(R.id.dialog_view);

		// 加载imageView

		ImageView imageView = (ImageView) v.findViewById(R.id.loading_img);
       
		// 设置textVeiw
		TextView textView = (TextView) v.findViewById(R.id.loading_textView);
		if(msg.equals("")){
		textView.setText(msg );
		}else{
		textView.setText(msg+"..." );
		}
		
		Resources res = appContext.getResources(); 
		float fontsize = res.getDimension(R.dimen.activity_login_button_fontsize);
		textView.setTextSize(fontsize);

		//获取Imageview设置图片的宽度和高度
		Bitmap bitmap = ((BitmapDrawable)imageView.getBackground()).getBitmap();
		
		// 加载动画
		Animation loadingAnimation = AnimationUtils.loadAnimation(appContext,
				R.anim.loading_animation);

		// 使用ImageView显示动画
		imageView.startAnimation(loadingAnimation);
		loadingDialog = new Dialog(appContext, R.style.loadingData_dialog);
		loadingDialog.setCanceledOnTouchOutside(false);// 设置点击屏幕Dialog不消失 
		// 不可以用“返回键”取消
		// loadingDialog.setCancelable(false);
		//loadingDialog.setCancelable(true);
		loadingDialog.setContentView(layout);

		//设置dialog的大小
		 Window dialogWindow = loadingDialog.getWindow();
		 WindowManager.LayoutParams lp = dialogWindow.getAttributes();
		 lp.width = bitmap.getWidth()+textView.getLineHeight()+150;
		 int w = View.MeasureSpec.makeMeasureSpec(0,View.MeasureSpec.UNSPECIFIED);  
		 int h = View.MeasureSpec.makeMeasureSpec(0,View.MeasureSpec.UNSPECIFIED);  
		 textView.measure(w, h);  
		 lp.height = bitmap.getHeight()+textView.getMeasuredHeight()+60;
		 dialogWindow.setAttributes(lp);

		loadingDialog.getWindow().setType(
				WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);
		loadingDialog.show();
		}
	}

	/*
	 * 取消dialog
	 */

	public static void dismiss() {
		if (loadingDialog != null)
			loadingDialog.dismiss();
	}
	
	/*
	 * 取消dialog
	 */

	public static Dialog getDialog(){
		return loadingDialog;
	}
}
