package com.efounder.util;


import java.util.HashMap;
import java.util.Map;

/**
 * 全局map
 *
 * @author cherise
 */
public class GlobalMap {
    private static Map<String, String> envMap;

    public static void clearGlobalMap() {
        if (envMap != null)
            envMap.clear();
    }

    public static String getProperty(String key, String defValue) {
        if (envMap == null) return defValue;

        String value = (String) envMap.get(key);
        if (value == null)
            value = defValue;
        return value;
    }

    public static void setProperty(String key, String value) {
        if (envMap == null) envMap = new HashMap();
        envMap.put(key, value);
    }
}
